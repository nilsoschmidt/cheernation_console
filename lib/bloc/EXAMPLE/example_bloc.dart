import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'example_bloc.freezed.dart';

@freezed
abstract class ExampleEvent with _$ExampleEvent {
  const factory ExampleEvent.appStart() = _AppStart;
  const factory ExampleEvent.loggedIn() = _LoggedIn;
  const factory ExampleEvent.loggedOut() = _LoggedOut;
}

@freezed
abstract class ExampleState with _$ExampleState {
  const factory ExampleState.uninitialized() = _Uninitialized;
  const factory ExampleState.authenticated() = _Authenticated;
  const factory ExampleState.unauthenticated() = _Unauthenticated;
}

class ExampleBloc extends Bloc<ExampleEvent, ExampleState> {
  ExampleBloc() : super(ExampleState.uninitialized());

  @override
  Stream<ExampleState> mapEventToState(ExampleEvent event) async* {
    if (event is _AppStart) {
      yield* _mapAppStartedToState();
    } else if (event is _LoggedIn) {
      yield* _mapLoggedInToState();
    } else if (event is _LoggedOut) {
      yield* _mapLoggedOutToState();
    }
  }

  Stream<ExampleState> _mapAppStartedToState() async* {
    try {
      //Laden eines Users oder ähnliches
      if (true) {
        yield _Authenticated();
      } else {
        yield _Unauthenticated();
      }
    } catch (_) {
      yield _Unauthenticated();
    }
  }

  Stream<ExampleState> _mapLoggedInToState() async* {
    yield _Authenticated();
  }

  Stream<ExampleState> _mapLoggedOutToState() async* {
    yield _Unauthenticated();
  }
}
