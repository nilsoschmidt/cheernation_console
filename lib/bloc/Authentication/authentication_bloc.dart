import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cheernation_admin/data/repositories/user_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'authentication_bloc.freezed.dart';

@freezed
abstract class AuthenticationEvent with _$AuthenticationEvent {
  const factory AuthenticationEvent.appStart() = _AppStart;
  const factory AuthenticationEvent.loggedIn() = _LoggedIn;
  const factory AuthenticationEvent.loggedOut() = _LoggedOut;
  const factory AuthenticationEvent.deleteUser() = _DeleteUser;
}

@freezed
abstract class AuthenticationState with _$AuthenticationState {
  const factory AuthenticationState.uninitialized() = _Uninitialized;
  const factory AuthenticationState.authenticated() =
      _Authenticated;
  const factory AuthenticationState.unauthenticated() = _Unauthenticated;
}

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository _userRepository;

  AuthenticationBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(AuthenticationState.uninitialized());

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    if (event is _AppStart || event is _LoggedIn) {
      yield* _mapAppStartedToState();
    } /*else if (event is _LoggedIn) {
      yield* _mapLoggedInToState();
    }*/
    else if (event is _LoggedOut) {
      yield* _mapLoggedOutToState();
    }  
  }

  Stream<AuthenticationState> _mapAppStartedToState() async* {
    await Future.delayed(Duration(milliseconds: 300));

    try {
      final isSignedIn = _userRepository.isSignedIn();
      if (isSignedIn) {
        final userPrivate = await _userRepository.getUserPrivate();
        if (userPrivate.email == "cheernation.team@gmail.com") {
          yield _Authenticated();
        } 
      } else {
        yield _Unauthenticated();
      }
    } catch (_) {
      yield _Unauthenticated();
    }
  }

  Stream<AuthenticationState> _mapLoggedOutToState() async* {
    yield _Unauthenticated();
    _userRepository.signOut();
  }
}
