import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cheernation_admin/core/services/injection.dart';
import 'package:cheernation_admin/data/models/Team/team_short.dart';
import 'package:cheernation_admin/data/repositories/club_repository.dart';
import 'package:cheernation_admin/data/repositories/team_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'club_detail_bloc.freezed.dart';

@freezed
abstract class ClubDetailEvent with _$ClubDetailEvent {
  const factory ClubDetailEvent.loadTeamList(String clubId) = _LoadTeamList;
  const factory ClubDetailEvent.deleteClub(String clubId) = _DeleteClub;
}

@freezed
abstract class ClubDetailState with _$ClubDetailState {
  const factory ClubDetailState.loading() = _Loading;
  const factory ClubDetailState.loaded(List<TeamShort> teamList) = _Loaded;
  const factory ClubDetailState.failure() = _Failure;
}

class ClubDetailBloc extends Bloc<ClubDetailEvent, ClubDetailState> {
  ClubDetailBloc() : super(ClubDetailState.loading());

  @override
  Stream<ClubDetailState> mapEventToState(ClubDetailEvent event) async* {
    if (event is _LoadTeamList) {
      yield* _mapLoadClubListToState(event.clubId);
    } else if (event is _DeleteClub) {
      yield* _mapDeleteClubToState(event.clubId);
    }
  }

  Stream<ClubDetailState> _mapLoadClubListToState(String clubId) async* {
    try {
      TeamRepository teamRepository = resolve();
      print(await teamRepository.getTeamsByClubId(clubId));
      List<TeamShort> teamList = await teamRepository.getTeamsByClubId(clubId);

      yield _Loaded(teamList);
    } catch (e) {
      print(e);
      yield _Failure();
    }
  }

  Stream<ClubDetailState> _mapDeleteClubToState(String clubId) async* {
    try {
      ClubRepository clubRepository = resolve();
      await clubRepository.deleteClub(clubId);

      yield _Loaded([]);
    } catch (e) {
      print(e);
      yield _Failure();
    }
  }
}
