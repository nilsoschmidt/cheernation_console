import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cheernation_admin/core/services/injection.dart';
import 'package:cheernation_admin/data/models/Club/club_short.dart';
import 'package:cheernation_admin/data/repositories/club_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'clubs_main_bloc.freezed.dart';

@freezed
abstract class ClubsMainEvent with _$ClubsMainEvent {
  const factory ClubsMainEvent.loadClubList() = _LoadClubList;
}

@freezed
abstract class ClubsMainState with _$ClubsMainState {
  const factory ClubsMainState.loading() = _Loading;
  const factory ClubsMainState.loaded(List<ClubShort> clubList) = _Loaded;
  const factory ClubsMainState.failure() = _Failure;
}

class ClubsMainBloc extends Bloc<ClubsMainEvent, ClubsMainState> {
  ClubsMainBloc() : super(ClubsMainState.loading());

  @override
  Stream<ClubsMainState> mapEventToState(ClubsMainEvent event) async* {
    if (event is _LoadClubList) {
      yield* _mapLoadClubListToState();
    }
  }

  Stream<ClubsMainState> _mapLoadClubListToState() async* {
    try {
      ClubRepository clubRepository = resolve();
      List<ClubShort> clubList = await clubRepository.getAllClubs();
      if (clubList.isNotEmpty) {
        print(clubList);
        yield _Loaded(clubList);
      } else {
        yield _Failure();
      }
    } catch (e) {
      print(e);
      yield _Failure();
    }
  }
}
