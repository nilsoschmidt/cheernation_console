import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cheernation_admin/core/services/injection.dart';
import 'package:cheernation_admin/data/models/Club/club_short.dart';
import 'package:cheernation_admin/data/repositories/club_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'add_club_bloc.freezed.dart';

@freezed
abstract class AddClubEvent with _$AddClubEvent {
  const factory AddClubEvent.createNewClub(String clubName, Map<String,String> clubAdress) = _CreateNewClub;
}

@freezed
abstract class AddClubState with _$AddClubState {
  const factory AddClubState.empty() = _Empty;
  const factory AddClubState.finished() = _Finished;
  const factory AddClubState.failure() = _Failure;
}

class AddClubBloc extends Bloc<AddClubEvent, AddClubState> {
  AddClubBloc() : super(AddClubState.empty());

  @override
  Stream<AddClubState> mapEventToState(AddClubEvent event) async* {
    if (event is _CreateNewClub) {
      yield* _mapCreateNewClubToState(event.clubName, event.clubAdress);
    }
  }

  Stream<AddClubState> _mapCreateNewClubToState(String clubName, Map<String,String> clubAdress) async* {
    try {
      ClubRepository clubRepository = resolve();
      if (await clubRepository.createNewClub(clubName,clubAdress)) {
        print('lolololololol');
        yield _Finished();
      } else {
        yield _Failure();
      }
    } catch (e) {
      print('Fehler: ' + e);
      yield _Failure();
    }
  }
}
