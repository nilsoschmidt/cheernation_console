import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cheernation_admin/core/services/injection.dart';
import 'package:cheernation_admin/data/models/Team/team_member_short.dart';
import 'package:cheernation_admin/data/models/Team/team_short.dart';
import 'package:cheernation_admin/data/repositories/team_repository.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'team_detail_bloc.freezed.dart';

@freezed
abstract class TeamDetailEvent with _$TeamDetailEvent {
  const factory TeamDetailEvent.loadTeamMembers(TeamShort teamShort) =
      _LoadTeamMembers;
  const factory TeamDetailEvent.setAdmin(TeamMemberShort teamMemberShort,
      List<TeamMemberShort> teamMemberList) = _SetAdmin;
  const factory TeamDetailEvent.deleteTeam(String clubId, teamId) = _DeleteTeam;
}

@freezed
abstract class TeamDetailState with _$TeamDetailState {
  const factory TeamDetailState.loading() = _Loading;
  const factory TeamDetailState.loaded(List<TeamMemberShort> teamMemberList) =
      _Loaded;
  const factory TeamDetailState.failure() = _Failure;
}

class TeamDetailBloc extends Bloc<TeamDetailEvent, TeamDetailState> {
  TeamDetailBloc() : super(TeamDetailState.loading());

  @override
  Stream<TeamDetailState> mapEventToState(TeamDetailEvent event) async* {
    if (event is _LoadTeamMembers) {
      yield* _mapLoadTeamMembersToState(event.teamShort);
    } else if (event is _SetAdmin) {
      yield* _mapSetAdminToState(event.teamMemberShort, event.teamMemberList);
    } else if (event is _DeleteTeam) {
      yield* _mapDeleteTeamToState(event.clubId, event.teamId);
    }
  }

  Stream<TeamDetailState> _mapLoadTeamMembersToState(
      TeamShort teamShort) async* {
    try {
      TeamRepository teamRepository = resolve();
      List<TeamMemberShort> teamMemberList = await teamRepository.getTeamMember(
          teamShort.clubId, teamShort.teamId);
        print(teamMemberList);
        yield _Loaded(teamMemberList);
    } catch (e) {
      print(e);
      yield _Failure();
    }
  }

  Stream<TeamDetailState> _mapSetAdminToState(TeamMemberShort teamMemberShort,
      List<TeamMemberShort> teamMemberList) async* {
    try {
      TeamRepository teamRepository = resolve();
      await teamRepository.setAdmin(teamMemberShort.clubId,
          teamMemberShort.teamId, teamMemberShort.userId);
      yield _Loaded(teamMemberList);
    } catch (e) {
      print(e);
      yield _Failure();
    }
  }

  Stream<TeamDetailState> _mapDeleteTeamToState(String clubId, teamId) async* {
    try {
      TeamRepository teamRepository = resolve();
      await teamRepository.deleteTeam(clubId, teamId);
      yield _Failure();
    } catch (e) {
      print(e);
      yield _Failure();
    }
  }
}
