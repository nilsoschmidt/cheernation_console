import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cheernation_admin/core/services/injection.dart';
import 'package:cheernation_admin/data/models/Club/club_member_short.dart';
import 'package:cheernation_admin/data/models/Club/club_short.dart';
import 'package:cheernation_admin/data/repositories/club_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'club_member_bloc.freezed.dart';

@freezed
abstract class ClubMemberEvent with _$ClubMemberEvent {
  const factory ClubMemberEvent.loadClubMembers(ClubShort clubShort) =
      _LoadClubMembers;
  const factory ClubMemberEvent.setAdmin(ClubMemberShort clubMemberShort) =
      _SetAdmin;
}

@freezed
abstract class ClubMemberState with _$ClubMemberState {
  const factory ClubMemberState.loading() = _Loading;
  const factory ClubMemberState.loaded(List<ClubMemberShort> clubMemberList) =
      _Loaded;
  const factory ClubMemberState.failure() = _Failure;
}

class ClubMemberBloc extends Bloc<ClubMemberEvent, ClubMemberState> {
  ClubMemberBloc() : super(ClubMemberState.loading());

  @override
  Stream<ClubMemberState> mapEventToState(ClubMemberEvent event) async* {
    if (event is _LoadClubMembers) {
      yield* _mapLoadClubMembersToState(event.clubShort);
    } else if (event is _SetAdmin) {
      yield* _mapSetAdminToState(event.clubMemberShort);
    }
  }

  Stream<ClubMemberState> _mapLoadClubMembersToState(
      ClubShort clubShort) async* {
    try {
      ClubRepository clubRepository = resolve();
      List<ClubMemberShort> clubMemberList =
          await clubRepository.getClubMember(clubShort.clubId);
      if (clubMemberList.isNotEmpty) {
        print(clubMemberList);
        yield _Loaded(clubMemberList);
      } else {
        yield _Failure();
      }
    } catch (e) {
      print(e);
      yield _Failure();
    }
  }

  Stream<ClubMemberState> _mapSetAdminToState(
      ClubMemberShort clubMemberShort) async* {
    try {
      ClubRepository clubRepository = resolve();
      await clubRepository.setAdmin(clubMemberShort);
      // _mapLoadClubMembersToState(
      //     await clubRepository.getClubShortById(clubMemberShort.clubId));
    } catch (e) {
      print('Fehler2: ' + e.toString());
      yield _Failure();
    }
  }
}
