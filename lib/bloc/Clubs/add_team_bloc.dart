import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cheernation_admin/core/services/injection.dart';
import 'package:cheernation_admin/data/repositories/team_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'add_team_bloc.freezed.dart';

@freezed
abstract class AddTeamEvent with _$AddTeamEvent {
  const factory AddTeamEvent.createNewTeam(
          String clubId, String name, String image, String category) =
      _CreateNewTeam;
}

@freezed
abstract class AddTeamState with _$AddTeamState {
  const factory AddTeamState.empty() = _Empty;
  const factory AddTeamState.finished() = _Finished;
  const factory AddTeamState.failure() = _Failure;
}

class AddTeamBloc extends Bloc<AddTeamEvent, AddTeamState> {
  AddTeamBloc() : super(AddTeamState.empty());

  @override
  Stream<AddTeamState> mapEventToState(AddTeamEvent event) async* {
    if (event is _CreateNewTeam) {
      yield* _mapCreateNewTeamToState(
          event.clubId, event.name, event.image, event.category);
    }
  }

  Stream<AddTeamState> _mapCreateNewTeamToState(
      String clubId, String name, String image, String category) async* {
    try {
      TeamRepository teamRepository = resolve();
      if (await teamRepository.createNewTeam(clubId, name, image, category)) {
        yield _Finished();
      } else {
        yield _Failure();
      }
    } catch (e) {
      print('Fehler: ' + e.toString());
      yield _Failure();
    }
  }
}
