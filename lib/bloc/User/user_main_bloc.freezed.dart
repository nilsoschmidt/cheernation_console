// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'user_main_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$UserMainEventTearOff {
  const _$UserMainEventTearOff();

// ignore: unused_element
  _LoadUsers loadUsers() {
    return const _LoadUsers();
  }

// ignore: unused_element
  _DeleteUser deleteUser(String userId) {
    return _DeleteUser(
      userId,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $UserMainEvent = _$UserMainEventTearOff();

/// @nodoc
mixin _$UserMainEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUsers(),
    @required TResult deleteUser(String userId),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUsers(),
    TResult deleteUser(String userId),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUsers(_LoadUsers value),
    @required TResult deleteUser(_DeleteUser value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUsers(_LoadUsers value),
    TResult deleteUser(_DeleteUser value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $UserMainEventCopyWith<$Res> {
  factory $UserMainEventCopyWith(
          UserMainEvent value, $Res Function(UserMainEvent) then) =
      _$UserMainEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$UserMainEventCopyWithImpl<$Res>
    implements $UserMainEventCopyWith<$Res> {
  _$UserMainEventCopyWithImpl(this._value, this._then);

  final UserMainEvent _value;
  // ignore: unused_field
  final $Res Function(UserMainEvent) _then;
}

/// @nodoc
abstract class _$LoadUsersCopyWith<$Res> {
  factory _$LoadUsersCopyWith(
          _LoadUsers value, $Res Function(_LoadUsers) then) =
      __$LoadUsersCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadUsersCopyWithImpl<$Res> extends _$UserMainEventCopyWithImpl<$Res>
    implements _$LoadUsersCopyWith<$Res> {
  __$LoadUsersCopyWithImpl(_LoadUsers _value, $Res Function(_LoadUsers) _then)
      : super(_value, (v) => _then(v as _LoadUsers));

  @override
  _LoadUsers get _value => super._value as _LoadUsers;
}

/// @nodoc
class _$_LoadUsers with DiagnosticableTreeMixin implements _LoadUsers {
  const _$_LoadUsers();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'UserMainEvent.loadUsers()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'UserMainEvent.loadUsers'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _LoadUsers);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUsers(),
    @required TResult deleteUser(String userId),
  }) {
    assert(loadUsers != null);
    assert(deleteUser != null);
    return loadUsers();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUsers(),
    TResult deleteUser(String userId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadUsers != null) {
      return loadUsers();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUsers(_LoadUsers value),
    @required TResult deleteUser(_DeleteUser value),
  }) {
    assert(loadUsers != null);
    assert(deleteUser != null);
    return loadUsers(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUsers(_LoadUsers value),
    TResult deleteUser(_DeleteUser value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadUsers != null) {
      return loadUsers(this);
    }
    return orElse();
  }
}

abstract class _LoadUsers implements UserMainEvent {
  const factory _LoadUsers() = _$_LoadUsers;
}

/// @nodoc
abstract class _$DeleteUserCopyWith<$Res> {
  factory _$DeleteUserCopyWith(
          _DeleteUser value, $Res Function(_DeleteUser) then) =
      __$DeleteUserCopyWithImpl<$Res>;
  $Res call({String userId});
}

/// @nodoc
class __$DeleteUserCopyWithImpl<$Res> extends _$UserMainEventCopyWithImpl<$Res>
    implements _$DeleteUserCopyWith<$Res> {
  __$DeleteUserCopyWithImpl(
      _DeleteUser _value, $Res Function(_DeleteUser) _then)
      : super(_value, (v) => _then(v as _DeleteUser));

  @override
  _DeleteUser get _value => super._value as _DeleteUser;

  @override
  $Res call({
    Object userId = freezed,
  }) {
    return _then(_DeleteUser(
      userId == freezed ? _value.userId : userId as String,
    ));
  }
}

/// @nodoc
class _$_DeleteUser with DiagnosticableTreeMixin implements _DeleteUser {
  const _$_DeleteUser(this.userId) : assert(userId != null);

  @override
  final String userId;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'UserMainEvent.deleteUser(userId: $userId)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'UserMainEvent.deleteUser'))
      ..add(DiagnosticsProperty('userId', userId));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _DeleteUser &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(userId);

  @JsonKey(ignore: true)
  @override
  _$DeleteUserCopyWith<_DeleteUser> get copyWith =>
      __$DeleteUserCopyWithImpl<_DeleteUser>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadUsers(),
    @required TResult deleteUser(String userId),
  }) {
    assert(loadUsers != null);
    assert(deleteUser != null);
    return deleteUser(userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadUsers(),
    TResult deleteUser(String userId),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deleteUser != null) {
      return deleteUser(userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadUsers(_LoadUsers value),
    @required TResult deleteUser(_DeleteUser value),
  }) {
    assert(loadUsers != null);
    assert(deleteUser != null);
    return deleteUser(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadUsers(_LoadUsers value),
    TResult deleteUser(_DeleteUser value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deleteUser != null) {
      return deleteUser(this);
    }
    return orElse();
  }
}

abstract class _DeleteUser implements UserMainEvent {
  const factory _DeleteUser(String userId) = _$_DeleteUser;

  String get userId;
  @JsonKey(ignore: true)
  _$DeleteUserCopyWith<_DeleteUser> get copyWith;
}

/// @nodoc
class _$UserMainStateTearOff {
  const _$UserMainStateTearOff();

// ignore: unused_element
  _Loading loading() {
    return const _Loading();
  }

// ignore: unused_element
  _Loaded loaded(List<UserShort> userList) {
    return _Loaded(
      userList,
    );
  }

// ignore: unused_element
  _Failure failure() {
    return const _Failure();
  }
}

/// @nodoc
// ignore: unused_element
const $UserMainState = _$UserMainStateTearOff();

/// @nodoc
mixin _$UserMainState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loading(),
    @required TResult loaded(List<UserShort> userList),
    @required TResult failure(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loading(),
    TResult loaded(List<UserShort> userList),
    TResult failure(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loading(_Loading value),
    @required TResult loaded(_Loaded value),
    @required TResult failure(_Failure value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loading(_Loading value),
    TResult loaded(_Loaded value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $UserMainStateCopyWith<$Res> {
  factory $UserMainStateCopyWith(
          UserMainState value, $Res Function(UserMainState) then) =
      _$UserMainStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$UserMainStateCopyWithImpl<$Res>
    implements $UserMainStateCopyWith<$Res> {
  _$UserMainStateCopyWithImpl(this._value, this._then);

  final UserMainState _value;
  // ignore: unused_field
  final $Res Function(UserMainState) _then;
}

/// @nodoc
abstract class _$LoadingCopyWith<$Res> {
  factory _$LoadingCopyWith(_Loading value, $Res Function(_Loading) then) =
      __$LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadingCopyWithImpl<$Res> extends _$UserMainStateCopyWithImpl<$Res>
    implements _$LoadingCopyWith<$Res> {
  __$LoadingCopyWithImpl(_Loading _value, $Res Function(_Loading) _then)
      : super(_value, (v) => _then(v as _Loading));

  @override
  _Loading get _value => super._value as _Loading;
}

/// @nodoc
class _$_Loading with DiagnosticableTreeMixin implements _Loading {
  const _$_Loading();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'UserMainState.loading()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'UserMainState.loading'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loading(),
    @required TResult loaded(List<UserShort> userList),
    @required TResult failure(),
  }) {
    assert(loading != null);
    assert(loaded != null);
    assert(failure != null);
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loading(),
    TResult loaded(List<UserShort> userList),
    TResult failure(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loading(_Loading value),
    @required TResult loaded(_Loaded value),
    @required TResult failure(_Failure value),
  }) {
    assert(loading != null);
    assert(loaded != null);
    assert(failure != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loading(_Loading value),
    TResult loaded(_Loaded value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements UserMainState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$LoadedCopyWith<$Res> {
  factory _$LoadedCopyWith(_Loaded value, $Res Function(_Loaded) then) =
      __$LoadedCopyWithImpl<$Res>;
  $Res call({List<UserShort> userList});
}

/// @nodoc
class __$LoadedCopyWithImpl<$Res> extends _$UserMainStateCopyWithImpl<$Res>
    implements _$LoadedCopyWith<$Res> {
  __$LoadedCopyWithImpl(_Loaded _value, $Res Function(_Loaded) _then)
      : super(_value, (v) => _then(v as _Loaded));

  @override
  _Loaded get _value => super._value as _Loaded;

  @override
  $Res call({
    Object userList = freezed,
  }) {
    return _then(_Loaded(
      userList == freezed ? _value.userList : userList as List<UserShort>,
    ));
  }
}

/// @nodoc
class _$_Loaded with DiagnosticableTreeMixin implements _Loaded {
  const _$_Loaded(this.userList) : assert(userList != null);

  @override
  final List<UserShort> userList;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'UserMainState.loaded(userList: $userList)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'UserMainState.loaded'))
      ..add(DiagnosticsProperty('userList', userList));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Loaded &&
            (identical(other.userList, userList) ||
                const DeepCollectionEquality()
                    .equals(other.userList, userList)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(userList);

  @JsonKey(ignore: true)
  @override
  _$LoadedCopyWith<_Loaded> get copyWith =>
      __$LoadedCopyWithImpl<_Loaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loading(),
    @required TResult loaded(List<UserShort> userList),
    @required TResult failure(),
  }) {
    assert(loading != null);
    assert(loaded != null);
    assert(failure != null);
    return loaded(userList);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loading(),
    TResult loaded(List<UserShort> userList),
    TResult failure(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loaded != null) {
      return loaded(userList);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loading(_Loading value),
    @required TResult loaded(_Loaded value),
    @required TResult failure(_Failure value),
  }) {
    assert(loading != null);
    assert(loaded != null);
    assert(failure != null);
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loading(_Loading value),
    TResult loaded(_Loaded value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class _Loaded implements UserMainState {
  const factory _Loaded(List<UserShort> userList) = _$_Loaded;

  List<UserShort> get userList;
  @JsonKey(ignore: true)
  _$LoadedCopyWith<_Loaded> get copyWith;
}

/// @nodoc
abstract class _$FailureCopyWith<$Res> {
  factory _$FailureCopyWith(_Failure value, $Res Function(_Failure) then) =
      __$FailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$FailureCopyWithImpl<$Res> extends _$UserMainStateCopyWithImpl<$Res>
    implements _$FailureCopyWith<$Res> {
  __$FailureCopyWithImpl(_Failure _value, $Res Function(_Failure) _then)
      : super(_value, (v) => _then(v as _Failure));

  @override
  _Failure get _value => super._value as _Failure;
}

/// @nodoc
class _$_Failure with DiagnosticableTreeMixin implements _Failure {
  const _$_Failure();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'UserMainState.failure()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'UserMainState.failure'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Failure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loading(),
    @required TResult loaded(List<UserShort> userList),
    @required TResult failure(),
  }) {
    assert(loading != null);
    assert(loaded != null);
    assert(failure != null);
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loading(),
    TResult loaded(List<UserShort> userList),
    TResult failure(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loading(_Loading value),
    @required TResult loaded(_Loaded value),
    @required TResult failure(_Failure value),
  }) {
    assert(loading != null);
    assert(loaded != null);
    assert(failure != null);
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loading(_Loading value),
    TResult loaded(_Loaded value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class _Failure implements UserMainState {
  const factory _Failure() = _$_Failure;
}
