// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'user_search_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$UserSearchEventTearOff {
  const _$UserSearchEventTearOff();

// ignore: unused_element
  _ChangeString changeString(String userSearchText) {
    return _ChangeString(
      userSearchText,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $UserSearchEvent = _$UserSearchEventTearOff();

/// @nodoc
mixin _$UserSearchEvent {
  String get userSearchText;

  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult changeString(String userSearchText),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult changeString(String userSearchText),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult changeString(_ChangeString value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult changeString(_ChangeString value),
    @required TResult orElse(),
  });

  @JsonKey(ignore: true)
  $UserSearchEventCopyWith<UserSearchEvent> get copyWith;
}

/// @nodoc
abstract class $UserSearchEventCopyWith<$Res> {
  factory $UserSearchEventCopyWith(
          UserSearchEvent value, $Res Function(UserSearchEvent) then) =
      _$UserSearchEventCopyWithImpl<$Res>;
  $Res call({String userSearchText});
}

/// @nodoc
class _$UserSearchEventCopyWithImpl<$Res>
    implements $UserSearchEventCopyWith<$Res> {
  _$UserSearchEventCopyWithImpl(this._value, this._then);

  final UserSearchEvent _value;
  // ignore: unused_field
  final $Res Function(UserSearchEvent) _then;

  @override
  $Res call({
    Object userSearchText = freezed,
  }) {
    return _then(_value.copyWith(
      userSearchText: userSearchText == freezed
          ? _value.userSearchText
          : userSearchText as String,
    ));
  }
}

/// @nodoc
abstract class _$ChangeStringCopyWith<$Res>
    implements $UserSearchEventCopyWith<$Res> {
  factory _$ChangeStringCopyWith(
          _ChangeString value, $Res Function(_ChangeString) then) =
      __$ChangeStringCopyWithImpl<$Res>;
  @override
  $Res call({String userSearchText});
}

/// @nodoc
class __$ChangeStringCopyWithImpl<$Res>
    extends _$UserSearchEventCopyWithImpl<$Res>
    implements _$ChangeStringCopyWith<$Res> {
  __$ChangeStringCopyWithImpl(
      _ChangeString _value, $Res Function(_ChangeString) _then)
      : super(_value, (v) => _then(v as _ChangeString));

  @override
  _ChangeString get _value => super._value as _ChangeString;

  @override
  $Res call({
    Object userSearchText = freezed,
  }) {
    return _then(_ChangeString(
      userSearchText == freezed
          ? _value.userSearchText
          : userSearchText as String,
    ));
  }
}

/// @nodoc
class _$_ChangeString implements _ChangeString {
  const _$_ChangeString(this.userSearchText) : assert(userSearchText != null);

  @override
  final String userSearchText;

  @override
  String toString() {
    return 'UserSearchEvent.changeString(userSearchText: $userSearchText)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ChangeString &&
            (identical(other.userSearchText, userSearchText) ||
                const DeepCollectionEquality()
                    .equals(other.userSearchText, userSearchText)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userSearchText);

  @JsonKey(ignore: true)
  @override
  _$ChangeStringCopyWith<_ChangeString> get copyWith =>
      __$ChangeStringCopyWithImpl<_ChangeString>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult changeString(String userSearchText),
  }) {
    assert(changeString != null);
    return changeString(userSearchText);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult changeString(String userSearchText),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (changeString != null) {
      return changeString(userSearchText);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult changeString(_ChangeString value),
  }) {
    assert(changeString != null);
    return changeString(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult changeString(_ChangeString value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (changeString != null) {
      return changeString(this);
    }
    return orElse();
  }
}

abstract class _ChangeString implements UserSearchEvent {
  const factory _ChangeString(String userSearchText) = _$_ChangeString;

  @override
  String get userSearchText;
  @override
  @JsonKey(ignore: true)
  _$ChangeStringCopyWith<_ChangeString> get copyWith;
}

/// @nodoc
class _$UserSearchStateTearOff {
  const _$UserSearchStateTearOff();

// ignore: unused_element
  _StringEmpty stringEmpty() {
    return const _StringEmpty();
  }

// ignore: unused_element
  _Loading loading() {
    return const _Loading();
  }

// ignore: unused_element
  _UsersFound usersFound(List<UserShort> userShortList) {
    return _UsersFound(
      userShortList,
    );
  }

// ignore: unused_element
  _Failure failure() {
    return const _Failure();
  }
}

/// @nodoc
// ignore: unused_element
const $UserSearchState = _$UserSearchStateTearOff();

/// @nodoc
mixin _$UserSearchState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult stringEmpty(),
    @required TResult loading(),
    @required TResult usersFound(List<UserShort> userShortList),
    @required TResult failure(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult stringEmpty(),
    TResult loading(),
    TResult usersFound(List<UserShort> userShortList),
    TResult failure(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult stringEmpty(_StringEmpty value),
    @required TResult loading(_Loading value),
    @required TResult usersFound(_UsersFound value),
    @required TResult failure(_Failure value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult stringEmpty(_StringEmpty value),
    TResult loading(_Loading value),
    TResult usersFound(_UsersFound value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $UserSearchStateCopyWith<$Res> {
  factory $UserSearchStateCopyWith(
          UserSearchState value, $Res Function(UserSearchState) then) =
      _$UserSearchStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$UserSearchStateCopyWithImpl<$Res>
    implements $UserSearchStateCopyWith<$Res> {
  _$UserSearchStateCopyWithImpl(this._value, this._then);

  final UserSearchState _value;
  // ignore: unused_field
  final $Res Function(UserSearchState) _then;
}

/// @nodoc
abstract class _$StringEmptyCopyWith<$Res> {
  factory _$StringEmptyCopyWith(
          _StringEmpty value, $Res Function(_StringEmpty) then) =
      __$StringEmptyCopyWithImpl<$Res>;
}

/// @nodoc
class __$StringEmptyCopyWithImpl<$Res>
    extends _$UserSearchStateCopyWithImpl<$Res>
    implements _$StringEmptyCopyWith<$Res> {
  __$StringEmptyCopyWithImpl(
      _StringEmpty _value, $Res Function(_StringEmpty) _then)
      : super(_value, (v) => _then(v as _StringEmpty));

  @override
  _StringEmpty get _value => super._value as _StringEmpty;
}

/// @nodoc
class _$_StringEmpty implements _StringEmpty {
  const _$_StringEmpty();

  @override
  String toString() {
    return 'UserSearchState.stringEmpty()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _StringEmpty);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult stringEmpty(),
    @required TResult loading(),
    @required TResult usersFound(List<UserShort> userShortList),
    @required TResult failure(),
  }) {
    assert(stringEmpty != null);
    assert(loading != null);
    assert(usersFound != null);
    assert(failure != null);
    return stringEmpty();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult stringEmpty(),
    TResult loading(),
    TResult usersFound(List<UserShort> userShortList),
    TResult failure(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (stringEmpty != null) {
      return stringEmpty();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult stringEmpty(_StringEmpty value),
    @required TResult loading(_Loading value),
    @required TResult usersFound(_UsersFound value),
    @required TResult failure(_Failure value),
  }) {
    assert(stringEmpty != null);
    assert(loading != null);
    assert(usersFound != null);
    assert(failure != null);
    return stringEmpty(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult stringEmpty(_StringEmpty value),
    TResult loading(_Loading value),
    TResult usersFound(_UsersFound value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (stringEmpty != null) {
      return stringEmpty(this);
    }
    return orElse();
  }
}

abstract class _StringEmpty implements UserSearchState {
  const factory _StringEmpty() = _$_StringEmpty;
}

/// @nodoc
abstract class _$LoadingCopyWith<$Res> {
  factory _$LoadingCopyWith(_Loading value, $Res Function(_Loading) then) =
      __$LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadingCopyWithImpl<$Res> extends _$UserSearchStateCopyWithImpl<$Res>
    implements _$LoadingCopyWith<$Res> {
  __$LoadingCopyWithImpl(_Loading _value, $Res Function(_Loading) _then)
      : super(_value, (v) => _then(v as _Loading));

  @override
  _Loading get _value => super._value as _Loading;
}

/// @nodoc
class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'UserSearchState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult stringEmpty(),
    @required TResult loading(),
    @required TResult usersFound(List<UserShort> userShortList),
    @required TResult failure(),
  }) {
    assert(stringEmpty != null);
    assert(loading != null);
    assert(usersFound != null);
    assert(failure != null);
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult stringEmpty(),
    TResult loading(),
    TResult usersFound(List<UserShort> userShortList),
    TResult failure(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult stringEmpty(_StringEmpty value),
    @required TResult loading(_Loading value),
    @required TResult usersFound(_UsersFound value),
    @required TResult failure(_Failure value),
  }) {
    assert(stringEmpty != null);
    assert(loading != null);
    assert(usersFound != null);
    assert(failure != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult stringEmpty(_StringEmpty value),
    TResult loading(_Loading value),
    TResult usersFound(_UsersFound value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements UserSearchState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$UsersFoundCopyWith<$Res> {
  factory _$UsersFoundCopyWith(
          _UsersFound value, $Res Function(_UsersFound) then) =
      __$UsersFoundCopyWithImpl<$Res>;
  $Res call({List<UserShort> userShortList});
}

/// @nodoc
class __$UsersFoundCopyWithImpl<$Res>
    extends _$UserSearchStateCopyWithImpl<$Res>
    implements _$UsersFoundCopyWith<$Res> {
  __$UsersFoundCopyWithImpl(
      _UsersFound _value, $Res Function(_UsersFound) _then)
      : super(_value, (v) => _then(v as _UsersFound));

  @override
  _UsersFound get _value => super._value as _UsersFound;

  @override
  $Res call({
    Object userShortList = freezed,
  }) {
    return _then(_UsersFound(
      userShortList == freezed
          ? _value.userShortList
          : userShortList as List<UserShort>,
    ));
  }
}

/// @nodoc
class _$_UsersFound implements _UsersFound {
  const _$_UsersFound(this.userShortList) : assert(userShortList != null);

  @override
  final List<UserShort> userShortList;

  @override
  String toString() {
    return 'UserSearchState.usersFound(userShortList: $userShortList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _UsersFound &&
            (identical(other.userShortList, userShortList) ||
                const DeepCollectionEquality()
                    .equals(other.userShortList, userShortList)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(userShortList);

  @JsonKey(ignore: true)
  @override
  _$UsersFoundCopyWith<_UsersFound> get copyWith =>
      __$UsersFoundCopyWithImpl<_UsersFound>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult stringEmpty(),
    @required TResult loading(),
    @required TResult usersFound(List<UserShort> userShortList),
    @required TResult failure(),
  }) {
    assert(stringEmpty != null);
    assert(loading != null);
    assert(usersFound != null);
    assert(failure != null);
    return usersFound(userShortList);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult stringEmpty(),
    TResult loading(),
    TResult usersFound(List<UserShort> userShortList),
    TResult failure(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (usersFound != null) {
      return usersFound(userShortList);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult stringEmpty(_StringEmpty value),
    @required TResult loading(_Loading value),
    @required TResult usersFound(_UsersFound value),
    @required TResult failure(_Failure value),
  }) {
    assert(stringEmpty != null);
    assert(loading != null);
    assert(usersFound != null);
    assert(failure != null);
    return usersFound(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult stringEmpty(_StringEmpty value),
    TResult loading(_Loading value),
    TResult usersFound(_UsersFound value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (usersFound != null) {
      return usersFound(this);
    }
    return orElse();
  }
}

abstract class _UsersFound implements UserSearchState {
  const factory _UsersFound(List<UserShort> userShortList) = _$_UsersFound;

  List<UserShort> get userShortList;
  @JsonKey(ignore: true)
  _$UsersFoundCopyWith<_UsersFound> get copyWith;
}

/// @nodoc
abstract class _$FailureCopyWith<$Res> {
  factory _$FailureCopyWith(_Failure value, $Res Function(_Failure) then) =
      __$FailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$FailureCopyWithImpl<$Res> extends _$UserSearchStateCopyWithImpl<$Res>
    implements _$FailureCopyWith<$Res> {
  __$FailureCopyWithImpl(_Failure _value, $Res Function(_Failure) _then)
      : super(_value, (v) => _then(v as _Failure));

  @override
  _Failure get _value => super._value as _Failure;
}

/// @nodoc
class _$_Failure implements _Failure {
  const _$_Failure();

  @override
  String toString() {
    return 'UserSearchState.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Failure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult stringEmpty(),
    @required TResult loading(),
    @required TResult usersFound(List<UserShort> userShortList),
    @required TResult failure(),
  }) {
    assert(stringEmpty != null);
    assert(loading != null);
    assert(usersFound != null);
    assert(failure != null);
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult stringEmpty(),
    TResult loading(),
    TResult usersFound(List<UserShort> userShortList),
    TResult failure(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult stringEmpty(_StringEmpty value),
    @required TResult loading(_Loading value),
    @required TResult usersFound(_UsersFound value),
    @required TResult failure(_Failure value),
  }) {
    assert(stringEmpty != null);
    assert(loading != null);
    assert(usersFound != null);
    assert(failure != null);
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult stringEmpty(_StringEmpty value),
    TResult loading(_Loading value),
    TResult usersFound(_UsersFound value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class _Failure implements UserSearchState {
  const factory _Failure() = _$_Failure;
}
