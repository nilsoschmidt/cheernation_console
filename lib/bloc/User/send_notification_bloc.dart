import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cheernation_admin/core/services/injection.dart';
import 'package:cheernation_admin/data/models/User/user_short.dart';
import 'package:cheernation_admin/data/repositories/user_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'send_notification_bloc.freezed.dart';

@freezed
abstract class SendNotificationEvent with _$SendNotificationEvent {
  const factory SendNotificationEvent.sendMessage(String userId, String text) =
      _SendMessage;
}

@freezed
abstract class SendNotificationState with _$SendNotificationState {
  const factory SendNotificationState.empty() = _Empty;
  const factory SendNotificationState.finished() = _Finished;
  const factory SendNotificationState.failure() = _Failure;
}

class SendNotificationBloc extends Bloc<SendNotificationEvent, SendNotificationState> {
  SendNotificationBloc() : super(SendNotificationState.empty());

  @override
  Stream<SendNotificationState> mapEventToState(SendNotificationEvent event) async* {
    if (event is _SendMessage) {
      yield* _mapSendMessageToState(event.userId, event.text);
    }
  }

  Stream<SendNotificationState> _mapSendMessageToState(
      String userId, String text) async* {
    try {
      UserRepository userRepository = resolve();
      await userRepository.sendNotification(userId, text);
      yield _Finished();
    } catch (_) {
      yield _Failure();
    }
  }
}
