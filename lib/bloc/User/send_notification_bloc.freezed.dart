// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'send_notification_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$SendNotificationEventTearOff {
  const _$SendNotificationEventTearOff();

// ignore: unused_element
  _SendMessage sendMessage(String userId, String text) {
    return _SendMessage(
      userId,
      text,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $SendNotificationEvent = _$SendNotificationEventTearOff();

/// @nodoc
mixin _$SendNotificationEvent {
  String get userId;
  String get text;

  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult sendMessage(String userId, String text),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult sendMessage(String userId, String text),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult sendMessage(_SendMessage value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult sendMessage(_SendMessage value),
    @required TResult orElse(),
  });

  @JsonKey(ignore: true)
  $SendNotificationEventCopyWith<SendNotificationEvent> get copyWith;
}

/// @nodoc
abstract class $SendNotificationEventCopyWith<$Res> {
  factory $SendNotificationEventCopyWith(SendNotificationEvent value,
          $Res Function(SendNotificationEvent) then) =
      _$SendNotificationEventCopyWithImpl<$Res>;
  $Res call({String userId, String text});
}

/// @nodoc
class _$SendNotificationEventCopyWithImpl<$Res>
    implements $SendNotificationEventCopyWith<$Res> {
  _$SendNotificationEventCopyWithImpl(this._value, this._then);

  final SendNotificationEvent _value;
  // ignore: unused_field
  final $Res Function(SendNotificationEvent) _then;

  @override
  $Res call({
    Object userId = freezed,
    Object text = freezed,
  }) {
    return _then(_value.copyWith(
      userId: userId == freezed ? _value.userId : userId as String,
      text: text == freezed ? _value.text : text as String,
    ));
  }
}

/// @nodoc
abstract class _$SendMessageCopyWith<$Res>
    implements $SendNotificationEventCopyWith<$Res> {
  factory _$SendMessageCopyWith(
          _SendMessage value, $Res Function(_SendMessage) then) =
      __$SendMessageCopyWithImpl<$Res>;
  @override
  $Res call({String userId, String text});
}

/// @nodoc
class __$SendMessageCopyWithImpl<$Res>
    extends _$SendNotificationEventCopyWithImpl<$Res>
    implements _$SendMessageCopyWith<$Res> {
  __$SendMessageCopyWithImpl(
      _SendMessage _value, $Res Function(_SendMessage) _then)
      : super(_value, (v) => _then(v as _SendMessage));

  @override
  _SendMessage get _value => super._value as _SendMessage;

  @override
  $Res call({
    Object userId = freezed,
    Object text = freezed,
  }) {
    return _then(_SendMessage(
      userId == freezed ? _value.userId : userId as String,
      text == freezed ? _value.text : text as String,
    ));
  }
}

/// @nodoc
class _$_SendMessage implements _SendMessage {
  const _$_SendMessage(this.userId, this.text)
      : assert(userId != null),
        assert(text != null);

  @override
  final String userId;
  @override
  final String text;

  @override
  String toString() {
    return 'SendNotificationEvent.sendMessage(userId: $userId, text: $text)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SendMessage &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)) &&
            (identical(other.text, text) ||
                const DeepCollectionEquality().equals(other.text, text)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userId) ^
      const DeepCollectionEquality().hash(text);

  @JsonKey(ignore: true)
  @override
  _$SendMessageCopyWith<_SendMessage> get copyWith =>
      __$SendMessageCopyWithImpl<_SendMessage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult sendMessage(String userId, String text),
  }) {
    assert(sendMessage != null);
    return sendMessage(userId, text);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult sendMessage(String userId, String text),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (sendMessage != null) {
      return sendMessage(userId, text);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult sendMessage(_SendMessage value),
  }) {
    assert(sendMessage != null);
    return sendMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult sendMessage(_SendMessage value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (sendMessage != null) {
      return sendMessage(this);
    }
    return orElse();
  }
}

abstract class _SendMessage implements SendNotificationEvent {
  const factory _SendMessage(String userId, String text) = _$_SendMessage;

  @override
  String get userId;
  @override
  String get text;
  @override
  @JsonKey(ignore: true)
  _$SendMessageCopyWith<_SendMessage> get copyWith;
}

/// @nodoc
class _$SendNotificationStateTearOff {
  const _$SendNotificationStateTearOff();

// ignore: unused_element
  _Empty empty() {
    return const _Empty();
  }

// ignore: unused_element
  _Finished finished() {
    return const _Finished();
  }

// ignore: unused_element
  _Failure failure() {
    return const _Failure();
  }
}

/// @nodoc
// ignore: unused_element
const $SendNotificationState = _$SendNotificationStateTearOff();

/// @nodoc
mixin _$SendNotificationState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult empty(),
    @required TResult finished(),
    @required TResult failure(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult empty(),
    TResult finished(),
    TResult failure(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult empty(_Empty value),
    @required TResult finished(_Finished value),
    @required TResult failure(_Failure value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult empty(_Empty value),
    TResult finished(_Finished value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $SendNotificationStateCopyWith<$Res> {
  factory $SendNotificationStateCopyWith(SendNotificationState value,
          $Res Function(SendNotificationState) then) =
      _$SendNotificationStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$SendNotificationStateCopyWithImpl<$Res>
    implements $SendNotificationStateCopyWith<$Res> {
  _$SendNotificationStateCopyWithImpl(this._value, this._then);

  final SendNotificationState _value;
  // ignore: unused_field
  final $Res Function(SendNotificationState) _then;
}

/// @nodoc
abstract class _$EmptyCopyWith<$Res> {
  factory _$EmptyCopyWith(_Empty value, $Res Function(_Empty) then) =
      __$EmptyCopyWithImpl<$Res>;
}

/// @nodoc
class __$EmptyCopyWithImpl<$Res>
    extends _$SendNotificationStateCopyWithImpl<$Res>
    implements _$EmptyCopyWith<$Res> {
  __$EmptyCopyWithImpl(_Empty _value, $Res Function(_Empty) _then)
      : super(_value, (v) => _then(v as _Empty));

  @override
  _Empty get _value => super._value as _Empty;
}

/// @nodoc
class _$_Empty implements _Empty {
  const _$_Empty();

  @override
  String toString() {
    return 'SendNotificationState.empty()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Empty);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult empty(),
    @required TResult finished(),
    @required TResult failure(),
  }) {
    assert(empty != null);
    assert(finished != null);
    assert(failure != null);
    return empty();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult empty(),
    TResult finished(),
    TResult failure(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (empty != null) {
      return empty();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult empty(_Empty value),
    @required TResult finished(_Finished value),
    @required TResult failure(_Failure value),
  }) {
    assert(empty != null);
    assert(finished != null);
    assert(failure != null);
    return empty(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult empty(_Empty value),
    TResult finished(_Finished value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (empty != null) {
      return empty(this);
    }
    return orElse();
  }
}

abstract class _Empty implements SendNotificationState {
  const factory _Empty() = _$_Empty;
}

/// @nodoc
abstract class _$FinishedCopyWith<$Res> {
  factory _$FinishedCopyWith(_Finished value, $Res Function(_Finished) then) =
      __$FinishedCopyWithImpl<$Res>;
}

/// @nodoc
class __$FinishedCopyWithImpl<$Res>
    extends _$SendNotificationStateCopyWithImpl<$Res>
    implements _$FinishedCopyWith<$Res> {
  __$FinishedCopyWithImpl(_Finished _value, $Res Function(_Finished) _then)
      : super(_value, (v) => _then(v as _Finished));

  @override
  _Finished get _value => super._value as _Finished;
}

/// @nodoc
class _$_Finished implements _Finished {
  const _$_Finished();

  @override
  String toString() {
    return 'SendNotificationState.finished()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Finished);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult empty(),
    @required TResult finished(),
    @required TResult failure(),
  }) {
    assert(empty != null);
    assert(finished != null);
    assert(failure != null);
    return finished();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult empty(),
    TResult finished(),
    TResult failure(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (finished != null) {
      return finished();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult empty(_Empty value),
    @required TResult finished(_Finished value),
    @required TResult failure(_Failure value),
  }) {
    assert(empty != null);
    assert(finished != null);
    assert(failure != null);
    return finished(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult empty(_Empty value),
    TResult finished(_Finished value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (finished != null) {
      return finished(this);
    }
    return orElse();
  }
}

abstract class _Finished implements SendNotificationState {
  const factory _Finished() = _$_Finished;
}

/// @nodoc
abstract class _$FailureCopyWith<$Res> {
  factory _$FailureCopyWith(_Failure value, $Res Function(_Failure) then) =
      __$FailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$FailureCopyWithImpl<$Res>
    extends _$SendNotificationStateCopyWithImpl<$Res>
    implements _$FailureCopyWith<$Res> {
  __$FailureCopyWithImpl(_Failure _value, $Res Function(_Failure) _then)
      : super(_value, (v) => _then(v as _Failure));

  @override
  _Failure get _value => super._value as _Failure;
}

/// @nodoc
class _$_Failure implements _Failure {
  const _$_Failure();

  @override
  String toString() {
    return 'SendNotificationState.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Failure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult empty(),
    @required TResult finished(),
    @required TResult failure(),
  }) {
    assert(empty != null);
    assert(finished != null);
    assert(failure != null);
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult empty(),
    TResult finished(),
    TResult failure(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult empty(_Empty value),
    @required TResult finished(_Finished value),
    @required TResult failure(_Failure value),
  }) {
    assert(empty != null);
    assert(finished != null);
    assert(failure != null);
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult empty(_Empty value),
    TResult finished(_Finished value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class _Failure implements SendNotificationState {
  const factory _Failure() = _$_Failure;
}
