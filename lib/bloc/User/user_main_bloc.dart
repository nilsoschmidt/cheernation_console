import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cheernation_admin/core/services/injection.dart';
import 'package:cheernation_admin/data/models/User/user_short.dart';
import 'package:cheernation_admin/data/repositories/user_repository.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_main_bloc.freezed.dart';

@freezed
abstract class UserMainEvent with _$UserMainEvent {
  const factory UserMainEvent.loadUsers() = _LoadUsers;
  const factory UserMainEvent.deleteUser(String userId) = _DeleteUser;
}

@freezed
abstract class UserMainState with _$UserMainState {
  const factory UserMainState.loading() = _Loading;
  const factory UserMainState.loaded(List<UserShort> userList) = _Loaded;
  const factory UserMainState.failure() = _Failure;
}

class UserMainBloc extends Bloc<UserMainEvent, UserMainState> {
  UserMainBloc() : super(UserMainState.loading());

  @override
  Stream<UserMainState> mapEventToState(UserMainEvent event) async* {
    if (event is _LoadUsers) {
      yield* _mapLoadUsersToState();
    } else if (event is _DeleteUser) {
      yield* _mapDeleteUserToState(event.userId);
    }
  }

  Stream<UserMainState> _mapLoadUsersToState() async* {
    try {
      List<UserShort> userList = [];
      UserRepository userRepository = resolve();
      userList = await userRepository.getAllUsers();
      if (userList.isNotEmpty) {
        yield _Loaded(userList);
      } else {
        yield _Failure();
      }
    } catch (_) {
      yield _Failure();
    }
  }

  Stream<UserMainState> _mapDeleteUserToState(String userId) async* {
    try {
      UserRepository userRepository = resolve();
      await userRepository.deleteUser(userId);
    } catch (_) {
      yield _Failure();
    }
  }
}
