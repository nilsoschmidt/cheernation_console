import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cheernation_admin/core/services/injection.dart';
import 'package:cheernation_admin/data/models/User/user_short.dart';
import 'package:cheernation_admin/data/repositories/user_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_search_bloc.freezed.dart';

@freezed
abstract class UserSearchEvent with _$UserSearchEvent {
  const factory UserSearchEvent.changeString(String userSearchText) = _ChangeString;
}

@freezed
abstract class UserSearchState with _$UserSearchState {
  const factory UserSearchState.stringEmpty() = _StringEmpty;
  const factory UserSearchState.loading() = _Loading;
  const factory UserSearchState.usersFound(List<UserShort> userShortList) =
      _UsersFound;
  const factory UserSearchState.failure() = _Failure;
}

class UserSearchBloc extends Bloc<UserSearchEvent, UserSearchState> {
  UserSearchBloc() : super(UserSearchState.stringEmpty());
  UserRepository _userRepository = resolve();
  String oldText = "";

  @override
  Stream<UserSearchState> mapEventToState(UserSearchEvent event) async* {
    if (event is _ChangeString) {
      yield* _mapChangeStringToState(event.userSearchText);
    }
  }

  Stream<UserSearchState> _mapChangeStringToState(String userSearchText) async* {
    if (oldText == userSearchText) {
    } else {
      oldText = userSearchText;
      if (userSearchText.length < 1) {
        if (userSearchText.length == 0) {
          yield _StringEmpty();
        } else {
          yield _Failure();
        }
      } else {
        yield _Loading();
        try {
          List<UserShort> userShortList =
              await _userRepository.getUserShortList(userSearchText.toLowerCase());
          if (userShortList != null) {
            yield _UsersFound(userShortList);
          } else {
            yield _Failure();
          }
        } catch (_) {
          yield _Failure();
        }
      }
    }
  }
}
