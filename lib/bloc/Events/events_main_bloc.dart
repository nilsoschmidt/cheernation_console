import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cheernation_admin/core/services/injection.dart';
import 'package:cheernation_admin/data/models/Events/championship.dart';
import 'package:cheernation_admin/data/repositories/event_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'events_main_bloc.freezed.dart';

@freezed
abstract class EventsMainEvent with _$EventsMainEvent {
  const factory EventsMainEvent.loadEvents() = _LoadEvents;
  const factory EventsMainEvent.deleteEvent(String eventId) = _DeleteEvent;
}

@freezed
abstract class EventsMainState with _$EventsMainState {
  const factory EventsMainState.empty() = _Empty;
  const factory EventsMainState.loaded(List<Championship> championshipList) =
      _Loaded;
  const factory EventsMainState.failure() = _Failure;
}

class EventsMainBloc extends Bloc<EventsMainEvent, EventsMainState> {
  EventsMainBloc() : super(EventsMainState.empty());

  @override
  Stream<EventsMainState> mapEventToState(EventsMainEvent event) async* {
    if (event is _LoadEvents) {
      yield* _mapLoadEventsToState();
    } else if (event is _DeleteEvent) {
      yield* _mapDeleteEventToState(event.eventId);
    }
  }

  Stream<EventsMainState> _mapLoadEventsToState() async* {
    try {
      EventRepository eventRepository = resolve();
      List<Championship> championshipList =
          await eventRepository.getAllChampionships();
      //Laden eines Users oder ähnliches
      if (championshipList.isNotEmpty) {
        yield _Loaded(championshipList);
      } else {
        yield _Failure();
      }
    } catch (_) {
      yield _Failure();
    }
  }

  Stream<EventsMainState> _mapDeleteEventToState(String eventId) async* {
    try {
      EventRepository eventRepository = resolve();
      await eventRepository.deleteEvent(eventId);
      //Laden eines Users oder ähnliches
      if (true) {
      } else {
        yield _Failure();
      }
    } catch (_) {
      yield _Failure();
    }
  }
}
