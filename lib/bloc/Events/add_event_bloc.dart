import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cheernation_admin/core/services/injection.dart';
import 'package:cheernation_admin/data/models/Events/championship.dart';
import 'package:cheernation_admin/data/repositories/event_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'add_event_bloc.freezed.dart';

@freezed
abstract class AddEventEvent with _$AddEventEvent {
  const factory AddEventEvent.addEvent(Championship championship) = _AddEvent;
  const factory AddEventEvent.editEvent(Championship championship) = _EditEvent;
}

@freezed
abstract class AddEventState with _$AddEventState {
  const factory AddEventState.empty() = _Empty;
}

class AddEventBloc extends Bloc<AddEventEvent, AddEventState> {
  AddEventBloc() : super(AddEventState.empty());

  @override
  Stream<AddEventState> mapEventToState(AddEventEvent event) async* {
    if (event is _AddEvent) {
      yield* _mapAddEventToState(event.championship);
    } else if (event is _EditEvent) {
      yield* _mapEditEventToState(event.championship);
    }
  }

  Stream<AddEventState> _mapAddEventToState(Championship championship) async* {
    try {
      EventRepository eventRepository = resolve();
      await eventRepository.addEvent(championship);
      //Laden eines Users oder ähnliches

    } catch (_) {}
  }

  Stream<AddEventState> _mapEditEventToState(Championship championship) async* {
    try {
      EventRepository eventRepository = resolve();
      await eventRepository.editEvent(championship);
      //Laden eines Users oder ähnliches

    } catch (_) {}
  }
}
