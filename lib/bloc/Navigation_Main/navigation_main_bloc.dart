import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:cheernation_admin/core/services/injection.dart';
import 'package:cheernation_admin/data/models/Team/team_member_short.dart';
import 'package:cheernation_admin/data/models/Team/team_short.dart';
import 'package:cheernation_admin/data/models/User/user_private.dart';
import 'package:cheernation_admin/data/repositories/team_repository.dart';
import 'package:cheernation_admin/data/repositories/user_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'navigation_main_bloc.freezed.dart';

@freezed
abstract class NavigationMainEvent with _$NavigationMainEvent {
  const factory NavigationMainEvent.changeToUser() = _ChangeToUser;
}

@freezed
abstract class NavigationMainState with _$NavigationMainState {
  const factory NavigationMainState.empty() = _Empty;
  const factory NavigationMainState.loading() = _Loading;
  const factory NavigationMainState.navMenu() = _NavMenu;
}

class NavigationMainBloc
    extends Bloc<NavigationMainEvent, NavigationMainState> {
  NavigationMainBloc() : super(NavigationMainState.empty());

  @override
  Stream<NavigationMainState> mapEventToState(
      NavigationMainEvent event) async* {
    if (event is _ChangeToUser) {
      yield* _mapUserInterfaceToState();
    } 
  }

  Stream<NavigationMainState> _mapUserInterfaceToState() async* {
    yield _Loading();
    //await Future.delayed(200.milliseconds);
    try {
      yield _NavMenu();
    } catch (e) {
      print(e);
      yield _Empty();
    }
  }
}
