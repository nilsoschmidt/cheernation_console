// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'news_main_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$NewsMainEventTearOff {
  const _$NewsMainEventTearOff();

// ignore: unused_element
  _LoadNews loadNews() {
    return const _LoadNews();
  }

// ignore: unused_element
  _DeleteArticle deleteArticle(String articleId) {
    return _DeleteArticle(
      articleId,
    );
  }

// ignore: unused_element
  _CreateNewArticle createNewArticle(
      String title, String subtitle, String text) {
    return _CreateNewArticle(
      title,
      subtitle,
      text,
    );
  }

// ignore: unused_element
  _EditArticle editArticle(
      String articleId, String title, String subtitle, String text) {
    return _EditArticle(
      articleId,
      title,
      subtitle,
      text,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $NewsMainEvent = _$NewsMainEventTearOff();

/// @nodoc
mixin _$NewsMainEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadNews(),
    @required TResult deleteArticle(String articleId),
    @required
        TResult createNewArticle(String title, String subtitle, String text),
    @required
        TResult editArticle(
            String articleId, String title, String subtitle, String text),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadNews(),
    TResult deleteArticle(String articleId),
    TResult createNewArticle(String title, String subtitle, String text),
    TResult editArticle(
        String articleId, String title, String subtitle, String text),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadNews(_LoadNews value),
    @required TResult deleteArticle(_DeleteArticle value),
    @required TResult createNewArticle(_CreateNewArticle value),
    @required TResult editArticle(_EditArticle value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadNews(_LoadNews value),
    TResult deleteArticle(_DeleteArticle value),
    TResult createNewArticle(_CreateNewArticle value),
    TResult editArticle(_EditArticle value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $NewsMainEventCopyWith<$Res> {
  factory $NewsMainEventCopyWith(
          NewsMainEvent value, $Res Function(NewsMainEvent) then) =
      _$NewsMainEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$NewsMainEventCopyWithImpl<$Res>
    implements $NewsMainEventCopyWith<$Res> {
  _$NewsMainEventCopyWithImpl(this._value, this._then);

  final NewsMainEvent _value;
  // ignore: unused_field
  final $Res Function(NewsMainEvent) _then;
}

/// @nodoc
abstract class _$LoadNewsCopyWith<$Res> {
  factory _$LoadNewsCopyWith(_LoadNews value, $Res Function(_LoadNews) then) =
      __$LoadNewsCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadNewsCopyWithImpl<$Res> extends _$NewsMainEventCopyWithImpl<$Res>
    implements _$LoadNewsCopyWith<$Res> {
  __$LoadNewsCopyWithImpl(_LoadNews _value, $Res Function(_LoadNews) _then)
      : super(_value, (v) => _then(v as _LoadNews));

  @override
  _LoadNews get _value => super._value as _LoadNews;
}

/// @nodoc
class _$_LoadNews implements _LoadNews {
  const _$_LoadNews();

  @override
  String toString() {
    return 'NewsMainEvent.loadNews()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _LoadNews);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadNews(),
    @required TResult deleteArticle(String articleId),
    @required
        TResult createNewArticle(String title, String subtitle, String text),
    @required
        TResult editArticle(
            String articleId, String title, String subtitle, String text),
  }) {
    assert(loadNews != null);
    assert(deleteArticle != null);
    assert(createNewArticle != null);
    assert(editArticle != null);
    return loadNews();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadNews(),
    TResult deleteArticle(String articleId),
    TResult createNewArticle(String title, String subtitle, String text),
    TResult editArticle(
        String articleId, String title, String subtitle, String text),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadNews != null) {
      return loadNews();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadNews(_LoadNews value),
    @required TResult deleteArticle(_DeleteArticle value),
    @required TResult createNewArticle(_CreateNewArticle value),
    @required TResult editArticle(_EditArticle value),
  }) {
    assert(loadNews != null);
    assert(deleteArticle != null);
    assert(createNewArticle != null);
    assert(editArticle != null);
    return loadNews(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadNews(_LoadNews value),
    TResult deleteArticle(_DeleteArticle value),
    TResult createNewArticle(_CreateNewArticle value),
    TResult editArticle(_EditArticle value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadNews != null) {
      return loadNews(this);
    }
    return orElse();
  }
}

abstract class _LoadNews implements NewsMainEvent {
  const factory _LoadNews() = _$_LoadNews;
}

/// @nodoc
abstract class _$DeleteArticleCopyWith<$Res> {
  factory _$DeleteArticleCopyWith(
          _DeleteArticle value, $Res Function(_DeleteArticle) then) =
      __$DeleteArticleCopyWithImpl<$Res>;
  $Res call({String articleId});
}

/// @nodoc
class __$DeleteArticleCopyWithImpl<$Res>
    extends _$NewsMainEventCopyWithImpl<$Res>
    implements _$DeleteArticleCopyWith<$Res> {
  __$DeleteArticleCopyWithImpl(
      _DeleteArticle _value, $Res Function(_DeleteArticle) _then)
      : super(_value, (v) => _then(v as _DeleteArticle));

  @override
  _DeleteArticle get _value => super._value as _DeleteArticle;

  @override
  $Res call({
    Object articleId = freezed,
  }) {
    return _then(_DeleteArticle(
      articleId == freezed ? _value.articleId : articleId as String,
    ));
  }
}

/// @nodoc
class _$_DeleteArticle implements _DeleteArticle {
  const _$_DeleteArticle(this.articleId) : assert(articleId != null);

  @override
  final String articleId;

  @override
  String toString() {
    return 'NewsMainEvent.deleteArticle(articleId: $articleId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _DeleteArticle &&
            (identical(other.articleId, articleId) ||
                const DeepCollectionEquality()
                    .equals(other.articleId, articleId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(articleId);

  @JsonKey(ignore: true)
  @override
  _$DeleteArticleCopyWith<_DeleteArticle> get copyWith =>
      __$DeleteArticleCopyWithImpl<_DeleteArticle>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadNews(),
    @required TResult deleteArticle(String articleId),
    @required
        TResult createNewArticle(String title, String subtitle, String text),
    @required
        TResult editArticle(
            String articleId, String title, String subtitle, String text),
  }) {
    assert(loadNews != null);
    assert(deleteArticle != null);
    assert(createNewArticle != null);
    assert(editArticle != null);
    return deleteArticle(articleId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadNews(),
    TResult deleteArticle(String articleId),
    TResult createNewArticle(String title, String subtitle, String text),
    TResult editArticle(
        String articleId, String title, String subtitle, String text),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deleteArticle != null) {
      return deleteArticle(articleId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadNews(_LoadNews value),
    @required TResult deleteArticle(_DeleteArticle value),
    @required TResult createNewArticle(_CreateNewArticle value),
    @required TResult editArticle(_EditArticle value),
  }) {
    assert(loadNews != null);
    assert(deleteArticle != null);
    assert(createNewArticle != null);
    assert(editArticle != null);
    return deleteArticle(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadNews(_LoadNews value),
    TResult deleteArticle(_DeleteArticle value),
    TResult createNewArticle(_CreateNewArticle value),
    TResult editArticle(_EditArticle value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (deleteArticle != null) {
      return deleteArticle(this);
    }
    return orElse();
  }
}

abstract class _DeleteArticle implements NewsMainEvent {
  const factory _DeleteArticle(String articleId) = _$_DeleteArticle;

  String get articleId;
  @JsonKey(ignore: true)
  _$DeleteArticleCopyWith<_DeleteArticle> get copyWith;
}

/// @nodoc
abstract class _$CreateNewArticleCopyWith<$Res> {
  factory _$CreateNewArticleCopyWith(
          _CreateNewArticle value, $Res Function(_CreateNewArticle) then) =
      __$CreateNewArticleCopyWithImpl<$Res>;
  $Res call({String title, String subtitle, String text});
}

/// @nodoc
class __$CreateNewArticleCopyWithImpl<$Res>
    extends _$NewsMainEventCopyWithImpl<$Res>
    implements _$CreateNewArticleCopyWith<$Res> {
  __$CreateNewArticleCopyWithImpl(
      _CreateNewArticle _value, $Res Function(_CreateNewArticle) _then)
      : super(_value, (v) => _then(v as _CreateNewArticle));

  @override
  _CreateNewArticle get _value => super._value as _CreateNewArticle;

  @override
  $Res call({
    Object title = freezed,
    Object subtitle = freezed,
    Object text = freezed,
  }) {
    return _then(_CreateNewArticle(
      title == freezed ? _value.title : title as String,
      subtitle == freezed ? _value.subtitle : subtitle as String,
      text == freezed ? _value.text : text as String,
    ));
  }
}

/// @nodoc
class _$_CreateNewArticle implements _CreateNewArticle {
  const _$_CreateNewArticle(this.title, this.subtitle, this.text)
      : assert(title != null),
        assert(subtitle != null),
        assert(text != null);

  @override
  final String title;
  @override
  final String subtitle;
  @override
  final String text;

  @override
  String toString() {
    return 'NewsMainEvent.createNewArticle(title: $title, subtitle: $subtitle, text: $text)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CreateNewArticle &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.subtitle, subtitle) ||
                const DeepCollectionEquality()
                    .equals(other.subtitle, subtitle)) &&
            (identical(other.text, text) ||
                const DeepCollectionEquality().equals(other.text, text)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(subtitle) ^
      const DeepCollectionEquality().hash(text);

  @JsonKey(ignore: true)
  @override
  _$CreateNewArticleCopyWith<_CreateNewArticle> get copyWith =>
      __$CreateNewArticleCopyWithImpl<_CreateNewArticle>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadNews(),
    @required TResult deleteArticle(String articleId),
    @required
        TResult createNewArticle(String title, String subtitle, String text),
    @required
        TResult editArticle(
            String articleId, String title, String subtitle, String text),
  }) {
    assert(loadNews != null);
    assert(deleteArticle != null);
    assert(createNewArticle != null);
    assert(editArticle != null);
    return createNewArticle(title, subtitle, text);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadNews(),
    TResult deleteArticle(String articleId),
    TResult createNewArticle(String title, String subtitle, String text),
    TResult editArticle(
        String articleId, String title, String subtitle, String text),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (createNewArticle != null) {
      return createNewArticle(title, subtitle, text);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadNews(_LoadNews value),
    @required TResult deleteArticle(_DeleteArticle value),
    @required TResult createNewArticle(_CreateNewArticle value),
    @required TResult editArticle(_EditArticle value),
  }) {
    assert(loadNews != null);
    assert(deleteArticle != null);
    assert(createNewArticle != null);
    assert(editArticle != null);
    return createNewArticle(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadNews(_LoadNews value),
    TResult deleteArticle(_DeleteArticle value),
    TResult createNewArticle(_CreateNewArticle value),
    TResult editArticle(_EditArticle value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (createNewArticle != null) {
      return createNewArticle(this);
    }
    return orElse();
  }
}

abstract class _CreateNewArticle implements NewsMainEvent {
  const factory _CreateNewArticle(String title, String subtitle, String text) =
      _$_CreateNewArticle;

  String get title;
  String get subtitle;
  String get text;
  @JsonKey(ignore: true)
  _$CreateNewArticleCopyWith<_CreateNewArticle> get copyWith;
}

/// @nodoc
abstract class _$EditArticleCopyWith<$Res> {
  factory _$EditArticleCopyWith(
          _EditArticle value, $Res Function(_EditArticle) then) =
      __$EditArticleCopyWithImpl<$Res>;
  $Res call({String articleId, String title, String subtitle, String text});
}

/// @nodoc
class __$EditArticleCopyWithImpl<$Res> extends _$NewsMainEventCopyWithImpl<$Res>
    implements _$EditArticleCopyWith<$Res> {
  __$EditArticleCopyWithImpl(
      _EditArticle _value, $Res Function(_EditArticle) _then)
      : super(_value, (v) => _then(v as _EditArticle));

  @override
  _EditArticle get _value => super._value as _EditArticle;

  @override
  $Res call({
    Object articleId = freezed,
    Object title = freezed,
    Object subtitle = freezed,
    Object text = freezed,
  }) {
    return _then(_EditArticle(
      articleId == freezed ? _value.articleId : articleId as String,
      title == freezed ? _value.title : title as String,
      subtitle == freezed ? _value.subtitle : subtitle as String,
      text == freezed ? _value.text : text as String,
    ));
  }
}

/// @nodoc
class _$_EditArticle implements _EditArticle {
  const _$_EditArticle(this.articleId, this.title, this.subtitle, this.text)
      : assert(articleId != null),
        assert(title != null),
        assert(subtitle != null),
        assert(text != null);

  @override
  final String articleId;
  @override
  final String title;
  @override
  final String subtitle;
  @override
  final String text;

  @override
  String toString() {
    return 'NewsMainEvent.editArticle(articleId: $articleId, title: $title, subtitle: $subtitle, text: $text)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EditArticle &&
            (identical(other.articleId, articleId) ||
                const DeepCollectionEquality()
                    .equals(other.articleId, articleId)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.subtitle, subtitle) ||
                const DeepCollectionEquality()
                    .equals(other.subtitle, subtitle)) &&
            (identical(other.text, text) ||
                const DeepCollectionEquality().equals(other.text, text)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(articleId) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(subtitle) ^
      const DeepCollectionEquality().hash(text);

  @JsonKey(ignore: true)
  @override
  _$EditArticleCopyWith<_EditArticle> get copyWith =>
      __$EditArticleCopyWithImpl<_EditArticle>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult loadNews(),
    @required TResult deleteArticle(String articleId),
    @required
        TResult createNewArticle(String title, String subtitle, String text),
    @required
        TResult editArticle(
            String articleId, String title, String subtitle, String text),
  }) {
    assert(loadNews != null);
    assert(deleteArticle != null);
    assert(createNewArticle != null);
    assert(editArticle != null);
    return editArticle(articleId, title, subtitle, text);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult loadNews(),
    TResult deleteArticle(String articleId),
    TResult createNewArticle(String title, String subtitle, String text),
    TResult editArticle(
        String articleId, String title, String subtitle, String text),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (editArticle != null) {
      return editArticle(articleId, title, subtitle, text);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult loadNews(_LoadNews value),
    @required TResult deleteArticle(_DeleteArticle value),
    @required TResult createNewArticle(_CreateNewArticle value),
    @required TResult editArticle(_EditArticle value),
  }) {
    assert(loadNews != null);
    assert(deleteArticle != null);
    assert(createNewArticle != null);
    assert(editArticle != null);
    return editArticle(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult loadNews(_LoadNews value),
    TResult deleteArticle(_DeleteArticle value),
    TResult createNewArticle(_CreateNewArticle value),
    TResult editArticle(_EditArticle value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (editArticle != null) {
      return editArticle(this);
    }
    return orElse();
  }
}

abstract class _EditArticle implements NewsMainEvent {
  const factory _EditArticle(
          String articleId, String title, String subtitle, String text) =
      _$_EditArticle;

  String get articleId;
  String get title;
  String get subtitle;
  String get text;
  @JsonKey(ignore: true)
  _$EditArticleCopyWith<_EditArticle> get copyWith;
}

/// @nodoc
class _$NewsMainStateTearOff {
  const _$NewsMainStateTearOff();

// ignore: unused_element
  _Empty empty() {
    return const _Empty();
  }

// ignore: unused_element
  _Loaded loaded(List<Article> articleList) {
    return _Loaded(
      articleList,
    );
  }

// ignore: unused_element
  _Failure failure() {
    return const _Failure();
  }
}

/// @nodoc
// ignore: unused_element
const $NewsMainState = _$NewsMainStateTearOff();

/// @nodoc
mixin _$NewsMainState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult empty(),
    @required TResult loaded(List<Article> articleList),
    @required TResult failure(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult empty(),
    TResult loaded(List<Article> articleList),
    TResult failure(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult empty(_Empty value),
    @required TResult loaded(_Loaded value),
    @required TResult failure(_Failure value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult empty(_Empty value),
    TResult loaded(_Loaded value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $NewsMainStateCopyWith<$Res> {
  factory $NewsMainStateCopyWith(
          NewsMainState value, $Res Function(NewsMainState) then) =
      _$NewsMainStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$NewsMainStateCopyWithImpl<$Res>
    implements $NewsMainStateCopyWith<$Res> {
  _$NewsMainStateCopyWithImpl(this._value, this._then);

  final NewsMainState _value;
  // ignore: unused_field
  final $Res Function(NewsMainState) _then;
}

/// @nodoc
abstract class _$EmptyCopyWith<$Res> {
  factory _$EmptyCopyWith(_Empty value, $Res Function(_Empty) then) =
      __$EmptyCopyWithImpl<$Res>;
}

/// @nodoc
class __$EmptyCopyWithImpl<$Res> extends _$NewsMainStateCopyWithImpl<$Res>
    implements _$EmptyCopyWith<$Res> {
  __$EmptyCopyWithImpl(_Empty _value, $Res Function(_Empty) _then)
      : super(_value, (v) => _then(v as _Empty));

  @override
  _Empty get _value => super._value as _Empty;
}

/// @nodoc
class _$_Empty implements _Empty {
  const _$_Empty();

  @override
  String toString() {
    return 'NewsMainState.empty()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Empty);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult empty(),
    @required TResult loaded(List<Article> articleList),
    @required TResult failure(),
  }) {
    assert(empty != null);
    assert(loaded != null);
    assert(failure != null);
    return empty();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult empty(),
    TResult loaded(List<Article> articleList),
    TResult failure(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (empty != null) {
      return empty();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult empty(_Empty value),
    @required TResult loaded(_Loaded value),
    @required TResult failure(_Failure value),
  }) {
    assert(empty != null);
    assert(loaded != null);
    assert(failure != null);
    return empty(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult empty(_Empty value),
    TResult loaded(_Loaded value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (empty != null) {
      return empty(this);
    }
    return orElse();
  }
}

abstract class _Empty implements NewsMainState {
  const factory _Empty() = _$_Empty;
}

/// @nodoc
abstract class _$LoadedCopyWith<$Res> {
  factory _$LoadedCopyWith(_Loaded value, $Res Function(_Loaded) then) =
      __$LoadedCopyWithImpl<$Res>;
  $Res call({List<Article> articleList});
}

/// @nodoc
class __$LoadedCopyWithImpl<$Res> extends _$NewsMainStateCopyWithImpl<$Res>
    implements _$LoadedCopyWith<$Res> {
  __$LoadedCopyWithImpl(_Loaded _value, $Res Function(_Loaded) _then)
      : super(_value, (v) => _then(v as _Loaded));

  @override
  _Loaded get _value => super._value as _Loaded;

  @override
  $Res call({
    Object articleList = freezed,
  }) {
    return _then(_Loaded(
      articleList == freezed
          ? _value.articleList
          : articleList as List<Article>,
    ));
  }
}

/// @nodoc
class _$_Loaded implements _Loaded {
  const _$_Loaded(this.articleList) : assert(articleList != null);

  @override
  final List<Article> articleList;

  @override
  String toString() {
    return 'NewsMainState.loaded(articleList: $articleList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Loaded &&
            (identical(other.articleList, articleList) ||
                const DeepCollectionEquality()
                    .equals(other.articleList, articleList)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(articleList);

  @JsonKey(ignore: true)
  @override
  _$LoadedCopyWith<_Loaded> get copyWith =>
      __$LoadedCopyWithImpl<_Loaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult empty(),
    @required TResult loaded(List<Article> articleList),
    @required TResult failure(),
  }) {
    assert(empty != null);
    assert(loaded != null);
    assert(failure != null);
    return loaded(articleList);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult empty(),
    TResult loaded(List<Article> articleList),
    TResult failure(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loaded != null) {
      return loaded(articleList);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult empty(_Empty value),
    @required TResult loaded(_Loaded value),
    @required TResult failure(_Failure value),
  }) {
    assert(empty != null);
    assert(loaded != null);
    assert(failure != null);
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult empty(_Empty value),
    TResult loaded(_Loaded value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class _Loaded implements NewsMainState {
  const factory _Loaded(List<Article> articleList) = _$_Loaded;

  List<Article> get articleList;
  @JsonKey(ignore: true)
  _$LoadedCopyWith<_Loaded> get copyWith;
}

/// @nodoc
abstract class _$FailureCopyWith<$Res> {
  factory _$FailureCopyWith(_Failure value, $Res Function(_Failure) then) =
      __$FailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$FailureCopyWithImpl<$Res> extends _$NewsMainStateCopyWithImpl<$Res>
    implements _$FailureCopyWith<$Res> {
  __$FailureCopyWithImpl(_Failure _value, $Res Function(_Failure) _then)
      : super(_value, (v) => _then(v as _Failure));

  @override
  _Failure get _value => super._value as _Failure;
}

/// @nodoc
class _$_Failure implements _Failure {
  const _$_Failure();

  @override
  String toString() {
    return 'NewsMainState.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Failure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult empty(),
    @required TResult loaded(List<Article> articleList),
    @required TResult failure(),
  }) {
    assert(empty != null);
    assert(loaded != null);
    assert(failure != null);
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult empty(),
    TResult loaded(List<Article> articleList),
    TResult failure(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult empty(_Empty value),
    @required TResult loaded(_Loaded value),
    @required TResult failure(_Failure value),
  }) {
    assert(empty != null);
    assert(loaded != null);
    assert(failure != null);
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult empty(_Empty value),
    TResult loaded(_Loaded value),
    TResult failure(_Failure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class _Failure implements NewsMainState {
  const factory _Failure() = _$_Failure;
}
