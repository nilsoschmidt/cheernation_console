import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cheernation_admin/core/services/injection.dart';
import 'package:cheernation_admin/data/models/News/article.dart';
import 'package:cheernation_admin/data/repositories/news_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'news_main_bloc.freezed.dart';

@freezed
abstract class NewsMainEvent with _$NewsMainEvent {
  const factory NewsMainEvent.loadNews() = _LoadNews;
  const factory NewsMainEvent.deleteArticle(String articleId) = _DeleteArticle;
  const factory NewsMainEvent.createNewArticle(
      String title, String subtitle, String text) = _CreateNewArticle;
  const factory NewsMainEvent.editArticle(
          String articleId, String title, String subtitle, String text) =
      _EditArticle;
}

@freezed
abstract class NewsMainState with _$NewsMainState {
  const factory NewsMainState.empty() = _Empty;
  const factory NewsMainState.loaded(List<Article> articleList) = _Loaded;
  const factory NewsMainState.failure() = _Failure;
}

class NewsMainBloc extends Bloc<NewsMainEvent, NewsMainState> {
  NewsMainBloc() : super(NewsMainState.empty());

  @override
  Stream<NewsMainState> mapEventToState(NewsMainEvent event) async* {
    if (event is _LoadNews) {
      yield* _mapLoadNewsToState();
    } else if (event is _DeleteArticle) {
      yield* _mapDeleteArticleToState(event.articleId);
    } else if (event is _CreateNewArticle) {
      yield* _mapCreateNewArticleToState(
          event.title, event.subtitle, event.text);
    } else if (event is _EditArticle) {
      yield* _mapEditArticleToState(event.articleId,
          event.title, event.subtitle, event.text);
    }
  }

  Stream<NewsMainState> _mapLoadNewsToState() async* {
    try {
      NewsRepository newsRepository = resolve();
      List<Article> articleList = await newsRepository.getArticleList();
      //Laden eines Users oder ähnliches
      if (articleList.isNotEmpty) {
        yield _Loaded(articleList);
      } else {
        yield _Failure();
      }
    } catch (_) {
      yield _Failure();
    }
  }

  Stream<NewsMainState> _mapDeleteArticleToState(String articleId) async* {
    try {
      NewsRepository newsRepository = resolve();
      newsRepository.deleteArticle(articleId);
    } catch (_) {}
  }

  Stream<NewsMainState> _mapCreateNewArticleToState(
      String title, String subtitle, String text) async* {
    try {
      NewsRepository newsRepository = resolve();
      newsRepository.createNewArticle(title, subtitle, text);
    } catch (_) {}
  }

  Stream<NewsMainState> _mapEditArticleToState(
      String articleId, String title, String subtitle, String text) async* {
    try {
      NewsRepository newsRepository = resolve();
      newsRepository.editArticle(articleId, title, subtitle, text);
    } catch (_) {}
  }
}
