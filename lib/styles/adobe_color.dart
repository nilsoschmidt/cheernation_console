import 'package:flutter/material.dart';

class AdobeColor {
  static Color splashscreenBackground =
      scaffoldBackground; //const Color(0xffEB4992);//const Color(0xffE91E63);//const Color(0xffEB4992);

  static const Color primary = const Color(0xffE91E63);
  static const Color lightPrimary = const Color(0xffF8BBD0);
  static const Color scaffoldBackground =
      Colors.white; //const Color(0xfffce4ec);
  static const Color grey_10 = const Color(0xffe5e5e5);
  static const Color white = const Color(0xffffffff);

  static const Color facebookBlue = const Color(0xff4267B2);

  static const LinearGradient gradient = LinearGradient(
      colors: [const Color(0xfff07875), const Color(0xff772b4b)]);
  static const LinearGradient clubGradient = LinearGradient(
      colors: [const Color(0xfff07875), const Color(0xff2f7af5)]);
  //static const LinearGradient gradient = LinearGradient(colors: [const Color(0xfff4a1aa), const Color(0xff772b4b)]);
  //static const LinearGradient gradient = LinearGradient(colors: [const Color(0xfff4a1aa), const Color(0xff6c263b)]);
  //static const LinearGradient gradient = LinearGradient(colors: [const Color(0xfff07875), const Color(0xfff44193)]);
  //Team Event Reaction Color
  static const Color eventAccepted = Colors.green;
  static const Color eventMaybed = Colors.amber;
  static const Color eventDeclined = Colors.redAccent;
}

class LightThemeColor {
  static const Color scaffoldBackground = const Color(0xfff4f4f8);
  static const Color background = Colors.white;
  static const Color primaryColor = const Color(0xff772b4b);
  //static const LinearGradient gradient = LinearGradient(colors: [const Color(0xfff78f71), const Color(0xfff44193)]);
}

class DarkThemeColor {
  static const Color scaffoldBackground = const Color(0xff171b24);
  static const Color background = const Color(0xff323542);
  static const Color primaryColor = const Color(0xfff44193);
  //static const LinearGradient gradient = LinearGradient(colors: [const Color(0xfff78f71), const Color(0xfff44193)]);
}
