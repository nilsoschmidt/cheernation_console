import 'package:auto_route/auto_route.dart';
import 'package:cheernation_admin/bloc/Authentication/authentication_bloc.dart';
import 'package:cheernation_admin/core/services/injection.dart';
import 'package:cheernation_admin/data/repositories/user_repository.dart';
import 'package:cheernation_admin/routes/router.gr.dart';
import 'package:cheernation_admin/ui/Login/login_page.dart';
import 'package:cheernation_admin/ui/Navigation_Main/pages/navigation_main_page.dart';
import 'package:cheernation_admin/ui/Splashscreen/pages/splashscreen_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

//final RouteObserver<PageRoute> globalRouteObserver = RouteObserver<PageRoute>();

class App extends StatelessWidget {
  //final AppLocalizationsDelegate localeOverrideDelegate;
  final UserRepository _userRepository = resolve();

  App();

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<AuthenticationBloc>(
              create: (context) =>
                  AuthenticationBloc(userRepository: _userRepository)
                    ..add(AuthenticationEvent.appStart()))
        ],
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Cheernation Konsole',
            home: StartupDecider(),
            builder: ExtendedNavigator.builder(router: AppRouter()),
          )
          // return widget here based on BlocA's state
        );
  }
}

class StartupDecider extends StatelessWidget {
  final UserRepository _userRepository = resolve();

  StartupDecider({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthenticationBloc, AuthenticationState>(
      builder: (context, state) {
        return state.when(
          uninitialized: () => SplashscreenPage(),
          unauthenticated: () => LoginPage(userRepository: _userRepository),
          authenticated: () => NavigationMainPage(),
        );
      },
    );
  }
}
