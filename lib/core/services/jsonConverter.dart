import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

class FirestoreTimestampConverter
    implements JsonConverter<DateTime, Timestamp> {
  const FirestoreTimestampConverter();

  @override
  DateTime fromJson(Timestamp timestamp) {
    if (timestamp != null) {
      return timestamp.toDate();
    } else {
      return null;
    }

    //return json.toDate();
    //return DateTime.fromMicrosecondsSinceEpoch(json);
    //return  json;
    //return FieldValue.serverTimestamp();
  }

  @override
  Timestamp toJson(DateTime dateTime) {
    if (dateTime != null) {
      return Timestamp.fromDate(dateTime);
    } else {
      return null;
    }
  }
}
