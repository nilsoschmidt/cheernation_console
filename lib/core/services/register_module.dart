import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:injectable/injectable.dart';

/*@registerModule
abstract class RegisterAsyncModule {
  Future<SharedPreferences> get sharedPreferences => SharedPreferences.getInstance();
}*/

@module
abstract class RegisterModule {
  FirebaseAuth get firebaseAuth => FirebaseAuth.instance;
  FirebaseFirestore get firestore => FirebaseFirestore.instance;
  FirebaseStorage get firebaseStorage =>
      FirebaseStorage(storageBucket: 'gs://cheernation-prod.appspot.com');

}
