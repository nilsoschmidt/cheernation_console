import 'package:intl/intl.dart';

String getDateWithDayName(DateTime date) {
    return DateFormat("EEEE dd.MM.yyyy", "de").format(date);
}

String getDate(DateTime date) {
    return DateFormat("dd.MM.yyyy", "de").format(date);
}

String getTime(DateTime date) {
    return DateFormat("HH:mm", "de").format(date);
}

String getDateForAddEvent(DateTime datum) {
    return DateFormat("EE HH:mm", "de").format(datum) +
        " Uhr, " +
        DateFormat("dd.MM.yyyy", "de").format(datum);
  }