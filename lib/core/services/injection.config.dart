// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import '../../data/repositories/club_repository.dart';
import '../../data/repositories/event_repository.dart';
import '../../data/repositories/news_repository.dart';
import 'register_module.dart';
import '../../data/repositories/team_repository.dart';
import '../../data/repositories/user_repository.dart';

/// adds generated dependencies
/// to the provided [GetIt] instance

GetIt $initGetIt(
  GetIt get, {
  String environment,
  EnvironmentFilter environmentFilter,
}) {
  final gh = GetItHelper(get, environment, environmentFilter);
  final registerModule = _$RegisterModule();
  gh.factory<FirebaseAuth>(() => registerModule.firebaseAuth);
  gh.factory<FirebaseFirestore>(() => registerModule.firestore);
  gh.factory<FirebaseStorage>(() => registerModule.firebaseStorage);

  // Eager singletons must be registered in the right order
  gh.singleton<NewsRepository>(NewsRepository(get<FirebaseFirestore>()));
  gh.singleton<TeamRepository>(TeamRepository(get<FirebaseFirestore>()));
  gh.singleton<UserRepository>(
      UserRepository(get<FirebaseAuth>(), get<FirebaseFirestore>()));
  gh.singleton<ClubRepository>(ClubRepository(get<FirebaseFirestore>()));
  gh.singleton<EventRepository>(EventRepository(get<FirebaseFirestore>()));
  return get;
}

class _$RegisterModule extends RegisterModule {}
