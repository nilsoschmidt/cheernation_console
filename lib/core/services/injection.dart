import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import 'injection.config.dart';

/*GetIt _getIt;

@injectableInit
void configureCommonsModule(GetIt getIt, {String environment}) {
  $initGetIt(getIt, environment: environment);
  _getIt = getIt;
}*/

final getIt = GetIt.instance;

@injectableInit
Future<void> configureInjection(String environment) async =>
    await $initGetIt(getIt, environment: environment);

abstract class Env {
  static const dev = "dev";
  static const prod = "prod";
}

T resolve<T>([String instanceName]) => getIt == null
    ? null
    : instanceName == null
        ? getIt<T>()
        : getIt.get<T>(instanceName: instanceName);
