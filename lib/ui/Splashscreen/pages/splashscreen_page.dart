import 'package:flutter/material.dart';

class SplashscreenPage extends StatefulWidget {
  const SplashscreenPage({Key key}) : super(key: key);

  @override
  _SplashscreenPageState createState() => _SplashscreenPageState();
}

class _SplashscreenPageState extends State<SplashscreenPage>
    with SingleTickerProviderStateMixin {
  Animation<double> opacity;
  AnimationController controller;

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    opacity = Tween<double>(begin: 0, end: 1).animate(controller)
      ..addListener(() {
        if (opacity.value == 1) {
          //controller.reverse();
        }
        setState(() {
          // The state that has changed here is the animation object’s value.
        });
      });
    controller.forward();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //backgroundColor: AdobeColor.splashscreenBackground,
        body: Opacity(
            opacity: 1,
            child: Stack(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  /*child: Image.asset(
                    "assets/cheernation_splashscreen.gif",
                    fit: BoxFit.cover,
                  ),*/
                ),
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.black.withAlpha(0),
                ),
                Positioned.fill(
                  child: Container(
                    decoration: BoxDecoration(
                        //color: Colors.black.withAlpha(150),
                        ),
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width / 10,
                        right: MediaQuery.of(context).size.width / 10,
                        bottom: MediaQuery.of(context).size.height / 6),
                    child: Image.asset('assets/cheernation_logo.png',
                      //height: MediaQuery.of(context).size.height / 3
                    ),
                  ),
                )
              ],
            )));
  }
}
