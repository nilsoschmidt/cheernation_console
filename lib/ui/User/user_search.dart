import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:cheernation_admin/bloc/User/user_main_bloc.dart';
import 'package:cheernation_admin/bloc/User/user_search_bloc.dart';
import 'package:cheernation_admin/data/models/User/user_short.dart';
import 'package:cheernation_admin/routes/router.gr.dart';
import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:cheernation_admin/ui/Widgets/team_profil_image_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class UserSearchPage extends StatelessWidget {
  const UserSearchPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: BlocProvider<UserSearchBloc>(
        create: (context) => UserSearchBloc(),
        child: UserSearchForm(),
      ),
    );
  }
}

class UserSearchForm extends StatefulWidget {
  UserSearchForm({Key key}) : super(key: key);

  @override
  UserSearchFormState createState() => UserSearchFormState();
}

class UserSearchFormState extends State<UserSearchForm>
    with WidgetsBindingObserver {
  final TextEditingController _userSearchTextController =
      TextEditingController();
  Timer _debounce;
  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    //BlocProvider.of<SearchBloc>(context).add(SearchEvent.changeString("Nils"));
    WidgetsBinding.instance.addObserver(this);
    _userSearchTextController.addListener(_onSearchTextChanged);
    super.initState();
  }

  @override
  void didChangeMetrics() {
    super.didChangeMetrics();
    final value = WidgetsBinding.instance.window.viewInsets.bottom;
    if (value == 0) {
      unfocusTextField();
    }
  }

  @override
  void dispose() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _userSearchTextController.removeListener(_onSearchTextChanged);
    _userSearchTextController.dispose();
    WidgetsBinding.instance.removeObserver(this);
    focusNode.dispose();
    super.dispose();
  }

  void _onSearchTextChanged() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      BlocProvider.of<UserSearchBloc>(context)
          .add(UserSearchEvent.changeString(_userSearchTextController.text));
    });
  }

  void unfocusTextField() {
    focusNode.unfocus();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          CustomAppBar('Search'),
          Container(
            padding: EdgeInsets.fromLTRB(24, 0, 6, 8),
            child: Row(
              children: <Widget>[
                //SizedBox(width: 24),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(),
                        borderRadius: BorderRadius.circular(20)),
                    padding: EdgeInsets.fromLTRB(24, 0, 0, 0),
                    child: TextField(
                        focusNode: focusNode,
                        //style: TextStyle(color:Colors.white ),
                        controller: _userSearchTextController,
                        decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 20),
                            //fillColor: Colors.white,
                            /*suffixIcon: IconButton(
                              icon: Icon(Icons.close),
                              onPressed: () {
                                BlocProvider.of<SearchBloc>(context)
                                    .add(SearchEvent.changeString(""));
                                _userSearchTextController.text = "";
                              },
                            ),*/
                            prefixIcon: Padding(
                                padding: EdgeInsets.fromLTRB(0, 2, 20, 0),
                                child: Icon(Icons.search, size: 36)),
                            border: InputBorder.none,
                            hintText: 'Suche')),
                    //autofocus: true,
                  ),
                ),

                BlocBuilder<UserSearchBloc, UserSearchState>(
                  builder: (context, state) {
                    return state.maybeWhen(
                        stringEmpty: () => Container(
                              width: 53,
                            ),
                        orElse: () => Container(
                            padding: EdgeInsets.only(left: 5),
                            child: IconButton(
                              icon: Icon(Icons.close,
                                  size: 30, color: Colors.redAccent),
                              onPressed: () {
                                BlocProvider.of<UserSearchBloc>(context)
                                    .add(UserSearchEvent.changeString(""));
                                _userSearchTextController.text = "";
                              },
                            )));
                  },
                ),
              ],
            ),
          ),
          Expanded(child: BlocBuilder<UserSearchBloc, UserSearchState>(
            builder: (context, state) {
              return state.when(
                  stringEmpty: () => getEmptyWidget(),
                  /*Container(
                      padding: EdgeInsets.only(top: 32),
                      child: Text('Tippe einen Namen unter "Suchen" ein')),*/
                  usersFound: (userList) => getListView(userList),
                  failure: () => Container(
                      padding: EdgeInsets.only(top: 32),
                      child: Text("Es wurden keine User gefunden.")),
                  loading: () => getLoadingWidget());
            },
          ))
        ],
      ),
    );
  }

  Widget getEmptyWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.people),
          Text('Search Users',
              style: TextStyle(
                fontSize: MediaQuery.of(context).size.width / 8,
              )),
          SizedBox(
            height: 85,
          )
        ],
      ),
    );
  }

  Widget getLoadingWidget() {
    return Container(
      height: 50,
      //width: MediaQuery.of(context).size.width / 2,
      padding: EdgeInsets.fromLTRB(32, 32, 16, 16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              padding: EdgeInsets.fromLTRB(0, 8, 32, 8),
              width: 80,
              height: 20,
              child: LinearProgressIndicator()),
          Container(
            width: MediaQuery.of(context).size.width / 3 * 2,
            child: Text(
                'Suche nach "${_userSearchTextController.text}" läuft ...'),
          )
        ],
      ),
    );
  }

  Widget getListView(List<UserShort> userShortList) {
    Size screenSize = MediaQuery.of(context).size;
    return Expanded(
        child: Container(
            // height: screenSize.height / 1.3,
            margin: EdgeInsets.all(5),
            // decoration: BoxDecoration(border: Border.all()),
            child: ListView.builder(
                // padding: EdgeInsets.fromLTRB(24, 16, 24, 16),
                itemCount: userShortList.length,
                itemBuilder: (BuildContext ctxt, int index) => UserSearchItem(
                      userShort: userShortList[index],
                      userSearchFormState: this,
                    ))));
  }
}

class UserSearchItem extends StatefulWidget {
  final UserSearchFormState userSearchFormState;
  final UserShort userShort;
  UserSearchItem(
      {Key key, @required this.userShort, @required this.userSearchFormState})
      : super(key: key);

  @override
  _UserSearchItemState createState() => _UserSearchItemState();
}

class _UserSearchItemState extends State<UserSearchItem> {
  @override
  Widget build(BuildContext context) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.25,
      secondaryActions: [
        IconSlideAction(
          caption: 'Message',
          color: Colors.blue,
          icon: Icons.message,
          onTap: () {
            ExtendedNavigator.of(context).popAndPush(
                Routes.sendNotificationPage,
                arguments: SendNotificationPageArguments(
                    userId: widget.userShort.authUserId));
          },
        ),
        IconSlideAction(
          caption: 'Delete',
          color: Colors.red,
          icon: Icons.delete,
          onTap: () {
            showDialog(
                context: context,
                builder: (BuildContext buildContext) {
                  return AlertDialog(
                    title: Text('Wirklich Löschen?'),
                    content: Text('Möchtest du dieses Team wirklich löschen?'),
                    actions: [
                      TextButton(
                          onPressed: () => Navigator.pop(buildContext),
                          child: Text('NEIN')),
                      TextButton(
                          onPressed: () {
                            UserMainBloc()
                              ..add(UserMainEvent.deleteUser(
                                  widget.userShort.authUserId));
                            for (var i = 0; i < 2; i++) {
                              ExtendedNavigator.of(context).pop();
                            }
                            ExtendedNavigator.of(context)
                                .popAndPush(Routes.userMainPage);
                          },
                          child: Text('JA'))
                    ],
                  );
                });
          },
        )
      ],
      child: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        height: 60,
        margin: EdgeInsets.all(5),
        // decoration: BoxDecoration(
        //   border: Border.all()
        // ),
        child: Row(
          children: [
            TeamProfilImageContainer(teamImageUrl: null/*widget.userShort.profilImageUrl*/, height: 64),
            SizedBox(width: 24),
            Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      child: Text(
                    widget.userShort.nickname,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )),
                  Container(
                      child: Text(widget.userShort.name != null
                          ? widget.userShort.name
                          : "null"))
                ])
          ],
        ),
      ),
    );
  }
}
