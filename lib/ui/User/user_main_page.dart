import 'package:auto_route/auto_route.dart';
import 'package:cheernation_admin/bloc/User/user_main_bloc.dart';
import 'package:cheernation_admin/data/models/User/user_short.dart';
import 'package:cheernation_admin/routes/router.gr.dart';
import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:cheernation_admin/ui/Widgets/loading_screen.dart';
import 'package:cheernation_admin/ui/Widgets/my_error_widget.dart';
import 'package:cheernation_admin/ui/Widgets/team_profil_image_container.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class UserMainPage extends StatelessWidget {
  const UserMainPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<UserMainBloc>(
        create: (context) => UserMainBloc()..add(UserMainEvent.loadUsers()),
        child: UserMainForm(),
      ),
    );
  }
}

class UserMainForm extends StatelessWidget {
  const UserMainForm({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return SafeArea(child:
        BlocBuilder<UserMainBloc, UserMainState>(builder: (context, state) {
      return state.when(
          loading: () => LoadingScreen(),
          loaded: (userList) => Column(
                children: [
                  CustomAppBar(
                    'User',
                    actions: [
                      IconButton(
                          icon: Icon(Icons.search),
                          onPressed: () => ExtendedNavigator.of(context)
                              .push(Routes.userSearchPage))
                    ],
                  ), //Teams Block
                  Expanded(
                    child: Container(
                        // height: screenSize.height,
                        margin: EdgeInsets.all(5),
                        child: ListView.builder(
                            itemCount: userList.length,
                            itemBuilder: (context, index) => UserListViewItem(
                                  userShort: userList[index],
                                ))),
                  )
                ],
              ),
          failure: () => MyErrorWidget());
    }));
  }
}

class UserListViewItem extends StatelessWidget {
  final UserShort userShort;

  const UserListViewItem({Key key, this.userShort}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.25,
      secondaryActions: [
        IconSlideAction(
          caption: 'Message',
          color: Colors.blue,
          icon: Icons.message,
          onTap: () {
            ExtendedNavigator.of(context).popAndPush(
                Routes.sendNotificationPage,
                arguments: SendNotificationPageArguments(
                    userId: userShort.authUserId));
          },
        ),
        IconSlideAction(
          caption: 'Delete',
          color: Colors.red,
          icon: Icons.delete,
          onTap: () {
            showDialog(
                context: context,
                builder: (BuildContext buildContext) {
                  return AlertDialog(
                    title: Text('Wirklich Löschen?'),
                    content: Text('Möchtest du dieses Team wirklich löschen?'),
                    actions: [
                      TextButton(
                          onPressed: () => Navigator.pop(buildContext),
                          child: Text('NEIN')),
                      TextButton(
                          onPressed: () {
                            UserMainBloc()
                              ..add(UserMainEvent.deleteUser(
                                  userShort.authUserId));
                            for (var i = 0; i < 2; i++) {
                              ExtendedNavigator.of(context).pop();
                            }
                            ExtendedNavigator.of(context)
                                .popAndPush(Routes.userMainPage);
                          },
                          child: Text('JA'))
                    ],
                  );
                });
          },
        )
      ],
      child: Container(
        width: screenSize.width * 0.8,
        height: 60,
        margin: EdgeInsets.all(5),
        // decoration: BoxDecoration(
        //   border: Border.all()
        // ),
        child: Row(
          children: [
            TeamProfilImageContainer(teamImageUrl: null, height: 64),
            SizedBox(width: 24),
            Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      child: Text(
                    userShort.nickname,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )),
                  Container(
                      child: Text(
                          userShort.name != null ? userShort.name : "null"))
                ])
          ],
        ),
      ),
    );
  }
}
