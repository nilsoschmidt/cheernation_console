import 'package:auto_route/auto_route.dart';
import 'package:cheernation_admin/bloc/User/send_notification_bloc.dart';
import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:cheernation_admin/ui/Widgets/my_error_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SendNotificationPage extends StatelessWidget {
  final String userId;

  const SendNotificationPage(this.userId, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<SendNotificationBloc>(
        create: (context) => SendNotificationBloc(),
        child: SendNotificationForm(userId),
      ),
    );
  }
}

class SendNotificationForm extends StatelessWidget {
  final String userId;

  const SendNotificationForm(this.userId, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return SafeArea(child:
        BlocBuilder<SendNotificationBloc, SendNotificationState>(
            builder: (context, state) {
      return state.when(
          finished: () => Column(
                children: [
                  CustomAppBar('Nachicht'),
                  Center(
                    child: Icon(
                      Icons.check,
                      color: Colors.green,
                    ),
                  ),
                  Text('GESENDET')
                ],
              ),
          empty: () {
            TextEditingController controller = TextEditingController();
            return Container(
              child: Column(
                children: [
                  CustomAppBar('Nachricht'), //Teams Block
                  Container(
                      height: screenSize.height / 1.5,
                      margin: EdgeInsets.all(10),
                      child: Container(
                        child: Column(
                          children: [
                            SizedBox(height: 50,),
                            Container(
                              padding: EdgeInsets.all(10),
                              child: TextFormField(
                                controller: controller,
                              ),
                            ),
                            SizedBox(height: 30,),
                            ElevatedButton(
                                onPressed: () {
                                  SendNotificationBloc()
                                    ..add(SendNotificationEvent.sendMessage(
                                        userId, controller.text));
                                  ExtendedNavigator.of(context).pop();
                                },
                                child: Text('OK'))
                          ],
                        ),
                      ))
                ],
              ),
            );
          },
          failure: () => MyErrorWidget());
    }));
  }
}
