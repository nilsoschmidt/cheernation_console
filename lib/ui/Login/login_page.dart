import 'package:cheernation_admin/bloc/Authentication/authentication_bloc.dart';
import 'package:cheernation_admin/data/repositories/user_repository.dart';
import 'package:cheernation_admin/bloc/Login/login_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginPage extends StatefulWidget {
  final UserRepository _userRepository;

  LoginPage({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  Animation<double> opacity;
  AnimationController controller;

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(duration: const Duration(seconds: 0), vsync: this);
    opacity = Tween<double>(begin: 0, end: 1).animate(controller)
      ..addListener(() {
        setState(() {
          // The state that has changed here is the animation object’s value.
        });
      });
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //backgroundColor: AdobeColor.splashscreenBackground,
        //appBar: AppBar(title: Text('Login')),
        body: Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          /*child: Image.asset(
            "assets/cheernation_splashscreen.gif",
            fit: BoxFit.cover,
          ),*/
        ),
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.black.withAlpha(0),
        ),
        Opacity(
          opacity: opacity.value,
          child: BlocProvider<LoginBloc>(
            create: (context) =>
                LoginBloc(userRepository: widget._userRepository),
            child: LoginForm(),
          ),
        )
      ],
    ));
  }
}

class LoginForm extends StatefulWidget {
  LoginForm({
    Key key,
  }) : super(key: key);

  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  LoginBloc _loginBloc;

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isLoginButtonEnabled(LoginState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state.isFailure) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text('Fehler beim einloggen'), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
        if (state.isSubmitting) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Logging In...'),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text('Eingeloggt'), Icon(Icons.error)],
                ),
                backgroundColor: Colors.green,
              ),
            );
          print("Login drin....");
          BlocProvider.of<AuthenticationBloc>(context)
              .add(AuthenticationEvent.loggedIn());
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          TextEditingController controller = TextEditingController();
          return Form(
            child: Column(
              children: <Widget>[
                Expanded(child: Container()),
                Text(
                  'Admin Panel',
                  style: TextStyle(fontSize: 40),
                ),
                SizedBox(
                  height: 16,
                ),
                Text(
                  'Login',
                  style: TextStyle(fontSize: 30),
                ),
                SizedBox(height: 90),
                Container(
                    padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                    height: 50,
                    child: TextFormField(
                      keyboardType: TextInputType.visiblePassword,
                      obscureText: true,
                      obscuringCharacter: '*',
                      decoration: InputDecoration(labelText: 'Password'),
                      controller: controller,
                    )),
                SizedBox(
                  height: 50,
                ),
                Container(
                  child: LoginButton(onPressed: () {
                    BlocProvider.of<LoginBloc>(context).add(
                        LoginEvent.loginWithCredentialsPressed(
                            email: 'cheernation.team@gmail.com',
                            password: controller.text));
                  }),
                ),
                Expanded(child: Container())
              ],
            ),
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onEmailChanged() {
    _loginBloc.add(
      LoginEvent.emailChanged(email: _emailController.text),
    );
  }

  void _onPasswordChanged() {
    _loginBloc.add(
      LoginEvent.passwordChanged(password: _passwordController.text),
    );
  }
}

class LoginButton extends StatelessWidget {
  final VoidCallback _onPressed;

  LoginButton({Key key, VoidCallback onPressed})
      : _onPressed = onPressed,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(),
      onPressed: _onPressed,
      child: Text('Login'),
    );
  }
}
