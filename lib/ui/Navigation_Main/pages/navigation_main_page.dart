import 'package:cheernation_admin/bloc/Navigation_Main/navigation_main_bloc.dart';
import 'package:cheernation_admin/ui/Main_Menu/main_menu_page.dart';
import 'package:cheernation_admin/ui/Splashscreen/pages/splashscreen_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NavigationMainPage extends StatelessWidget {
  const NavigationMainPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: BlocProvider<NavigationMainBloc>(
          create: (context) =>
              NavigationMainBloc()..add(NavigationMainEvent.changeToUser()),
          child: NavigationMainForm(),
        ));
  }
}

class NavigationMainForm extends StatefulWidget {
  NavigationMainForm({Key key}) : super(key: key);

  @override
  _NavigationMainFormState createState() => _NavigationMainFormState();
}

class _NavigationMainFormState extends State<NavigationMainForm> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<NavigationMainBloc, NavigationMainState>(
        listener: (context, state) {
      state.when(
        navMenu: () {},
        empty: () {},
        loading: () {},
      );
    }, child: BlocBuilder<NavigationMainBloc, NavigationMainState>(
      builder: (context, state) {
        return state.when(
            empty: () => Container(),
            loading: () => SplashscreenPage(),
            navMenu: () => MainMenuForm());
      },
    ));
  }
}