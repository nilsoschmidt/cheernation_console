import 'package:cheernation_admin/bloc/EXAMPLE/example_bloc.dart';
import 'package:cheernation_admin/ui/EXAMPLE/widgets/example_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ExamplePage extends StatelessWidget {
  const ExamplePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<ExampleBloc>(
        create: (context) => ExampleBloc()..add(ExampleEvent.appStart()),
        child: ExampleForm(),
      ),
    );
  }
}
