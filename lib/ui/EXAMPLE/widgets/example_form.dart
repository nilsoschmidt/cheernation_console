import 'package:cheernation_admin/bloc/EXAMPLE/example_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ExampleForm extends StatefulWidget {
  ExampleForm({Key key}) : super(key: key);

  @override
  _ExampleFormState createState() => _ExampleFormState();
}

class _ExampleFormState extends State<ExampleForm> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ExampleBloc, ExampleState>(
      builder: (context, state) {
        return state.when(
          uninitialized: () => Container(child: CircularProgressIndicator()),
          authenticated: () => Container(child: Text("Login succesfull.")),
          unauthenticated: () =>
              Container(child: Text("Error: You are not Logged in.")),
        );
      },
    );
  }
}
