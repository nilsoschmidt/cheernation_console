import 'package:auto_route/auto_route.dart';
import 'package:cheernation_admin/routes/router.gr.dart';
import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:flutter/material.dart';

class MainMenuPage extends StatelessWidget {
  const MainMenuPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MainMenuForm(),
    );
  }
}

class MainMenuForm extends StatefulWidget {
  MainMenuForm({Key key}) : super(key: key);

  @override
  _MainMenuFormState createState() => _MainMenuFormState();
}

class _MainMenuFormState extends State<MainMenuForm> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Center(
            child: Column(
      children: [
        CustomAppBar('Menu', back: false),
        SizedBox(height: 100),
        MenuItem(Routes.userMainPage, 'User verwalten'),
        MenuItem(Routes.clubsMainPage, 'Clubs verwalten'),
        MenuItem(Routes.eventsMainPage, 'Events verwalten'),
        MenuItem(Routes.newsMainPage, 'News verwalten')
      ],
    )));
  }
}

class MenuItem extends StatelessWidget {
  final String route;
  final String text;
  const MenuItem(this.route, this.text, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Container(
      width: screenSize.width / 1.5,
      child: RaisedButton(
        onPressed: () => ExtendedNavigator.of(context).push(route),
        child: Text(text),
      ),
    );
  }
}