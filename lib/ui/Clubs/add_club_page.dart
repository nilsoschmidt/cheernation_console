import 'package:cheernation_admin/bloc/Clubs/add_club_bloc.dart';
import 'package:cheernation_admin/data/models/Club/club_short.dart';
import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:cheernation_admin/ui/Widgets/loading_screen.dart';
import 'package:cheernation_admin/ui/Widgets/my_error_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

class AddClubPage extends StatelessWidget {
  const AddClubPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<AddClubBloc>(
        create: (context) => AddClubBloc(),
        child: AddClubForm(),
      ),
    );
  }
}

class AddClubForm extends StatelessWidget {
  const AddClubForm({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return BlocBuilder<AddClubBloc, AddClubState>(
      builder: (context, state) {
        TextEditingController controller1 = TextEditingController();
        TextEditingController controller2 = TextEditingController();
        TextEditingController controller3 = TextEditingController();
        return state.when(
          empty: () => SafeArea(
            child: Column(
              children: [
                CustomAppBar(
                  'Neuer Club',
                  actions: [
                    IconButton(
                        icon: Icon(Icons.check),
                        onPressed: () async {
                          String name = controller1.text;
                          Map<String, String> adress = {
                            'street': controller2.text,
                            'town': controller3.text
                          };
                          controller1.clear();
                          controller2.clear();
                          controller3.clear();
                          AddClubBloc()
                                ..add(AddClubEvent.createNewClub(name, adress));
                        })
                  ],
                ),
                Text('Name'),
                Container(
                  padding: EdgeInsets.fromLTRB(30, 50, 30, 0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      icon: Icon(Icons.group),
                      hintText: 'Vollständiger Name des Clubs',
                      labelText: 'Name',
                    ),
                    controller: controller1,
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(30, 20, 30, 0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      icon: Icon(Icons.traffic),
                      hintText: 'Straße Hausnummer',
                      labelText: 'Adresse 1',
                    ),
                    controller: controller2,
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(30, 20, 30, 0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      icon: Icon(Icons.location_city),
                      hintText: 'PLZ Ort',
                      labelText: 'Adresse 2',
                    ),
                    controller: controller3,
                  ),
                ),
              ],
            ),
          ),
          finished: () => Container(
            height: screenSize.height,
            width: screenSize.width,
            child: Column(
              children: [Icon(Icons.check)],
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
            ),
          ),
          failure: () => Container(
            height: screenSize.height,
            width: screenSize.width,
            child: Column(
              children: [Icon(Icons.error)],
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
            ),
          ),
        );
      },
    );
  }
}
