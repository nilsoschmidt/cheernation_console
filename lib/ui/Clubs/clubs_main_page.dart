import 'package:auto_route/auto_route.dart';
import 'package:cheernation_admin/bloc/Clubs/clubs_main_bloc.dart';
import 'package:cheernation_admin/data/models/Club/club_short.dart';
import 'package:cheernation_admin/routes/router.gr.dart';
import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:cheernation_admin/ui/Widgets/loading_screen.dart';
import 'package:cheernation_admin/ui/Widgets/my_error_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

class ClubsMainPage extends StatelessWidget {
  const ClubsMainPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<ClubsMainBloc>(
        create: (context) =>
            ClubsMainBloc()..add(ClubsMainEvent.loadClubList()),
        child: ClubsMainForm(),
      ),
    );
  }
}

class ClubsMainForm extends StatelessWidget {
  const ClubsMainForm({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ClubsMainBloc, ClubsMainState>(
      builder: (context, state) {
        return state.when(
            loading: () => LoadingScreen(),
            loaded: (clubList) => SafeArea(
                  child: Column(
                    children: [
                      CustomAppBar(
                        'Clubs',
                        actions: [
                          IconButton(
                            icon: Icon(
                              Icons.add,
                              size: 26,
                            ),
                            onPressed: () => ExtendedNavigator.of(context)
                                .push(Routes.addClubPage),
                          )
                        ],
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height - 90,
                        child: ListView.builder(
                            itemCount: clubList.length,
                            itemBuilder: (context, index) =>
                                ClubListViewItem(clubList[index])),
                      ),
                    ],
                  ),
                ),
            failure: () => MyErrorWidget());
      },
    );
  }
}

class ClubListViewItem extends StatelessWidget {
  final ClubShort clubShort;

  const ClubListViewItem(this.clubShort, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => ExtendedNavigator.of(context).push(Routes.clubDetailPage, arguments: ClubDetailPageArguments(clubShort: clubShort) ),
      child: Container(
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
        decoration: BoxDecoration(
          border: Border.all(),
          borderRadius: BorderRadius.circular(20)
        ),
        child: Center(child: Row(
          children: [
            Container(margin: EdgeInsets.fromLTRB(10, 0, 10, 0),height: 64, width: 64, decoration: BoxDecoration(border: Border.all(), borderRadius: BorderRadius.circular(50)),),
            FittedBox(
              fit: BoxFit.fitWidth,
              child: Text(clubShort.clubName)),
          ],
        )),
      ),
    );
  }
}
