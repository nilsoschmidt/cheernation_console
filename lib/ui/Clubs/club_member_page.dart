import 'package:auto_route/auto_route.dart';
import 'package:cheernation_admin/bloc/Clubs/club_member_bloc.dart';
import 'package:cheernation_admin/bloc/Clubs/team_detail_bloc.dart';
import 'package:cheernation_admin/data/models/Club/club_member_short.dart';
import 'package:cheernation_admin/data/models/Club/club_short.dart';
import 'package:cheernation_admin/data/models/Team/team_member_short.dart';
import 'package:cheernation_admin/data/models/Team/team_short.dart';
import 'package:cheernation_admin/routes/router.gr.dart';
import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:cheernation_admin/ui/Widgets/loading_screen.dart';
import 'package:cheernation_admin/ui/Widgets/my_error_widget.dart';
import 'package:cheernation_admin/ui/Widgets/team_profil_image_container.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ClubMemberPage extends StatelessWidget {
  final ClubShort clubShort;

  const ClubMemberPage({Key key, this.clubShort}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocProvider<ClubMemberBloc>(
      create: (context) =>
          ClubMemberBloc()..add(ClubMemberEvent.loadClubMembers(clubShort)),
      child: ClubMemberForm(clubShort),
    ));
  }
}

class ClubMemberForm extends StatelessWidget {
  final ClubShort clubShort;

  const ClubMemberForm(this.clubShort, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return BlocBuilder<ClubMemberBloc, ClubMemberState>(
        builder: (context, state) {
      return state.when(
          loading: () => LoadingScreen(),
          loaded: (clubMemberList) => Column(
                children: [
                  CustomAppBar(clubShort.clubName), //Teams Block
                  Container(
                      height: screenSize.height / 1.5,
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(border: Border.all()),
                      child: clubMemberList.isNotEmpty
                          ? ListView.builder(
                              itemCount: clubMemberList.length,
                              itemBuilder: (context, index) =>
                                  ClubMemberListViewItem(
                                    clubMemberShort: clubMemberList[index],
                                    clubShort: clubShort,
                                  ))
                          : Container())
                ],
              ),
          failure: () => MyErrorWidget());
    });
  }
}

class ClubMemberListViewItem extends StatelessWidget {
  final ClubShort clubShort;
  final ClubMemberShort clubMemberShort;

  const ClubMemberListViewItem({Key key, this.clubMemberShort, this.clubShort})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        print(clubMemberShort.nachname);
      },
      child: Container(
        width: screenSize.width * 0.8,
        height: 50,
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            border: Border.all(), borderRadius: BorderRadius.circular(20)),
        child: Row(
          children: [
            TeamProfilImageContainer(
                teamImageUrl: clubMemberShort.userImageUrl, height: 64),
            Text(
              clubMemberShort.vorname + ' ' + clubMemberShort.nachname,
              textAlign: TextAlign.center,
            ),
            Expanded(child: Container()),
            IconButton(
              icon: Icon(
                  clubMemberShort.isAdmin ? Icons.star : Icons.star_border),
              onPressed: () {
                ClubMemberBloc()
                  ..add(ClubMemberEvent.setAdmin(clubMemberShort));
                ExtendedNavigator.of(context).popAndPush(Routes.clubMemberPage,
                    arguments: ClubMemberPageArguments(clubShort: clubShort));
              },
            ),
            SizedBox(
              width: 20,
            )
          ],
        ),
      ),
    );
  }
}
