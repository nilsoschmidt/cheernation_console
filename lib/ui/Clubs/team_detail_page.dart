import 'package:auto_route/auto_route.dart';
import 'package:cheernation_admin/bloc/Clubs/team_detail_bloc.dart';
import 'package:cheernation_admin/data/models/Club/club_short.dart';
import 'package:cheernation_admin/data/models/Team/team_member_short.dart';
import 'package:cheernation_admin/data/models/Team/team_short.dart';
import 'package:cheernation_admin/routes/router.gr.dart';
import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:cheernation_admin/ui/Widgets/loading_screen.dart';
import 'package:cheernation_admin/ui/Widgets/my_error_widget.dart';
import 'package:cheernation_admin/ui/Widgets/team_profil_image_container.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TeamDetailPage extends StatelessWidget {
  final ClubShort clubShort;
  final TeamShort teamShort;

  const TeamDetailPage({Key key, this.teamShort, this.clubShort}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocProvider<TeamDetailBloc>(
      create: (context) =>
          TeamDetailBloc()..add(TeamDetailEvent.loadTeamMembers(teamShort)),
      child: TeamDetailForm(teamShort,clubShort),
    ));
  }
}

class TeamDetailForm extends StatelessWidget {
  final ClubShort clubShort;
  final TeamShort teamShort;

  const TeamDetailForm(this.teamShort, this.clubShort, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return BlocBuilder<TeamDetailBloc, TeamDetailState>(
        builder: (context, state) {
      print(teamShort.teamCategory);
      return state.when(
          loading: () => LoadingScreen(),
          loaded: (teamMemberList) => Column(
                children: [
                  CustomAppBar(teamShort.teamName, actions: [
                    IconButton(
                        onPressed: () => showDeleteDialog(
                              context, teamShort.clubId, teamShort.teamId),
                        icon: Icon(Icons.delete_forever))
                  ]),
                  Expanded(
                    //Detail Block
                    child: Container(
                      child: Column(
                        children: [
                          Center(
                            child: TeamProfilImageContainer(
                              teamImageUrl: teamShort.teamImageUrl,
                              height: 128,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('Name: '),
                                Container(
                                  width: screenSize.width / 2,
                                ),
                                Text(teamShort.teamName),
                              ],
                            ),
                          ),
                          teamShort.teamCategory != null
                              ? Container(
                                  padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text('Kategorie: '),
                                      Container(
                                        width: screenSize.width / 2,
                                      ),
                                      Text(teamShort.teamCategory),
                                    ],
                                  ),
                                )
                              : Container(),
                          Container(
                            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('Invite: '),
                                Container(
                                  width: screenSize.width / 2,
                                ),
                                IconButton(
                                    icon: Icon(Icons.copy),
                                    onPressed: () => FlutterClipboard.copy(
                                        teamShort.teamInviteLink))
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  //Teams Block
                  Container(
                      height: screenSize.height / 2.5,
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(border: Border.all()),
                      child: teamMemberList.isNotEmpty
                          ? ListView.builder(
                              itemCount: teamMemberList.length,
                              itemBuilder: (context, index) =>
                                  TeamMemberListViewItem(
                                teamMemberShort: teamMemberList[index],
                                teamMemberList: teamMemberList,
                                teamShort: teamShort,
                              ),
                            )
                          : Container())
                ],
              ),
          failure: () => MyErrorWidget());
    });
  }

  showDeleteDialog(BuildContext context, String clubId, teamId) {
    showDialog(
        context: context,
        builder: (BuildContext buildContext) {
          return AlertDialog(
            title: Text('Wirklich Löschen?'),
            content: Text('Möchtest du dieses Team wirklich löschen?'),
            actions: [
              TextButton(
                  onPressed: () => ExtendedNavigator.of(context).pop(buildContext),
                  child: Text('NEIN')),
              TextButton(
                  onPressed: () {
                    TeamDetailBloc()
                      ..add(TeamDetailEvent.deleteTeam(clubId, teamId));
                    ExtendedNavigator.of(context).pop();
                  },
                  child: Text('JA'))
            ],
          );
        });
  }
}

class TeamMemberListViewItem extends StatelessWidget {
  final TeamShort teamShort;
  final TeamMemberShort teamMemberShort;
  final List<TeamMemberShort> teamMemberList;

  const TeamMemberListViewItem(
      {Key key, this.teamMemberShort, this.teamMemberList, this.teamShort})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        print(teamMemberShort.nachname);
      },
      child: Container(
        width: screenSize.width * 0.8,
        height: 50,
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            border: Border.all(), borderRadius: BorderRadius.circular(20)),
        child: Row(
          children: [
            TeamProfilImageContainer(
                teamImageUrl: teamMemberShort.userImageUrl, height: 64),
            Text(
              teamMemberShort.vorname + ' ' + teamMemberShort.nachname,
              textAlign: TextAlign.center,
            ),
            Expanded(child: Container()),
            IconButton(
                icon: Icon(
                    teamMemberShort.isAdmin ? Icons.star : Icons.star_border),
                onPressed: () {
                  TeamDetailBloc()
                    ..add(TeamDetailEvent.setAdmin(
                        teamMemberShort, teamMemberList));
                  ExtendedNavigator.of(context).popAndPush(
                      Routes.teamDetailPage,
                      arguments: TeamDetailPageArguments(teamShort: teamShort));
                }),
            SizedBox(
              width: 20,
            )
          ],
        ),
      ),
    );
  }
}
