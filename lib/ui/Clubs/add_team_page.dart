import 'package:auto_route/auto_route.dart';
import 'package:cheernation_admin/bloc/Clubs/add_team_bloc.dart';
import 'package:cheernation_admin/data/models/Club/club_short.dart';
import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AddTeamPage extends StatelessWidget {
  final ClubShort clubShort;
  const AddTeamPage(this.clubShort, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<AddTeamBloc>(
        create: (context) => AddTeamBloc(),
        child: AddTeamForm(clubShort),
      ),
    );
  }
}

class AddTeamForm extends StatefulWidget {
  final ClubShort clubShort;
  AddTeamForm(this.clubShort, {Key key}) : super(key: key);

  @override
  _AddTeamFormState createState() => _AddTeamFormState();
}

class _AddTeamFormState extends State<AddTeamForm> {
  TextEditingController controller1 = TextEditingController();
  String dropdownValue1 = 'Senior';
  String dropdownValue2 = 'Coed';
  String dropdownValue3 = '0';

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return BlocBuilder<AddTeamBloc, AddTeamState>(
      builder: (context, state) {
        return state.when(
          empty: () => SafeArea(
            child: Column(
              children: [
                CustomAppBar(
                  'Neues Team',
                  actions: [
                    IconButton(
                        icon: Icon(Icons.check),
                        onPressed: () async {
                          String name = controller1.text;
                          String category = dropdownValue1[0] +
                              dropdownValue2[0] +
                              dropdownValue3;
                          controller1.clear();
                          AddTeamBloc()
                            ..add(AddTeamEvent.createNewTeam(
                                widget.clubShort.clubId,
                                name,
                                'https://via.placeholder.com/150/171b24/ff0266?text=Empty',
                                category.toUpperCase()));
                          ExtendedNavigator.of(context).pop();
                        })
                  ],
                ),
                Text('Name'),
                Container(
                  padding: EdgeInsets.fromLTRB(30, 50, 30, 0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      icon: Icon(Icons.group),
                      hintText: 'Vollständiger Name des Teams',
                      labelText: 'Name',
                    ),
                    controller: controller1,
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                        padding: EdgeInsets.all(5),
                        margin: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.black,
                                width: 2,
                                style: BorderStyle.solid)),
                        child: DropdownButton<String>(
                          value: dropdownValue1,
                          icon: Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          isDense: true,
                          underline: Container(),
                          hint: Text('Age'),
                          onChanged: (String newValue) {
                            setState(() {
                              dropdownValue1 = newValue;
                            });
                          },
                          items: <String>[
                            '',
                            'Peewee',
                            'Junior',
                            'Senior',
                            'All-Star'
                          ].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        margin: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.black,
                                width: 2,
                                style: BorderStyle.solid)),
                        child: DropdownButton<String>(
                          value: dropdownValue2,
                          icon: Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          isDense: true,
                          underline: Container(),
                          hint: Text('Kategorie'),
                          onChanged: (String newValue) {
                            setState(() {
                              dropdownValue2 = newValue;
                            });
                          },
                          items: <String>['','All-Girl', 'Coed', 'Limited Coed']
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        margin: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.black,
                                width: 2,
                                style: BorderStyle.solid)),
                        child: DropdownButton<String>(
                          value: dropdownValue3,
                          icon: Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          isDense: true,
                          underline: Container(),
                          hint: Text('Level'),
                          onChanged: (String newValue) {
                            setState(() {
                              dropdownValue3 = newValue;
                            });
                          },
                          items: <String>[
                            '',
                            '0',
                            '1',
                            '2',
                            '3',
                            '4',
                            '5',
                            '6',
                            '7'
                          ].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        )),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
          finished: () => Container(
            height: screenSize.height,
            width: screenSize.width,
            child: Column(
              children: [Icon(Icons.check)],
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
            ),
          ),
          failure: () => Container(
            height: screenSize.height,
            width: screenSize.width,
            child: Column(
              children: [Icon(Icons.error)],
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
            ),
          ),
        );
      },
    );
  }
}
