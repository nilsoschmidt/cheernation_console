import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cheernation_admin/bloc/Clubs/club_detail_bloc.dart';
import 'package:cheernation_admin/data/models/Club/club_short.dart';
import 'package:cheernation_admin/data/models/Team/team_short.dart';
import 'package:cheernation_admin/routes/router.gr.dart';
import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:cheernation_admin/ui/Widgets/loading_screen.dart';
import 'package:cheernation_admin/ui/Widgets/my_error_widget.dart';
import 'package:cheernation_admin/ui/Widgets/team_profil_image_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maps_launcher/maps_launcher.dart';

class ClubDetailPage extends StatelessWidget {
  final ClubShort clubShort;

  const ClubDetailPage({Key key, this.clubShort}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocProvider<ClubDetailBloc>(
      create: (context) =>
          ClubDetailBloc()..add(ClubDetailEvent.loadTeamList(clubShort.clubId)),
      child: ClubDetailForm(clubShort),
    ));
  }
}

class ClubDetailForm extends StatelessWidget {
  final ClubShort clubShort;

  const ClubDetailForm(this.clubShort, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return BlocBuilder<ClubDetailBloc, ClubDetailState>(
        builder: (context, state) {
      return state.when(
          loading: () => LoadingScreen(),
          loaded: (teamList) => Column(
                children: [
                  CustomAppBar(
                    clubShort.clubName,
                    actions: [
                      IconButton(
                        onPressed: () =>
                            showDeleteDialog(context, clubShort.clubId),
                        icon: Icon(Icons.delete_forever),
                      )
                    ],
                  ),
                  Expanded(
                    child: Container(
                      child: Column(
                        children: [
                          Container(
                            padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('Name: '),
                                // Expanded(child: Container()),
                                Text(clubShort.clubName),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('Adresse:'),
                                // Expanded(child: Container()),
                                Text(clubShort.clubAdress['street'] +
                                    '\n' +
                                    clubShort.clubAdress['town']),
                                IconButton(
                                    icon: Icon(Icons.map),
                                    onPressed: () => MapsLauncher.launchQuery(
                                        clubShort.clubAdress['street'] +
                                            ',' +
                                            clubShort.clubAdress['town']))
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                    height: 50,
                    decoration: BoxDecoration(border: Border.all()),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        GestureDetector(
                          onTap: () => ExtendedNavigator.of(context).push(
                              Routes.clubMemberPage,
                              arguments: ClubMemberPageArguments(
                                  clubShort: clubShort)),
                          child: Container(
                              width: 30,
                              height: 30,
                              child: Icon(
                                Icons.group,
                                size: 30,
                              )),
                        ),
                        Expanded(child: Container()),
                        GestureDetector(
                          child: Container(
                              width: 30,
                              height: 30,
                              child: Icon(
                                Icons.autorenew,
                                size: 30,
                              )),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        GestureDetector(
                          onTap: () => ExtendedNavigator.of(context).push(
                              Routes.addTeamPage,
                              arguments:
                                  AddTeamPageArguments(clubShort: clubShort)),
                          child: Container(
                              width: 30,
                              height: 30,
                              child: Icon(
                                Icons.group_add,
                                size: 30,
                              )),
                        ),
                        SizedBox(
                          width: 10,
                        )
                      ],
                    ),
                  ),
                  Container(
                      height: screenSize.height / 2,
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                      decoration: BoxDecoration(border: Border.all()),
                      child: teamList.isNotEmpty
                          ? ListView.builder(
                              itemCount: teamList.length,
                              itemBuilder: (context, index) =>
                                  TeamListViewItem(teamShort: teamList[index]),
                            )
                          : Container())
                ],
              ),
          failure: () => MyErrorWidget());
    });
  }

  showDeleteDialog(BuildContext context, String clubId) {
    showDialog(
        context: context,
        builder: (BuildContext buildContext) {
          return AlertDialog(
            title: Text('Wirklich Löschen?'),
            content: Text('Möchtest du dieses Team wirklich löschen?'),
            actions: [
              TextButton(
                  onPressed: () => Navigator.pop(buildContext),
                  child: Text('NEIN')),
              TextButton(
                  onPressed: () {
                    ClubDetailBloc()..add(ClubDetailEvent.deleteClub(clubId));
                    ExtendedNavigator.of(context)
                        .popUntilRoot();
                  },
                  child: Text('JA'))
            ],
          );
        });
  }
}

class TeamListViewItem extends StatelessWidget {
  final TeamShort teamShort;

  const TeamListViewItem({Key key, this.teamShort}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        ExtendedNavigator.of(context).push(Routes.teamDetailPage,
            arguments: TeamDetailPageArguments(teamShort: teamShort));
      },
      child: Container(
        width: screenSize.width * 0.8,
        height: 50,
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            border: Border.all(), borderRadius: BorderRadius.circular(20)),
        child: Row(
          children: [
            // TeamProfilImageContainer(teamImageUrl: teamShort.teamImageUrl, height: 64),
            /* Container(
                  decoration: BoxDecoration(shape: BoxShape.circle),
                  child: CachedNetworkImage(
                    imageUrl: teamShort.teamImageUrl,
                    errorWidget: (context, e, _) => Icon(Icons.error),
                  )), */
            Padding(
              padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
              child: Text(
                teamShort.teamName,
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(child: Container()),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
              child: Text(teamShort.teamCategory),
            )
          ],
        ),
      ),
    );
  }
}
