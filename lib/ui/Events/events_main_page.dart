import 'package:auto_route/auto_route.dart';
import 'package:cheernation_admin/bloc/Events/events_main_bloc.dart';
import 'package:cheernation_admin/data/models/Events/championship.dart';
import 'package:cheernation_admin/routes/router.gr.dart';
import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:cheernation_admin/ui/Widgets/loading_screen.dart';
import 'package:cheernation_admin/ui/Widgets/my_error_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EventsMainPage extends StatelessWidget {
  const EventsMainPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: BlocProvider<EventsMainBloc>(
          create: (context) =>
              EventsMainBloc()..add(EventsMainEvent.loadEvents()),
          child: EventsMainForm(),
        ),
      ),
    );
  }
}

class EventsMainForm extends StatelessWidget {
  const EventsMainForm({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EventsMainBloc, EventsMainState>(
      builder: (context, state) {
        return state.when(
            empty: () => LoadingScreen(),
            failure: () => MyErrorWidget(),
            loaded: (championshipList) {
              return Column(
                children: [
                  CustomAppBar(
                    'Events',
                    actions: [
                      IconButton(
                        icon: Icon(Icons.add),
                        onPressed: () {
                          ExtendedNavigator.of(context)
                              .push(Routes.addEventPage);
                        },
                      )
                    ],
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height - 90,
                    child: ListView.builder(
                      itemCount: championshipList.length,
                      itemBuilder: (context, index) =>
                          ChampionshipListViewItem(championshipList[index]),
                    ),
                  ),
                ],
              );
            });
      },
    );
  }
}

class ChampionshipListViewItem extends StatelessWidget {
  final Championship championship;

  const ChampionshipListViewItem(this.championship, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => showDetailDialog(context, championship),
      child: Container(
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
        decoration: BoxDecoration(
            border: Border.all(), borderRadius: BorderRadius.circular(20)),
        child: Center(
            child: Row(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              height: 64,
              width: 64,
              decoration: BoxDecoration(
                  border: Border.all(),
                  borderRadius: BorderRadius.circular(50)),
            ),
            Text(championship.name.replaceFirst(
                new RegExp(r'meisterschaft '), 'meisterschaft\n')),
          ],
        )),
      ),
    );
  }
}

showDetailDialog(BuildContext context, Championship championship) {
  showDialog(
      context: context,
      barrierColor: Colors.white.withOpacity(0.8),
      builder: (context) => Container(
            child: Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                Text(
                  championship.name,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 30,
                    color: Colors.black,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  championship.location,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 30,
                    color: Colors.black,
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                Text(
                  championship.date.day.toString() +
                      '.' +
                      championship.date.month.toString() +
                      '.' +
                      championship.date.year.toString(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 30,
                    color: Colors.black,
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                ElevatedButton(
                    onPressed: null
                    //() {
                    // ExtendedNavigator.of(context).push(Routes.editEventPage,
                    //     arguments: EditEventPageArguments(
                    //         championship: championship));
                    //}
                    ,
                    child: Text('Edit')),
                SizedBox(
                  height: 50,
                ),
                ElevatedButton(
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext buildContext) {
                            return AlertDialog(
                              title: Text('Wirklich Löschen?'),
                              content: Text(
                                  'Möchtest du dieses Event wirklich löschen?'),
                              actions: [
                                TextButton(
                                    onPressed: () =>
                                        Navigator.pop(buildContext),
                                    child: Text('NEIN')),
                                TextButton(
                                    onPressed: () {
                                      EventsMainBloc()
                                        ..add(EventsMainEvent.deleteEvent(
                                            championship.eventId));
                                      for (var i = 0; i < 2; i++) {
                                        ExtendedNavigator.of(context).pop();
                                      }
                                      ExtendedNavigator.of(context)
                                          .popAndPush(Routes.eventsMainPage);
                                    },
                                    child: Text('JA'))
                              ],
                            );
                          });
                    },
                    child: Text('Delete')),
              ],
            ),
          ));
}
