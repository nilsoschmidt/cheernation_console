import 'package:auto_route/auto_route.dart';
import 'package:cheernation_admin/bloc/Clubs/add_team_bloc.dart';
import 'package:cheernation_admin/bloc/Events/add_event_bloc.dart';
import 'package:cheernation_admin/data/models/Club/club_short.dart';
import 'package:cheernation_admin/data/models/Events/championship.dart';
import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AddEventPage extends StatelessWidget {
  const AddEventPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<AddEventBloc>(
        create: (context) => AddEventBloc(),
        child: AddEventForm(),
      ),
    );
  }
}

class AddEventForm extends StatefulWidget {
  AddEventForm({Key key}) : super(key: key);

  @override
  _AddEventFormState createState() => _AddEventFormState();
}

class _AddEventFormState extends State<AddEventForm> {
  TextEditingController nameController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  DateTime date = DateTime.now();
  String typeDropdownValue = 'verbandsmeisterschaft';

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return BlocBuilder<AddEventBloc, AddEventState>(
      builder: (context, state) {
        return state.when(
          empty: () => SafeArea(
            child: Column(
              children: [
                CustomAppBar(
                  'Neues Event',
                  actions: [
                    IconButton(
                        icon: Icon(Icons.check),
                        onPressed: () async {
                          Championship championship = Championship(
                              name: nameController.text,
                              location: locationController.text,
                              type: typeDropdownValue.toLowerCase(),
                              date: date);
                          AddEventBloc()
                            ..add(AddEventEvent.addEvent(championship));
                          ExtendedNavigator.of(context).pop();
                        })
                  ],
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(30, 50, 30, 0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      icon: Icon(Icons.group),
                      hintText: 'Vollständiger Name des Events',
                      labelText: 'Name',
                    ),
                    controller: nameController,
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(30, 50, 30, 0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      icon: Icon(Icons.group),
                      hintText: 'Ort des Events',
                      labelText: 'Ort',
                    ),
                    controller: locationController,
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,children: [
                  Container(
                    width: screenSize.width / 2,
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(textAlign: TextAlign.center,readOnly: true, controller: TextEditingController(text: date.day.toString() + '.' + date.month.toString() + '.' + date.year.toString()),),
                  ),
                  ElevatedButton(onPressed: () => _selectDate(context),child: Text('Pick Date'),)
                ],),
                SizedBox(height: 40,),
                Container(
                        padding: EdgeInsets.all(5),
                        margin: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.black,
                                width: 2,
                                style: BorderStyle.solid)),
                        child: DropdownButton<String>(
                          value: typeDropdownValue,
                          icon: Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          isDense: true,
                          underline: Container(),
                          hint: Text('Type'),
                          onChanged: (String newValue) {
                            setState(() {
                              typeDropdownValue = newValue;
                            });
                          },
                          items: <String>[
                            'verbandsmeisterschaft',
                            'offene',
                            'international',
                            ].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        )),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
    context: context,
    initialDate: date,// Refer step 1
    firstDate: DateTime(2000),
    lastDate: DateTime(2025),
  );
  if (picked != null && picked != date)
    setState(() {
      date = picked;
    });
  }

  
}
