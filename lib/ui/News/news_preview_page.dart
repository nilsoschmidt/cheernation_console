import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

class NewsPreviewPage extends StatelessWidget {
  final String text;

  const NewsPreviewPage(this.text, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(text);
    ScrollController controller = ScrollController();
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            CustomAppBar('Preview'),
            Divider(indent: 0, color: Colors.grey,),
            Expanded(
              child: Markdown(
                controller: controller,
                data: text != null ? text : '',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
