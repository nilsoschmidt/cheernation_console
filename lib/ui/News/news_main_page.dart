import 'package:auto_route/auto_route.dart';
import 'package:cheernation_admin/bloc/News/news_main_bloc.dart';
import 'package:cheernation_admin/data/models/News/article.dart';
import 'package:cheernation_admin/routes/router.gr.dart';
import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:cheernation_admin/ui/Widgets/loading_screen.dart';
import 'package:cheernation_admin/ui/Widgets/my_error_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NewsMainPage extends StatelessWidget {
  const NewsMainPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: BlocProvider<NewsMainBloc>(
          create: (context) => NewsMainBloc()..add(NewsMainEvent.loadNews()),
          child: NewsMainForm(),
        ),
      ),
    );
  }
}

class NewsMainForm extends StatelessWidget {
  const NewsMainForm({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewsMainBloc, NewsMainState>(
      builder: (context, state) {
        return state.when(
            empty: () => LoadingScreen(),
            failure: () => MyErrorWidget(),
            loaded: (articleList) {
              return Column(
                children: [
                  CustomAppBar(
                    'News',
                    actions: [
                      IconButton(
                        icon: Icon(Icons.add),
                        onPressed: () {
                          ExtendedNavigator.of(context)
                              .push(Routes.newsAddPage);
                        },
                      )
                    ],
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height - 90,
                    child: ListView.builder(
                      itemCount: articleList.length,
                      itemBuilder: (context, index) =>
                          ArticleListViewItem(articleList[index]),
                    ),
                  ),
                ],
              );
            });
      },
    );
  }
}

class ArticleListViewItem extends StatelessWidget {
  final Article article;

  const ArticleListViewItem(this.article, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => ExtendedNavigator.of(context).push(Routes.newsDetailPage,
          arguments: NewsDetailPageArguments(article: article)),
      child: Container(
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        decoration: BoxDecoration(
            border: Border.all(), borderRadius: BorderRadius.circular(20)),
        child: Center(
            child: Row(
          children: [
            Container(
              height: 64,
              width: 64,
              child: Icon(Icons.article),
            ),
            Text(article.title + '\n\nby ' + article.author),
          ],
        )),
      ),
    );
  }
}
