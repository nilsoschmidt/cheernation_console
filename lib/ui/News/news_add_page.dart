import 'package:auto_route/auto_route.dart';
import 'package:cheernation_admin/bloc/News/news_main_bloc.dart';
import 'package:cheernation_admin/routes/router.gr.dart';
import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:flutter/material.dart';

class NewsAddPage extends StatelessWidget {
  const NewsAddPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: NewsAddForm(),
    );
  }
}

class NewsAddForm extends StatefulWidget {
  NewsAddForm({Key key}) : super(key: key);

  @override
  _NewsAddFormState createState() => _NewsAddFormState();
}

class _NewsAddFormState extends State<NewsAddForm> {
  TextEditingController _textController = TextEditingController();
  final _scrollbarController = ScrollController();
  String title;
  String subtitle;

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
        body: Column(children: [
      CustomAppBar(
        'Artikel schreiben',
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.preview),
              onPressed: () => ExtendedNavigator.of(context).push(
                  Routes.newsPreviewPage,
                  arguments: NewsPreviewPageArguments(text: _textController.value.text))),
          IconButton(
            icon: Icon(Icons.save),
            //TODO RAusfinden warum der den scheiß Text nicht kriegt
            onPressed: () async {
              openArticleDialog(context, _textController.value.text);
            },
          )
        ],
      ),
      Flexible(
        child: Padding(
          padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
          child: TextFormField(
            maxLines: 100,
            autofocus: true,
            controller: _textController,
          ),
        ),
      ),
      SizedBox(
        height: 64,
        child: Scrollbar(
          controller: _scrollbarController,
          isAlwaysShown: true,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(
                tooltip: 'Heading1',
                icon: Icon(Icons.title),
                onPressed: () => _surroundTextSelection(
                  '# ',
                  '',
                ),
              ),
              IconButton(
                tooltip: 'Bold',
                icon: Icon(Icons.format_bold),
                onPressed: () => _surroundTextSelection(
                  '**',
                  '**',
                ),
              ),
              IconButton(
                tooltip: 'Underline',
                icon: Icon(Icons.format_italic),
                onPressed: () => _surroundTextSelection(
                  '__',
                  '__',
                ),
              ),
              IconButton(
                tooltip: 'Code',
                icon: Icon(Icons.code),
                onPressed: () => _surroundTextSelection(
                  '```',
                  '```',
                ),
              ),
              IconButton(
                tooltip: 'Strikethrough',
                icon: Icon(Icons.strikethrough_s_rounded),
                onPressed: () => _surroundTextSelection(
                  '~~',
                  '~~',
                ),
              ),
              IconButton(
                tooltip: 'Link',
                icon: Icon(Icons.link_sharp),
                onPressed: () => _surroundTextSelection(
                  '[title](https://',
                  ')',
                ),
              ),
              IconButton(
                tooltip: 'Image Link',
                icon: Icon(Icons.image),
                onPressed: () => _surroundTextSelection(
                  '![](https://',
                  ')',
                ),
              ),
              // FutureBuilder(
              //   future: _uploadImageFromGalleryFuture,
              //   builder: (context, snapshot) {
              //     final isLoading = snapshot.connectionState ==
              //         ConnectionState.waiting;

              //     void _onPressed() {
              //       setState(
              //         () {
              //           _uploadImageFromGalleryFuture =
              //               _uploadImage(
              //             ImageSource.gallery,
              //           );
              //         },
              //       );
              //     }

              //     return IconButton(
              //       tooltip: 'Upload image from gallery',
              //       icon: isLoading
              //           ? _iconButtonLoading
              //           : Icon(Icons.photo_library),
              //       onPressed: isLoading ? null : _onPressed,
              //     );
              //   },
              // ),
              // FutureBuilder(
              //   future: _uploadImageFromCameraFuture,
              //   builder: (context, snapshot) {
              //     final isLoading = snapshot.connectionState ==
              //         ConnectionState.waiting;

              //     void _onPressed() {
              //       setState(
              //         () {
              //           _uploadImageFromCameraFuture =
              //               _uploadImage(
              //             ImageSource.camera,
              //           );
              //         },
              //       );
              //     }

              //     return IconButton(
              //       tooltip: 'Upload image from camera',
              //       icon: isLoading
              //           ? _iconButtonLoading
              //           : Icon(Icons.camera_alt),
              //       onPressed: isLoading ? null : _onPressed,
              //     );
              //   },
              // )
            ],
          ),
        ),
      ),
    ]));
  }

  void _surroundTextSelection(String left, String right) {
    final currentTextValue = _textController.value.text;
    final selection = _textController.selection;
    final middle = selection.textInside(currentTextValue);
    final newTextValue = selection.textBefore(currentTextValue) +
        '$left$middle$right' +
        selection.textAfter(currentTextValue);

    _textController.value = _textController.value.copyWith(
        text: newTextValue,
        selection: TextSelection.collapsed(
            offset: selection.baseOffset + left.length + middle.length));
  }

  void openArticleDialog(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) {
          TextEditingController controller1 = TextEditingController();
          TextEditingController controller2 = TextEditingController();
          return Card(
              child: Container(
            padding: EdgeInsets.fromLTRB(10, 50, 10, 0),
            child: Column(children: [
              TextFormField(
                controller: controller1,
                decoration: InputDecoration(hintText: 'Title'),
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: controller2,
                decoration: InputDecoration(hintText: 'Subtitle'),
              ),
              SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: () {
                    title = controller1.text;
                    subtitle = controller2.text;
                    print('Test1: ' + title + '|' + subtitle + '|' + text);
                    NewsMainBloc()
                      ..add(NewsMainEvent.createNewArticle(
                          title, subtitle, text));
                    controller1.clear();
                    controller2.clear();
                    _textController.clear();
                    ExtendedNavigator.of(context).pop();
                    ExtendedNavigator.of(context).pop();
                    ExtendedNavigator.of(context).pop();
                  },
                  child: Text('OK'))
            ]),
          ));
        });
  }
}
