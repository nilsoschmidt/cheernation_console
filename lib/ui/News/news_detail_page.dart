import 'package:auto_route/auto_route.dart';
import 'package:cheernation_admin/bloc/News/news_main_bloc.dart';
import 'package:cheernation_admin/data/models/News/article.dart';
import 'package:cheernation_admin/routes/router.gr.dart';
import 'package:cheernation_admin/ui/Widgets/custom_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';

class NewsDetailPage extends StatelessWidget {
  final Article article;

  const NewsDetailPage(this.article, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: BlocProvider<NewsMainBloc>(
          create: (context) => NewsMainBloc(),
          child: NewsDetailForm(article),
        ),
      ),
    );
  }
}

class NewsDetailForm extends StatelessWidget {
  final Article article;

  const NewsDetailForm(this.article, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomAppBar(
          article.title,
          actions: [
            PopupMenuButton<Widget>(
              onSelected: handleClick,
              itemBuilder: (BuildContext context) {
                return {
                  ElevatedButton(
                      onPressed: () {
                        ExtendedNavigator.of(context).push(
                            Routes.newsPreviewPage,
                            arguments:
                                NewsPreviewPageArguments(text: article.text));
                      },
                      child: Row(
                        children: [Icon(Icons.preview), Text('Preview')],
                      )),
                  ElevatedButton(
                      onPressed: () {
                        ExtendedNavigator.of(context).push(
                            Routes.newsEditPage,
                            arguments:
                                NewsEditPageArguments(article: article));
                      },
                      child: Row(
                        children: [Icon(Icons.edit), Text('Edit')],
                      )),
                  ElevatedButton(
                      onPressed: () {
                        showDeleteDialog(context, article.articleId);
                      },
                      child: Row(
                        children: [Icon(Icons.delete), Text('Delete')],
                      )),
                }.map((Widget choice) {
                  return PopupMenuItem<Widget>(value: choice, child: choice);
                }).toList();
              },
            )
            // IconButton(icon: Icon(Icons.preview), onPressed: () => ExtendedNavigator.of(context).push(Routes.newsPreviewPage, arguments: NewsPreviewPageArguments(text: article.text))),
            // IconButton(
            //   onPressed: () => showDeleteDialog(context, article.articleId),
            //   icon: Icon(Icons.delete_forever),
            // )
          ],
        ),
        Divider(
          height: 1,
          color: Colors.grey,
        ),
        Container(
          height: 50,
          child: article.subtitle != '' ? Text(article.subtitle) : Container(),
        ),
        Text(
          article.text,
        ),
      ],
    );
  }

  void handleClick(Widget value) {}

  void launchURL(String url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';

  showDeleteDialog(BuildContext context, String articleId) {
    showDialog(
        context: context,
        builder: (BuildContext buildContext) {
          return AlertDialog(
            title: Text('Wirklich Löschen?'),
            content: Text('Möchtest du diesen Artikel wirklich löschen?'),
            actions: [
              TextButton(
                  onPressed: () => Navigator.pop(buildContext),
                  child: Text('NEIN')),
              TextButton(
                  onPressed: () {
                    NewsMainBloc()..add(NewsMainEvent.deleteArticle(articleId));
                    ExtendedNavigator.of(context).popUntilRoot();
                  },
                  child: Text('JA'))
            ],
          );
        });
  }
}
