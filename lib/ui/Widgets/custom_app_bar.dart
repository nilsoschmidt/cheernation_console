import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final List<Widget> actions;
  final bool back;

  static double hoehe = 60.0;

  const CustomAppBar(this.title, {Key key, this.actions, this.back = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: Container(
        height: hoehe,
        padding:
            const EdgeInsets.only(left: 8.0, top: 0.0, bottom: 0.0, right: 8),
        child: Row(
          children: <Widget>[
            Expanded(
              child: back
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(width: 40, child: BackButton()),
                      ],
                    )
                  : Container(),
            ),
            Text(
              title,
              style: new TextStyle(fontSize: 22.0, fontWeight: FontWeight.w500),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: actions == null
                    ? <Widget>[
                        Container(width: 50),
                      ]
                    : actions.map((element) => iconContainer(element)).toList(),
              ),
            ),
          ],
        ),
      ),
      decoration:
          new BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
    );
  }

  Widget iconContainer(Widget icon) {
    return Container(height: 40, width: 40, child: icon);
  }

  @override
  Size get preferredSize => Size.fromHeight(hoehe);
}
