import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TeamProfilImageContainer extends StatelessWidget {
  /*final String clubId;
  final String teamId;*/
  final String teamImageUrl;
  final double height;

  const TeamProfilImageContainer(
      {Key key, @required this.teamImageUrl, @required this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (teamImageUrl != null) {
      return Container(
        height: height,
        width: height,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          image: new DecorationImage(
              image: new NetworkImage((teamImageUrl)), fit: BoxFit.fill),
        ),
      );
    } else {
      return Container(
        width: height,
        height: height,
        decoration: BoxDecoration(
            shape: BoxShape.circle, gradient: LinearGradient(colors: [Colors.black, Colors.white])),
        child: Icon(
          FontAwesomeIcons.users,
          color: Colors.white,
          size: height/2,
        ),
      );
    }
  }
}
