import 'package:flutter/material.dart';

class MyErrorWidget extends StatefulWidget {
  MyErrorWidget({Key key}) : super(key: key);

  @override
  _MyErrorWidgetState createState() => _MyErrorWidgetState();
}

class _MyErrorWidgetState extends State<MyErrorWidget> {
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.error,
          size: 50,
          color: Colors.red,
        ),
        Text('Error', style: TextStyle(color: Colors.red, fontSize: 26)),
        Text(
          'Bitte den Fehler mit möglichst vielen Details im Bugs Channel melden',
          style: TextStyle(fontSize: 20),
          textAlign: TextAlign.center,
        )
      ],
    ));
  }
}
