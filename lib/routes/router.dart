import 'package:auto_route/auto_route_annotations.dart';
import 'package:cheernation_admin/ui/Clubs/add_club_page.dart';
import 'package:cheernation_admin/ui/Clubs/add_team_page.dart';
import 'package:cheernation_admin/ui/Clubs/club_detail_page.dart';
import 'package:cheernation_admin/ui/Clubs/club_member_page.dart';
import 'package:cheernation_admin/ui/Clubs/clubs_main_page.dart';
import 'package:cheernation_admin/ui/Clubs/team_detail_page.dart';
import 'package:cheernation_admin/ui/Events/add_event_page.dart';
import 'package:cheernation_admin/ui/Events/edit_event_page.dart';
import 'package:cheernation_admin/ui/Events/events_main_page.dart';
import 'package:cheernation_admin/ui/Main_Menu/main_menu_page.dart';
import 'package:cheernation_admin/ui/News/news_add_page.dart';
import 'package:cheernation_admin/ui/News/news_detail_page.dart';
import 'package:cheernation_admin/ui/News/news_edit_page.dart';
import 'package:cheernation_admin/ui/News/news_main_page.dart';
import 'package:cheernation_admin/ui/News/news_preview_page.dart';
import 'package:cheernation_admin/ui/User/send_notification.dart';
import 'package:cheernation_admin/ui/User/user_main_page.dart';
import 'package:cheernation_admin/ui/User/user_search.dart';
import '../app.dart';

@MaterialAutoRouter(routes: <AutoRoute>[
  MaterialRoute(page: StartupDecider, initial: true),

  MaterialRoute(page: MainMenuPage),

  //User
  MaterialRoute(page: UserMainPage),
  MaterialRoute(page: SendNotificationPage),
  MaterialRoute(page: UserSearchPage),

  //Clubs
  MaterialRoute(page: ClubsMainPage),
  MaterialRoute(page: AddClubPage),
  MaterialRoute(page: AddTeamPage),
  MaterialRoute(page: ClubDetailPage),
  MaterialRoute(page: TeamDetailPage),
  MaterialRoute(page: ClubMemberPage),

  //Events
  MaterialRoute(page: EventsMainPage),
  MaterialRoute(page: AddEventPage),
  MaterialRoute(page: EditEventPage),


  //News
  MaterialRoute(page: NewsMainPage),
  MaterialRoute(page: NewsDetailPage),
  MaterialRoute(page: NewsAddPage),
  MaterialRoute(page: NewsPreviewPage),
  MaterialRoute(page: NewsEditPage),


])
class $AppRouter {}
