// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../app.dart';
import '../data/models/Club/club_short.dart';
import '../data/models/Events/championship.dart';
import '../data/models/News/article.dart';
import '../data/models/Team/team_short.dart';
import '../ui/Clubs/add_club_page.dart';
import '../ui/Clubs/add_team_page.dart';
import '../ui/Clubs/club_detail_page.dart';
import '../ui/Clubs/club_member_page.dart';
import '../ui/Clubs/clubs_main_page.dart';
import '../ui/Clubs/team_detail_page.dart';
import '../ui/Events/add_event_page.dart';
import '../ui/Events/edit_event_page.dart';
import '../ui/Events/events_main_page.dart';
import '../ui/Main_Menu/main_menu_page.dart';
import '../ui/News/news_add_page.dart';
import '../ui/News/news_detail_page.dart';
import '../ui/News/news_edit_page.dart';
import '../ui/News/news_main_page.dart';
import '../ui/News/news_preview_page.dart';
import '../ui/User/send_notification.dart';
import '../ui/User/user_main_page.dart';
import '../ui/User/user_search.dart';

class Routes {
  static const String startupDecider = '/';
  static const String mainMenuPage = '/main-menu-page';
  static const String userMainPage = '/user-main-page';
  static const String sendNotificationPage = '/send-notification-page';
  static const String userSearchPage = '/user-search-page';
  static const String clubsMainPage = '/clubs-main-page';
  static const String addClubPage = '/add-club-page';
  static const String addTeamPage = '/add-team-page';
  static const String clubDetailPage = '/club-detail-page';
  static const String teamDetailPage = '/team-detail-page';
  static const String clubMemberPage = '/club-member-page';
  static const String eventsMainPage = '/events-main-page';
  static const String addEventPage = '/add-event-page';
  static const String editEventPage = '/edit-event-page';
  static const String newsMainPage = '/news-main-page';
  static const String newsDetailPage = '/news-detail-page';
  static const String newsAddPage = '/news-add-page';
  static const String newsPreviewPage = '/news-preview-page';
  static const String newsEditPage = '/news-edit-page';
  static const all = <String>{
    startupDecider,
    mainMenuPage,
    userMainPage,
    sendNotificationPage,
    userSearchPage,
    clubsMainPage,
    addClubPage,
    addTeamPage,
    clubDetailPage,
    teamDetailPage,
    clubMemberPage,
    eventsMainPage,
    addEventPage,
    editEventPage,
    newsMainPage,
    newsDetailPage,
    newsAddPage,
    newsPreviewPage,
    newsEditPage,
  };
}

class AppRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.startupDecider, page: StartupDecider),
    RouteDef(Routes.mainMenuPage, page: MainMenuPage),
    RouteDef(Routes.userMainPage, page: UserMainPage),
    RouteDef(Routes.sendNotificationPage, page: SendNotificationPage),
    RouteDef(Routes.userSearchPage, page: UserSearchPage),
    RouteDef(Routes.clubsMainPage, page: ClubsMainPage),
    RouteDef(Routes.addClubPage, page: AddClubPage),
    RouteDef(Routes.addTeamPage, page: AddTeamPage),
    RouteDef(Routes.clubDetailPage, page: ClubDetailPage),
    RouteDef(Routes.teamDetailPage, page: TeamDetailPage),
    RouteDef(Routes.clubMemberPage, page: ClubMemberPage),
    RouteDef(Routes.eventsMainPage, page: EventsMainPage),
    RouteDef(Routes.addEventPage, page: AddEventPage),
    RouteDef(Routes.editEventPage, page: EditEventPage),
    RouteDef(Routes.newsMainPage, page: NewsMainPage),
    RouteDef(Routes.newsDetailPage, page: NewsDetailPage),
    RouteDef(Routes.newsAddPage, page: NewsAddPage),
    RouteDef(Routes.newsPreviewPage, page: NewsPreviewPage),
    RouteDef(Routes.newsEditPage, page: NewsEditPage),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    StartupDecider: (data) {
      final args = data.getArgs<StartupDeciderArguments>(
        orElse: () => StartupDeciderArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => StartupDecider(key: args.key),
        settings: data,
      );
    },
    MainMenuPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const MainMenuPage(),
        settings: data,
      );
    },
    UserMainPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const UserMainPage(),
        settings: data,
      );
    },
    SendNotificationPage: (data) {
      final args = data.getArgs<SendNotificationPageArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => SendNotificationPage(
          args.userId,
          key: args.key,
        ),
        settings: data,
      );
    },
    UserSearchPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const UserSearchPage(),
        settings: data,
      );
    },
    ClubsMainPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const ClubsMainPage(),
        settings: data,
      );
    },
    AddClubPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const AddClubPage(),
        settings: data,
      );
    },
    AddTeamPage: (data) {
      final args = data.getArgs<AddTeamPageArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => AddTeamPage(
          args.clubShort,
          key: args.key,
        ),
        settings: data,
      );
    },
    ClubDetailPage: (data) {
      final args = data.getArgs<ClubDetailPageArguments>(
        orElse: () => ClubDetailPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => ClubDetailPage(
          key: args.key,
          clubShort: args.clubShort,
        ),
        settings: data,
      );
    },
    TeamDetailPage: (data) {
      final args = data.getArgs<TeamDetailPageArguments>(
        orElse: () => TeamDetailPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => TeamDetailPage(
          key: args.key,
          teamShort: args.teamShort,
          clubShort: args.clubShort,
        ),
        settings: data,
      );
    },
    ClubMemberPage: (data) {
      final args = data.getArgs<ClubMemberPageArguments>(
        orElse: () => ClubMemberPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => ClubMemberPage(
          key: args.key,
          clubShort: args.clubShort,
        ),
        settings: data,
      );
    },
    EventsMainPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const EventsMainPage(),
        settings: data,
      );
    },
    AddEventPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const AddEventPage(),
        settings: data,
      );
    },
    EditEventPage: (data) {
      final args = data.getArgs<EditEventPageArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => EditEventPage(
          args.championship,
          key: args.key,
        ),
        settings: data,
      );
    },
    NewsMainPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const NewsMainPage(),
        settings: data,
      );
    },
    NewsDetailPage: (data) {
      final args = data.getArgs<NewsDetailPageArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => NewsDetailPage(
          args.article,
          key: args.key,
        ),
        settings: data,
      );
    },
    NewsAddPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const NewsAddPage(),
        settings: data,
      );
    },
    NewsPreviewPage: (data) {
      final args = data.getArgs<NewsPreviewPageArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => NewsPreviewPage(
          args.text,
          key: args.key,
        ),
        settings: data,
      );
    },
    NewsEditPage: (data) {
      final args = data.getArgs<NewsEditPageArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => NewsEditPage(
          args.article,
          key: args.key,
        ),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// StartupDecider arguments holder class
class StartupDeciderArguments {
  final Key key;
  StartupDeciderArguments({this.key});
}

/// SendNotificationPage arguments holder class
class SendNotificationPageArguments {
  final String userId;
  final Key key;
  SendNotificationPageArguments({@required this.userId, this.key});
}

/// AddTeamPage arguments holder class
class AddTeamPageArguments {
  final ClubShort clubShort;
  final Key key;
  AddTeamPageArguments({@required this.clubShort, this.key});
}

/// ClubDetailPage arguments holder class
class ClubDetailPageArguments {
  final Key key;
  final ClubShort clubShort;
  ClubDetailPageArguments({this.key, this.clubShort});
}

/// TeamDetailPage arguments holder class
class TeamDetailPageArguments {
  final Key key;
  final TeamShort teamShort;
  final ClubShort clubShort;
  TeamDetailPageArguments({this.key, this.teamShort, this.clubShort});
}

/// ClubMemberPage arguments holder class
class ClubMemberPageArguments {
  final Key key;
  final ClubShort clubShort;
  ClubMemberPageArguments({this.key, this.clubShort});
}

/// EditEventPage arguments holder class
class EditEventPageArguments {
  final Championship championship;
  final Key key;
  EditEventPageArguments({@required this.championship, this.key});
}

/// NewsDetailPage arguments holder class
class NewsDetailPageArguments {
  final Article article;
  final Key key;
  NewsDetailPageArguments({@required this.article, this.key});
}

/// NewsPreviewPage arguments holder class
class NewsPreviewPageArguments {
  final String text;
  final Key key;
  NewsPreviewPageArguments({@required this.text, this.key});
}

/// NewsEditPage arguments holder class
class NewsEditPageArguments {
  final Article article;
  final Key key;
  NewsEditPageArguments({@required this.article, this.key});
}
