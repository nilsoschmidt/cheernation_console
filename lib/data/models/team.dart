import 'package:json_annotation/json_annotation.dart';
part 'team.g.dart';

@JsonSerializable()
class Team {
  //Login
  final String id;
  final String name;
  //final String email;

  //Attribute
  final List<String> userIds;
  final String clubId;
  /*final String nickName;
  final DateTime alter;*/
  final String profilImageURL;

  Team(this.id, this.name, this.userIds, this.profilImageURL, this.clubId);

  factory Team.fromJson(Map<String, dynamic> json) => _$TeamFromJson(json);
  Map<String, dynamic> toJson() => _$TeamToJson(this);
}
