import 'package:cheernation_admin/core/services/jsonConverter.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

part 'championship.freezed.dart';
part 'championship.g.dart';

@freezed
abstract class Championship with _$Championship {
  factory Championship(
      {@required String name,
      @required String location,
      String link,
      @FirestoreTimestampConverter() DateTime date,
      @required String type,
      String eventId}) = _Championship;

  factory Championship.fromJson(Map<String, dynamic> json) =>
      _$ChampionshipFromJson(json);
}
