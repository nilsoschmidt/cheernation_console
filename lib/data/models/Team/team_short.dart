import 'package:freezed_annotation/freezed_annotation.dart';

part 'team_short.freezed.dart';
part 'team_short.g.dart';

@freezed
abstract class TeamShort with _$TeamShort {
  factory TeamShort({
    @required String clubId,
    @required String teamId,
    @required String teamName,
    @required String teamImageUrl,
    @required String teamCategory,
    @required String teamInviteLink,
  }) = _TeamShort;

  factory TeamShort.fromJson(Map<String, dynamic> json) =>
      _$TeamShortFromJson(json);
}
