import 'package:freezed_annotation/freezed_annotation.dart';

part 'team_member_short.freezed.dart';
part 'team_member_short.g.dart';

@freezed
abstract class TeamMemberShort with _$TeamMemberShort {
  factory TeamMemberShort({
    @required String clubId,
    @required String teamId,
    @required String userId,
    String userImageUrl,
    String vorname,
    String nachname,
    bool isAdmin,
  }) = _TeamMemberShort;

  factory TeamMemberShort.fromJson(Map<String, dynamic> json) =>
      _$TeamMemberShortFromJson(json);
}
