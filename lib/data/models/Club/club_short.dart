import 'package:freezed_annotation/freezed_annotation.dart';

part 'club_short.freezed.dart';
part 'club_short.g.dart';

@freezed
abstract class ClubShort with _$ClubShort {
  factory ClubShort({
    @required String clubId,
    @required String clubName,
    @required Map<String, String> clubAdress,
    }) = _ClubShort;

  factory ClubShort.fromJson(Map<String, dynamic> json) =>
      _$ClubShortFromJson(json);
}
