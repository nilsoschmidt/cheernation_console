import 'package:freezed_annotation/freezed_annotation.dart';

part 'club_member_short.freezed.dart';
part 'club_member_short.g.dart';

@freezed
abstract class ClubMemberShort with _$ClubMemberShort {
  factory ClubMemberShort({
    @required String clubId,
    @required String userId,
    String userImageUrl,
    String vorname,
    String nachname,
    bool isAdmin
  }) = _ClubMemberShort;

  factory ClubMemberShort.fromJson(Map<String, dynamic> json) =>
      _$ClubMemberShortFromJson(json);
}
