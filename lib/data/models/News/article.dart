import 'package:cheernation_admin/core/services/jsonConverter.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

part 'article.freezed.dart';
part 'article.g.dart';

@freezed
abstract class Article with _$Article {
  factory Article({
    @required String articleId,
    @required String title,
    String subtitle,
    @required String text,
    @required String author,
    @FirestoreTimestampConverter() DateTime time,
  }) = _Article;

  factory Article.fromJson(Map<String, dynamic> json) =>
      _$ArticleFromJson(json);
}
