import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_public.freezed.dart';
part 'user_public.g.dart';

@freezed
abstract class UserPublic with _$UserPublic {
  factory UserPublic({
    @required String authUserId,
    String nickname,
    String name,
    String profilImageUrl,
    //@FirestoreTimestampConverter() DateTime birthday,

    List<String> clubIds,
    List<String> teamIds,
  }) = _UserPublic;

  factory UserPublic.fromJson(Map<String, dynamic> json) =>
      _$UserPublicFromJson(json);
}
