import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_short.freezed.dart';
part 'user_short.g.dart';

@freezed
abstract class UserShort with _$UserShort {
  factory UserShort({
    @required String authUserId,
    String nickname,
    String name,
    String profilImageUrl,
    List<String> searchArray,
  }) = _UserShort;

  factory UserShort.fromJson(Map<String, dynamic> json) =>
      _$UserShortFromJson(json);
}
