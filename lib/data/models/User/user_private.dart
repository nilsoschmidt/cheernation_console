import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_private.freezed.dart';
part 'user_private.g.dart';

@freezed
abstract class UserPrivate with _$UserPrivate {
  factory UserPrivate({
    @required String authUserId,
    @required String email,
    String nickname,
    String name,
    String profilImageUrl,
    //@FirestoreTimestampConverter() DateTime birthday,

    List<String> tokens,
    List<String> clubIds,
    List<String> teamIds,
  }) = _UserPrivate;

  factory UserPrivate.fromJson(Map<String, dynamic> json) =>
      _$UserPrivateFromJson(json);
}
