import 'package:freezed_annotation/freezed_annotation.dart';

part 'club.freezed.dart';
part 'club.g.dart';

@freezed
abstract class Club with _$Club {
  factory Club({
    @required String id,
    String name,
    List<String> mitgliederIds,
    String beschreibung,
    String strasse,
    String plz,
    String ort,
    String land,
  }) = _Club;

  factory Club.fromJson(Map<String, dynamic> json) => _$ClubFromJson(json);
}
