import 'package:cheernation_admin/data/models/News/article.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';

@singleton
class NewsRepository {
  final FirebaseFirestore _firestore;

  NewsRepository(this._firestore);

  Future<List<Article>> getArticleList() async {
    print("Bin in Article");
    QuerySnapshot snapshot = await _firestore
        .collection("News")
        .doc('artikel')
        .collection("Artikel")
        .orderBy("time", descending: true)
        .get();
    print("Habe Article geladen");
    if (snapshot.docs.length < 1) {
      print("Albert ist dumm");
      return <Article>[];
    } else {
      print("Nils ist dumm");
      final List<Article> articleList = <Article>[];
      snapshot.docs.forEach((article) {
        articleList.add(Article.fromJson(article.data()));
      });
      print("Nils ist dumm hoch 2");
      return articleList;
    }
  }

  Future<bool> deleteArticle(String articleId) async {
    try {
      await _firestore
          .collection('News')
          .doc('artikel')
          .collection('Artikel')
          .doc(articleId)
          .delete();
      return true;
    } catch (e) {
      print('Fehler: $e');
      return false;
    }
  }

  Future<bool> createNewArticle(
      String title, String subtitle, String text) async {
    try {
      String articleId = await _firestore
          .collection('News')
          .doc('artikel')
          .collection('Artikel')
          .doc()
          .id;
      Article artikel = Article(
          articleId: articleId,
          title: title,
          subtitle: subtitle,
          text: text,
          author: 'Cheernation',
          time: DateTime.now());
      await _firestore
          .collection('News')
          .doc('artikel')
          .collection('Artikel')
          .doc(articleId)
          .set(artikel.toJson());
      return true;
    } catch (e) {
      print('Fehler: $e');
      return false;
    }
  }

  Future<bool> editArticle(
      String articleId, String title, String subtitle, String text) async {
    try {
      await _firestore
          .collection('News')
          .doc('artikel')
          .collection('Artikel')
          .doc(articleId)
          .update({
        'title': title,
        'subtitle': subtitle,
        'text': text,
        'time': DateTime.now()
      });
      return true;
    } catch (e) {
      print('Fehler: $e');
      return false;
    }
  }
}
