import 'dart:math';
import 'dart:async';
import 'package:cheernation_admin/data/models/User/user_private.dart';
import 'package:cheernation_admin/data/models/User/user_public.dart';
import 'package:cheernation_admin/data/models/User/user_short.dart';
import 'package:cheernation_admin/data/models/User/user_stats.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
//import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:injectable/injectable.dart';

@singleton
class UserRepository {
  final FirebaseAuth _firebaseAuth;
  final FirebaseFirestore _firestore;

  CollectionReference collectionReference;

  UserRepository(this._firebaseAuth, this._firestore) {
    collectionReference = _firestore.collection("Users");
  }

  Future<bool> checkAppZugangPasswort(String passwort) async {
    try {
      DocumentSnapshot snapshot = await _firestore
          .collection("App_Zugang_Passwort")
          .doc(passwort)
          .get();
      print(snapshot.data());
      if (snapshot.data() != null) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  Future<void> signInWithCredentials(String email, String password) {
    return _firebaseAuth.signInWithEmailAndPassword(
      email: email,
      password: password,
    );
  }

  Future<void> signOut() async {
    return Future.wait([
      _firebaseAuth.signOut(),
    ]);
  }

  bool isSignedIn() {
    final currentUser = _firebaseAuth.currentUser;
    return currentUser != null;
  }

  Future<bool> isUserInDatabase() async {
    final currentUser = _firebaseAuth.currentUser;

    DocumentSnapshot snapshot =
        await collectionReference.doc(currentUser.uid).get();
    if (snapshot.data() == null) {
      return false;
    } else {
      return true;
    }
  }

  Future<UserShort> getUserShort(String nickname) async {
    var snapshot = await _firestore
        .collectionGroup("User_Short")
        .where("nickname", isEqualTo: nickname)
        .limit(1)
        .get();
    if (snapshot.docs.length < 1) {
      return null;
    } else {
      return UserShort.fromJson(snapshot.docs.first.data());
    }
  }

  Future<UserShort> getUserShortbyId(String userId) async {
    var snapshot = await _firestore
        .collection('Users')
        .doc(userId)
        .collection('User_Short')
        .doc('user_short')
        .get();
    if (snapshot == null) {
      return null;
    } else {
      return UserShort.fromJson(snapshot.data());
    }
  }

  Future<bool> isFollowingUser(String followingUserId) async {
    User user = getFirebaseUser();
    DocumentSnapshot followingUser = await _firestore
        .collection("Users")
        .doc(user.uid)
        .collection("Following")
        .doc(followingUserId)
        .get();
    if (followingUser.data() != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> followUser(String followingUserId) async {
    User user = getFirebaseUser();

    Map data = <String, dynamic>{
      "userId": user.uid,
      "followingUserId": followingUserId,
      "status": "accepted"
    };

    await _firestore
        .collection("Users")
        .doc(user.uid)
        .collection("Following")
        .doc(followingUserId)
        .set(data);
  }

  Future<void> unfollowUser(String followingUserId) async {
    User user = getFirebaseUser();

    /*Map data = <String, dynamic>{
      "userId" : user.uid,
      "followingUserId" : followingUserId,
      "status" : "accepted"
    };*/

    await _firestore
        .collection("Users")
        .doc(user.uid)
        .collection("Following")
        .doc(followingUserId)
        .delete();
  }

  Future<List<String>> getFollowerList(String userId) async {
    List<String> _followerIdsList = [];
    QuerySnapshot _snapshot = await _firestore
        .collection('Users')
        .doc(userId)
        .collection("Followers")
        .get();
    if (_snapshot.docs.length < 1) {
      return _followerIdsList;
    } else {
      _snapshot.docs.forEach((followerId) {
        _followerIdsList.add(followerId.id);
      });
      return _followerIdsList;
    }
  }

  Future<List<String>> getFollowingList(String userId) async {
    List<String> _followingIdsList = [];
    QuerySnapshot _snapshot = await _firestore
        .collection('Users')
        .doc(userId)
        .collection("Following")
        .get();
    if (_snapshot.docs.length < 1) {
      return _followingIdsList;
    } else {
      _snapshot.docs.forEach((followingId) {
        _followingIdsList.add(followingId.id);
      });
      return _followingIdsList;
    }
  }

  Future<UserStats> getUserStats(String userId) async {
    DocumentSnapshot _myDoc = await _firestore
        .collection('Users')
        .doc(userId)
        .collection('User_Stats')
        .doc('user_stats')
        .get();
    print(_myDoc);
    return UserStats.fromJson(_myDoc.data());
  }

  Future<List<UserShort>> getUserShortList(String nickname) async {
    List<UserShort> userShortList = [];
    UserShort userShort = await getUserShort(nickname);
    if (userShort != null) {
      userShortList.add(userShort);
    }
    QuerySnapshot snapshot = await _firestore
        .collectionGroup("User_Short")
        .where("searchArray", arrayContains: nickname)
        .limit(8)
        .get();
    if (snapshot.docs.length < 1) {
      return null;
    } else {
      snapshot.docs.forEach((userShort) {
        userShortList.add(UserShort.fromJson(userShort.data()));
      });
      return userShortList;
    }
  }

  Future<List<UserShort>> getAllUsers() async {
    List<UserShort> userShortList = [];
    QuerySnapshot snapshot = await _firestore
        .collectionGroup("User_Short")
        .orderBy('nickname')
        .get();
    if (snapshot.docs.length < 1) {
      return null;
    } else {
      snapshot.docs.forEach((userShort) {
        userShortList.add(UserShort.fromJson(userShort.data()));
      });
      return userShortList;
    }
  }

  Future<UserPublic> getUserPublic(UserShort userShort) async {
    DocumentSnapshot snapshot = await collectionReference
        .doc(userShort.authUserId)
        .collection("User_Public")
        .doc("user_public")
        .get();
    if (snapshot.data() != null) {
      print("UserPublic geladen");
      return UserPublic.fromJson(snapshot.data());
    } else {
      print("UserPublic NICHT geladen");
      return null;
      //FirebaseUser firebaseUser = await getFirebaseUser();
      //return UserPublic( nickname: userShort.nickname);
    }
  }

  Future<UserPrivate> getUserPrivate() async {
    final currentUser = _firebaseAuth.currentUser;

    DocumentSnapshot snapshot = await collectionReference
        .doc(currentUser.uid)
        .collection("User_Private")
        .doc("user_private")
        .get();
    if (snapshot.data() != null) {
      print("UserPrivate geladen");
      print(UserPrivate.fromJson(snapshot.data()));
      return UserPrivate.fromJson(snapshot.data());
    } else {
      print("UserPrivate NICHT geladen");
      User firebaseUser = getFirebaseUser();
      return UserPrivate(
          authUserId: firebaseUser.uid, email: firebaseUser.email);
    }
  }

  Future<bool> isNicknameAvailable(nickname) async {
    UserShort userShort = await getUserShort(nickname);
    if (userShort == null) {
      print("$nickname ist available");
      return true;
    } else {
      UserPrivate userPrivate = await getUserPrivate();
      if (userShort.nickname == userPrivate.nickname) {
        print("$nickname ist available");
        return true;
      } else {
        print("$nickname ist nicht available");
        return false;
      }
    }
  }

  List<String> _createSearchString(UserPrivate userPrivate) {
    List<String> searchArray = List<String>();
    Random random = new Random();
    for (int i = 0; i < 200; i++) {
      int zufallszahl1 = random.nextInt(userPrivate.nickname.length);
      int zufallszahl2 = random.nextInt(userPrivate.nickname.length);
      if (zufallszahl1 != zufallszahl2) {
        if (zufallszahl1 < zufallszahl2) {
          if (!searchArray.contains(
                  userPrivate.nickname.substring(zufallszahl1, zufallszahl2)) &&
              (zufallszahl2 - zufallszahl1) > 0) {
            searchArray.add(
                userPrivate.nickname.substring(zufallszahl1, zufallszahl2));
          }
        } else {
          if (!searchArray.contains(
                  userPrivate.nickname.substring(zufallszahl2, zufallszahl1)) &&
              (zufallszahl1 - zufallszahl2) > 0) {
            searchArray.add(
                userPrivate.nickname.substring(zufallszahl2, zufallszahl1));
          }
        }
      }
    }
    if (userPrivate.name != null && userPrivate.name.length > 0) {
      for (int i = 0; i < 100; i++) {
        int zufallszahl1 = random.nextInt(userPrivate.name.length);
        int zufallszahl2 = random.nextInt(userPrivate.name.length);
        if (zufallszahl1 != zufallszahl2) {
          if (zufallszahl1 < zufallszahl2) {
            if (!searchArray.contains(
                    userPrivate.name.substring(zufallszahl1, zufallszahl2)) &&
                (zufallszahl2 - zufallszahl1) > 0) {
              searchArray
                  .add(userPrivate.name.substring(zufallszahl1, zufallszahl2));
            }
          } else {
            if (!searchArray.contains(
                    userPrivate.name.substring(zufallszahl2, zufallszahl1)) &&
                (zufallszahl1 - zufallszahl2) > 0) {
              searchArray
                  .add(userPrivate.name.substring(zufallszahl2, zufallszahl1));
            }
          }
        }
      }
    }
    searchArray.sort((a, b) => a.compareTo(b));
    return searchArray;
  }

  Future<bool> createUser(UserPrivate userPrivate) async {
    //TODO: user_short, user_private, user_public erstellen!

    await collectionReference
        .doc(userPrivate.authUserId)
        .collection("User_Private")
        .doc("user_private")
        .set(userPrivate.toJson());

    UserPublic userPublic = UserPublic.fromJson(userPrivate.toJson());
    await collectionReference
        .doc(userPrivate.authUserId)
        .collection("User_Public")
        .doc("user_public")
        .set(userPublic.toJson());

    UserShort userShort1 = UserShort.fromJson(userPrivate.toJson());
    UserShort userShort =
        userShort1.copyWith(searchArray: _createSearchString(userPrivate));
    await collectionReference
        .doc(userPrivate.authUserId)
        .collection("User_Short")
        .doc("user_short")
        .set(userShort.toJson());

    UserStats userStats = UserStats(
        userId: userPrivate.authUserId,
        postCount: 0,
        followerCount: 0,
        followingCount: 0);
    await collectionReference
        .doc(userPrivate.authUserId)
        .collection("User_Stats")
        .doc("user_stats")
        .set(userStats.toJson());
    return true;
  }

  Future<bool> uploadProfilImage(String imageUrl) async {
    User firebaseUser = getFirebaseUser();
    try {
      await _firestore
          .collection("Users")
          .doc(firebaseUser.uid)
          .collection("User_Private")
          .doc("user_private")
          .update({"profilImageUrl": imageUrl});
      await _firestore
          .collection("Users")
          .doc(firebaseUser.uid)
          .collection("User_Public")
          .doc("user_public")
          .update({"profilImageUrl": imageUrl});
      await _firestore
          .collection("Users")
          .doc(firebaseUser.uid)
          .collection("User_Short")
          .doc("user_short")
          .update({"profilImageUrl": imageUrl});
      print("Profil Image wurde hochgeladen.");
      return true;
    } catch (e) {
      print("Error beim Upload des Profil Images.");
      return false;
    }
  }

  Future<bool> updateUser(UserPrivate userPrivate) async {
    //TODO: User update, vielleicht mit Cloudfunctions?
    await collectionReference
        .doc(userPrivate.authUserId)
        .collection("User_Private")
        .doc("user_private")
        .update(userPrivate.toJson());

    UserPublic userPublic = UserPublic.fromJson(userPrivate.toJson());
    await collectionReference
        .doc(userPrivate.authUserId)
        .collection("User_Public")
        .doc("user_public")
        .update(userPublic.toJson());

    UserShort userShort1 = UserShort.fromJson(userPrivate.toJson());
    UserShort userShort =
        userShort1.copyWith(searchArray: _createSearchString(userPrivate));
    await collectionReference
        .doc(userPrivate.authUserId)
        .collection("User_Short")
        .doc("user_short")
        .update(userShort.toJson());

    //await collectionReference.document(userPrivate.authUserId).updateData(userPrivate.toJson());
    return true;
  }

  Future<bool> deleteUser(String userId) async {
    try {
      await _firestore
          .collection("Users")
          .doc(userId)
          .collection("User_Private")
          .doc("user_private")
          .delete();
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> sendNotification(String userId, String text) async {
    try {
      await _firestore
          .collection('Planned_Notifications')
          .doc()
          .set({'userId': userId, 'message': text});
      return true;
    } catch (e) {
      print('[UserRepository.sendNotification]Fehler: ' + e.toString());
      return false;
    }
  }

  User getFirebaseUser() {
    return _firebaseAuth.currentUser;
  }
  //ACHTUNG!!! NUR WENN BUG DA IST!!! DEVELOP!!!
  /*void fixEmptyUserShorts() async {
    QuerySnapshot snapshot =
        await _firestore.collectionGroup("User_Private").get();
    print(snapshot.docs.length);
    snapshot.docs.forEach((user) async {
      DocumentSnapshot documentSnapshot = await _firestore
          .collection("Users")
          .doc(user.get("authUserId"))
          .collection("User_Short")
          .doc("user_short")
          .get();
      if (documentSnapshot.data() == null) {
        print(user.get("authUserId") + " " + "hat keinen User Short");
        UserShort userShort = UserShort(
          authUserId: user.get("authUserId"),
          nickname: user.get("nickname"),
          name: user.get("name"),
          profilImageUrl: user.get("profilImageUrl"),
          searchArray: _createSearchString(UserPrivate.fromJson(user.data())),
        );
        await _firestore
          .collection("Users")
          .doc(user.get("authUserId")).collection("User_Short").doc("user_short").set(userShort.toJson());
        print("User Short generiert");
      } else {
        print(user.get("authUserId") + " " + "ist korrekt.");
      }
    });
  }*/
}
