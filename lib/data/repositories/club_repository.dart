import 'dart:async';
import 'package:cheernation_admin/data/models/Club/club_member_short.dart';
import 'package:cheernation_admin/data/models/Club/club_short.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';

@singleton
class ClubRepository {
  final FirebaseFirestore _firestore;

  CollectionReference collectionReference;

  ClubRepository(this._firestore) {
    collectionReference = _firestore.collection("Clubs");
  }

  Future<ClubShort> getTeamShortbyId(String clubId) async {
    DocumentSnapshot snapshot = await _firestore
        .collection('Clubs')
        .doc(clubId)
        .collection('Club_Short')
        .doc('club_short')
        .get();
    if (snapshot == null) {
      return null;
    } else {
      return ClubShort.fromJson(snapshot.data());
    }
  }

  Future<List<ClubShort>> getAllClubs() async {
    List<ClubShort> clubList = [];
    QuerySnapshot snapshot =
        await _firestore.collectionGroup('Club_Short').get();
    snapshot.docs.forEach((element) {
      print('Fehler: ' + element.data().toString());
      clubList.add(ClubShort.fromJson(element.data()));
    });
    return clubList;
  }

  Future<ClubShort> getClubShortById(String clubId) async {
    DocumentSnapshot snap = await _firestore
        .collection('Clubs')
        .doc(clubId)
        .collection('Club_Short')
        .doc('club_short')
        .get();
    return ClubShort.fromJson(snap.data());
  }

  Future<bool> createNewClub(String clubName, Map<String,String> clubAdress) async {
    print('Das wird aber ausgeführt');
    try {
      String clubId = await _firestore.collection('Clubs').doc().id;
      print('Id: ' + clubId);
      ClubShort clubShort = ClubShort(
          clubId: clubId, clubName: clubName, clubAdress: clubAdress);
      await _firestore
          .collection('Clubs')
          .doc(clubId)
          .set({'id': clubId, 'name': clubShort.clubName});
      await _firestore
          .collection('Clubs')
          .doc(clubId)
          .collection('Club_Short')
          .doc('club_short')
          .set(clubShort.toJson());
      return true;
    } catch (e) {
      print('Fehler: Repository: ' + e.toString());
      return false;
    }
  }

  /*Future<List<ClubMemberShort>> getMyClubs() async {
    UserRepository userRepository = resolve();
    UserPrivate userPrivate = await userRepository.getUserPrivate();

    List<ClubMemberShort> myClubs = List<ClubMemberShort>();
    print(userPrivate.authUserId);
    var snapshot = await _firestore
        .collectionGroup("Team_Member_Short")
        .where("userId", isEqualTo:  userPrivate.authUserId)
        .getDocuments();
    //await collectionReference.where("mitgliederIds", arrayContains: userPrivate.authUserId).getDocuments();
    print(snapshot.documents);
    snapshot.documents.forEach((club) {
      myClubs.add(ClubMemberShort.fromJson(club.data));
    });
    print("Anzahl Clubs: ${myClubs.length}");
    return myClubs;
  }*/

  Future<DocumentSnapshot> checkInviteLink(String inviteLink) async {
    String clubId = inviteLink.substring(0, inviteLink.indexOf("_"));
    String linkId = inviteLink.substring(inviteLink.indexOf("_") + 1);
    print(clubId);
    print(linkId);
    return await _firestore
        .collection("Clubs")
        .doc(clubId)
        .collection("Invite_Links")
        .doc(linkId)
        .get();
  }

  Future<String> getInviteLink(String clubId, String teamId) async {
    QuerySnapshot snapshot = await _firestore
        .collection("Clubs")
        .doc(clubId)
        .collection("Invite_Links")
        .where("teamId", isEqualTo: teamId)
        .limit(1)
        .get();
    return snapshot.docs.first.get("inviteLink");
  }

  Future<List<ClubMemberShort>> getClubMember(String clubId) async {
    List<ClubMemberShort> clubList = [];

    var snapshot = await _firestore
        .collectionGroup("Club_Member_Short")
        .where("clubId", isEqualTo: clubId)
        .orderBy("vorname")
        .get();
    // snapshot.docs.forEach((team) {
    //   teamList.add(TeamMemberShort.fromJson(team.data()));
    // });
    for (DocumentSnapshot element in snapshot.docs) {
      bool isAdmin = false;
      Map<String, dynamic> data = element.data();
      try {
        DocumentSnapshot snap = await _firestore
            .collection('Clubs')
            .doc(clubId)
            .collection('Club_Members')
            .doc(element.data()['userId'])
            .get();
        isAdmin = snap.data().isNotEmpty ? true : false;
        //snap.data()['team_member_role'] != null ? true : false;
      } catch (_) {}
      data.addAll({'isAdmin': isAdmin});
      clubList.add(ClubMemberShort.fromJson(data));
    }
    return clubList;
  }

  Future<bool> setAdmin(ClubMemberShort clubMemberShort) async {
    try {
      DocumentSnapshot snap = await _firestore
          .collection('Clubs')
          .doc(clubMemberShort.clubId)
          .collection('Club_Members')
          .doc(clubMemberShort.userId)
          .get();
      print('User: ' + clubMemberShort.userId);
      // print('Data: ' + snap.data().toString());
      snap.data() != null
          ? await _firestore
              .collection('Clubs')
              .doc(clubMemberShort.clubId)
              .collection('Club_Members')
              .doc(clubMemberShort.userId)
              .delete()
          : await _firestore
              .collection('Clubs')
              .doc(clubMemberShort.clubId)
              .collection('Club_Members')
              .doc(clubMemberShort.userId)
              .set({
              'club_member_role': ['admin']
            });
      return true;
    } catch (e) {
      print('Fehler: ' + e.toString());
      return false;
    }
  }

  Future<bool> deleteClub(String clubId) async {
    try {
      await _firestore.collection('Clubs').doc(clubId).collection('Club_Short').doc('club_short').delete();
      return true;
    } catch (_) {
      return false;
    }
  }

  /*Future<bool> joinTeam(
      String vorname, String nachname, String clubId, String teamId) async {
    FirebaseUser user = await resolve<UserRepository>().getFirebaseUser();
    Map userData = <String, dynamic>{
      "vorname": vorname,
      "nachname": nachname,
      "userId": user.uid
    };

    print(user.uid);
    print(clubId);
    print(teamId);

    DocumentSnapshot clubSnapshot = await _firestore
        .collection("Clubs")
        .document(clubId)
        .collection("Club_Members")
        .document(user.uid).get();

    print("komme druch");
    print(clubSnapshot.data);
    
    if(clubSnapshot.data == null){
      print("hi bin drin 1");
      try {
        await _firestore
        .collection("Clubs")
        .document(clubId)
        .collection("Club_Members")
        .document(user.uid)
        .setData(userData);
      } catch (e) {
        return false;
      }
    }

    DocumentSnapshot teamSnapshot =  await _firestore
        .collection("Clubs")
        .document(clubId)
        .collection("Teams")
        .document(teamId)
        .collection("Team_Members")
        .document(user.uid).get();

    if(teamSnapshot.data == null){
     print("hi bin drin 2");
      try {
         await _firestore
        .collection("Clubs")
        .document(clubId)
        .collection("Teams")
        .document(teamId)
        .collection("Team_Members")
        .document(user.uid)  
        .setData(userData);
      } catch (e) {
        return false;
      }
    }
    return true;
  }*/
}
