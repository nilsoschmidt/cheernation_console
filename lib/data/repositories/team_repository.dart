import 'dart:async';
import 'package:cheernation_admin/core/services/injection.dart';
import 'package:cheernation_admin/data/models/Club/club_member_short.dart';
import 'package:cheernation_admin/data/models/Team/team_member_short.dart';
import 'package:cheernation_admin/data/models/User/user_private.dart';
import 'package:cheernation_admin/data/models/Team/team_short.dart';
import 'package:cheernation_admin/data/repositories/user_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';

@singleton
class TeamRepository {
  final FirebaseFirestore _firestore;

  CollectionReference collectionReference;

  TeamRepository(this._firestore) {
    collectionReference = _firestore.collection("Clubs");
  }

  Future<List<TeamMemberShort>> getMyTeams() async {
    UserRepository userRepository = resolve();
    UserPrivate userPrivate = await userRepository.getUserPrivate();

    List<TeamMemberShort> myTeams = [];
    print(userPrivate.authUserId);
    var snapshot = await _firestore
        .collectionGroup("Team_Member_Short")
        .where("userId", isEqualTo: userPrivate.authUserId)
        .get();
    print(snapshot.docs);
    snapshot.docs.forEach((team) {
      myTeams.add(TeamMemberShort.fromJson(team.data()));
    });
    print("Anzahl Teams: ${myTeams.length}");
    return myTeams;
  }

  Future<List<dynamic>> getMyTeamPermissionRoles(
      String clubId, String teamId) async {
    User user = resolve<UserRepository>().getFirebaseUser();
    DocumentSnapshot snapshot = await _firestore
        .collection('Clubs')
        .doc(clubId)
        .collection("Teams")
        .doc(teamId)
        .collection('Team_Members')
        .doc(user.uid)
        .get();
    if (snapshot == null || snapshot.data() == null) {
      return null;
    } else {
      return snapshot.get("team_member_role");
    }
  }

  Future<bool> createNewTeam(
      String clubId, String name, String image, String category) async {
    try {
      String teamId = await _firestore
          .collection('Clubs')
          .doc(clubId)
          .collection('Teams')
          .doc()
          .id;
      String inviteId = await _firestore
          .collection('Clubs')
          .doc(clubId)
          .collection('Invite_Links')
          .doc()
          .id;
      TeamShort teamShort = TeamShort(
          clubId: clubId,
          teamId: teamId,
          teamName: name,
          teamImageUrl: image,
          teamCategory: category,
          teamInviteLink: clubId + '_' + inviteId);
      await _firestore
          .collection('Clubs')
          .doc(clubId)
          .collection('Teams')
          .doc(teamId)
          .set({'id': teamId, 'name': name});
      await _firestore
          .collection('Clubs')
          .doc(clubId)
          .collection('Teams')
          .doc(teamId)
          .collection('Team_Short')
          .doc('team_short')
          .set(teamShort.toJson());

      await _firestore
          .collection('Clubs')
          .doc(clubId)
          .collection('Invite_Links')
          .doc(inviteId)
          .set({
        'id': inviteId,
        'clubId': clubId,
        'teamId': teamId,
        'inviteLink': clubId + '_' + inviteId
      });
      return true;
    } catch (_) {
      return false;
    }
  }

  Future<List<TeamShort>> getTeamsByClubId(String clubId) async {
    List<TeamShort> teamList = [];
    QuerySnapshot snapshot = await _firestore
        .collectionGroup('Team_Short')
        .where('clubId', isEqualTo: clubId)
        .get();
    for (QueryDocumentSnapshot element in snapshot.docs) {
      QuerySnapshot snap = await _firestore
          .collection('Clubs')
          .doc(clubId)
          .collection('Invite_Links')
          .where('teamId', isEqualTo: element.data()['teamId'])
          .get();
      Map<String, dynamic> data = element.data();
      data.addAll({'teamInviteLink': snap.docs.first.data()['inviteLink']});
      teamList.add(TeamShort.fromJson(data));
    }
    // await snapshot.docs.forEach((element)  {
    //   // String teamId = element.data();

    // });
    print(teamList);
    return teamList;
  }

  Future<TeamShort> getTeamShortbyId(String clubId, String teamId) async {
    var snapshot = await _firestore
        .collection('Clubs')
        .doc(clubId)
        .collection("Teams")
        .doc(teamId)
        .collection('Team_Short')
        .doc('team_short')
        .get();
    if (snapshot == null) {
      return null;
    } else {
      return TeamShort.fromJson(snapshot.data());
    }
  }

  Future<List<TeamMemberShort>> getTeamMember(String clubId, teamId) async {
    List<TeamMemberShort> teamList = [];

    var snapshot = await _firestore
        .collectionGroup("Team_Member_Short")
        .where("teamId", isEqualTo: teamId)
        .where("clubId", isEqualTo: clubId)
        .orderBy("vorname")
        .get();
    print(snapshot.docs);
    // snapshot.docs.forEach((team) {
    //   teamList.add(TeamMemberShort.fromJson(team.data()));
    // });
    for (DocumentSnapshot element in snapshot.docs) {
      bool isAdmin = false;
      Map<String, dynamic> data = element.data();
      try {
        DocumentSnapshot snap = await _firestore
            .collection('Clubs')
            .doc(clubId)
            .collection('Teams')
            .doc(teamId)
            .collection('Team_Members')
            .doc(element.data()['userId'])
            .get();
        isAdmin = snap.data().isNotEmpty ? true : false;
        //snap.data()['team_member_role'] != null ? true : false;
      } catch (_) {}
      data.addAll({'isAdmin': isAdmin});
      teamList.add(TeamMemberShort.fromJson(data));
    }
    return teamList;
  }

  Future<TeamMemberShort> getTeamMemberShortbyId(
      String clubId, String teamId, String userId) async {
    var snapshot = await _firestore
        .collection('Clubs')
        .doc(clubId)
        .collection("Teams")
        .doc(teamId)
        .collection('Team_Members')
        .doc(userId)
        .collection("Team_Member_Short")
        .doc("team_member_short")
        .get();
    if (snapshot == null) {
      return null;
    } else {
      return TeamMemberShort.fromJson(snapshot.data());
    }
  }

  Future<bool> setAdmin(String clubId, teamId, userId) async {
    try {
      DocumentSnapshot snap = await _firestore
          .collection('Clubs')
          .doc(clubId)
          .collection('Teams')
          .doc(teamId)
          .collection('Team_Members')
          .doc(userId)
          .get();
      snap.data() != null
          ? await _firestore
              .collection('Clubs')
              .doc(clubId)
              .collection('Teams')
              .doc(teamId)
              .collection('Team_Members')
              .doc(userId)
              .delete()
          : await _firestore
              .collection('Clubs')
              .doc(clubId)
              .collection('Teams')
              .doc(teamId)
              .collection('Team_Members')
              .doc(userId)
              .set({
              'team_member_role': ['admin']
            });
      return true;
    } catch (e) {
      print('Fehler' + e);
      return false;
    }
  }

  Future<bool> deleteTeam(String clubId, teamId) async {
    try {
      await _firestore
          .collection('Clubs')
          .doc(clubId)
          .collection('Teams')
          .doc(teamId)
          .delete();
      return true;
    } catch (_) {
      return false;
    }
  }

  /*Future<DocumentSnapshot> checkInviteLink(String inviteLink) async {
    String TeamId = inviteLink.substring(0, inviteLink.indexOf("_"));
    String linkId = inviteLink.substring(inviteLink.indexOf("_") + 1);
    print(TeamId);
    print(linkId);
    return await _firestore
        .collection("Teams")
        .document(TeamId)
        .collection("Invite_Links")
        .document(linkId)
        .get();
  }*/

  Future<bool> joinTeam(
      String vorname, String nachname, String clubId, String teamId) async {
    User user = resolve<UserRepository>().getFirebaseUser();

    DocumentSnapshot clubSnapshot = await _firestore
        .collection("Clubs")
        .doc(clubId)
        .collection("Club_Members")
        .doc(user.uid)
        .get();

    if (clubSnapshot.data() == null) {
      ClubMemberShort clubMemberShort = ClubMemberShort(
          clubId: clubId,
          userId: user.uid,
          vorname: vorname,
          nachname: nachname);
      try {
        await _firestore
            .collection("Clubs")
            .doc(clubId)
            .collection("Club_Members")
            .doc(user.uid)
            .collection("Club_Member_Short")
            .doc("club_member_short")
            .set(clubMemberShort.toJson());
      } catch (e) {
        return false;
      }
    }

    DocumentSnapshot teamSnapshot = await _firestore
        .collection("Clubs")
        .doc(clubId)
        .collection("Teams")
        .doc(teamId)
        .collection("Team_Members")
        .doc(user.uid)
        .get();

    if (teamSnapshot.data() == null) {
      TeamMemberShort teamMemberShort = TeamMemberShort(
          clubId: clubId,
          teamId: teamId,
          userId: user.uid,
          vorname: vorname,
          nachname: nachname);
      try {
        await _firestore
            .collection("Clubs")
            .doc(clubId)
            .collection("Teams")
            .doc(teamId)
            .collection("Team_Members")
            .doc(user.uid)
            .collection("Team_Member_Short")
            .doc("team_member_short")
            .set(teamMemberShort.toJson());
      } catch (e) {
        return false;
      }
    }
    return true;
  }
}
