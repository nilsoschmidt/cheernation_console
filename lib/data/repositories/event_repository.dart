import 'package:cheernation_admin/data/models/Events/championship.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';

@singleton
class EventRepository {
  final FirebaseFirestore _firestore;

  EventRepository(this._firestore);

  Future<List<Championship>> getOpenChampionships() async {
    var snapshot = await _firestore
        .collection("Events")
        .doc('meisterschaften')
        .collection("Meisterschaften")
        .where('type', isEqualTo: 'offene')
        .orderBy("date", descending: false)
        .get();

    print('offen geht');

    if (snapshot.docs.length < 1) {
      return <Championship>[];
    } else {
      final List<Championship> championshipList = <Championship>[];
      snapshot.docs.forEach((championship) {
        championshipList.add(Championship.fromJson(championship.data()));
      });
      return championshipList;
    }
  }

  Future<List<Championship>> getInternationalChampionships() async {
    var snapshot = await _firestore
        .collection("Events")
        .doc('meisterschaften')
        .collection("Meisterschaften")
        .where('type', isEqualTo: 'international')
        .orderBy("date", descending: false)
        .get();

    print('international auch');

    if (snapshot.docs.length < 1) {
      return <Championship>[];
    } else {
      final List<Championship> championshipList = <Championship>[];
      snapshot.docs.forEach((championship) {
        championshipList.add(Championship.fromJson(championship.data()));
      });
      return championshipList;
    }
  }

  Future<List<Championship>> getOfficialChampionships() async {
    var snapshot = await _firestore
        .collection("Events")
        .doc('meisterschaften')
        .collection("Meisterschaften")
        .where('type', isEqualTo: 'verbandsmeisterschaft')
        .orderBy("date", descending: false)
        .get();

    print('offiziel geht auch');

    if (snapshot.docs.length < 1) {
      return <Championship>[];
    } else {
      final List<Championship> championshipList = <Championship>[];
      snapshot.docs.forEach((championship) {
        championshipList.add(Championship.fromJson(championship.data()));
      });
      return championshipList;
    }
  }

  Future<List<Championship>> getAllChampionships() async {
    var snapshot = await _firestore
        .collection("Events")
        .doc('meisterschaften')
        .collection("Meisterschaften")
        .orderBy("date", descending: false)
        .get();

    print('offen geht');

    if (snapshot.docs.length < 1) {
      return <Championship>[];
    } else {
      final List<Championship> championshipList = <Championship>[];
      snapshot.docs.forEach((championship) {
        Map<String, dynamic> data = championship.data();
        data.addAll({'eventId': championship.id});
        championshipList.add(Championship.fromJson(data));
      });
      return championshipList;
    }
  }

  Future<Championship> getNextChampionship() async {
    var snapshot = await _firestore
        .collection("Events")
        .doc('meisterschaften')
        .collection("Meisterschaften")
        .orderBy("date", descending: false)
        .limit(1)
        .get();
    if (snapshot.docs.length < 1) {
      return null;
    } else {
      return Championship.fromJson(snapshot.docs.elementAt(0).data());
    }
  }

  void deleteEvent(String eventId) async {
    await _firestore
        .collection('Events')
        .doc('meisterschaften')
        .collection('Meisterschaften')
        .doc(eventId)
        .delete();
  }

  void addEvent(Championship championship) async {
    String iD = await _firestore
        .collection('Events')
        .doc('meisterschaften')
        .collection('Meisterschaften')
        .doc()
        .id;
    Championship newChampionship = Championship(
        name: championship.name,
        location: championship.location,
        type: championship.type,
        date: championship.date,
        link: championship.link,
        eventId: iD);
    await _firestore
        .collection('Events')
        .doc('meisterschaften')
        .collection('Meisterschaften')
        .doc(iD)
        .set(newChampionship.toJson());
  }

  void editEvent(Championship championship) async {
    await _firestore
        .collection('Events')
        .doc('meisterschaften')
        .collection('Meisterschaften')
        .doc(championship.eventId)
        .set(championship.toJson());
  }
}
